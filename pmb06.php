
<!-- bloc-9 -->
<div class="bloc bgc-white l-bloc" id="bloc-9">
	<div class="container bloc-sm">
		<div class="row">
			<div class="col-sm-12">
				<h2 class="text-center mg-md  tc-army-green">
					Kontak dan Lokasi
				</h2>
			</div>
		</div>
	</div>
</div>
<!-- bloc-9 END -->

<!-- bloc-10 -->
<div class="bloc bgc-white l-bloc" id="bloc-10">
	<div class="container bloc-sm">
		<div class="row">
			<div class="col-sm-4">
				<h3 class="mg-md  text-center tc-army-green">
					Universitas Budi Luhur (Pusat)
				</h3>
				
					<iframe src="img/lazyload-ph.png" data-src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3966.2136334306383!2d106.74512071466445!3d-6.23554676279865!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e69f0b89e1a77bd%3A0x398d2d41ed77b3b2!2sUniversitas+Budi+Luhur!5e0!3m2!1sen!2sid!4v1526543414986" width="380" height="480" frameborder="0" style="border:0" allowfullscreen="" class="lazyload"></iframe>
					
				
				<p class=" text-center">
						<br>Jl. Ciledug Raya, Petukangan Utara, Jakarta Selatan, 12260.&nbsp;DKI Jakarta, Indonesia.<br>P: 021-585 3753 ext:285/286/288 <br>F: 021-585 3752<br>
					</p>
			</div>
			<div class="col-sm-4">
					<h3 class="mg-md  text-center tc-army-green">
						Universitas Budi Luhur (Roxy)
					</h3>
					
						<iframe src="img/lazyload-ph.png" data-src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3966.7105600993596!2d106.80209881476884!3d-6.169497995533388!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e69f664e111daa5%3A0xfa987ef3218a4643!2sBudi+Luhur+University+-+Campus+Roxy+Mas!5e0!3m2!1sen!2sid!4v1526543276227" width="380" height="480" frameborder="0" style="border:0" allowfullscreen="" class="lazyload"></iframe>
						
					
					<p class=" text-center">
							<br>Pusat Niaga Roxy Mas Blok E2 No.38-39<br>Jl.K.H. Hasyim Ashari Jakarta Pusat<br>P: 021-6328709-10 F: 021-6322872<br>
						</p>
				</div>
			<div class="col-sm-4">
						<h3 class="mg-md  text-center tc-army-green">
							Universitas Budi Luhur (Salemba)
						</h3>
						
							<iframe src="img/lazyload-ph.png" data-src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3966.4844148013362!2d106.8507991147691!3d-6.1996428955119!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e69f466204dba4b%3A0xb8522a99a4f5b8f!2sBudi+Luhur+University%2C+Campus+C+Salemba+Mas!5e0!3m2!1sen!2sid!4v1526543530587" width="380" height="480" frameborder="0" style="border:0" allowfullscreen="" class="lazyload"></iframe>
							
						
						<p class=" text-center">
								<br>Sentra Salemba Mas Blok S-T<br>Jl. Salemba Raya No. 34-36<br>P: 021-3928688-89 F: 021-3161636<br>
							</p>
					</div>
		</div>
	</div>
</div>
<!-- bloc-10 END -->
