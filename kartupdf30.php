<?php

// Include the main TCPDF library (search for installation path).
require_once 'tcpdf/tcpdf.php';
require_once 'func.php';


// Extend the TCPDF class to create custom Header and Footer
class MYPDF extends TCPDF
{

    //Page header
    public function Header()
    {
        // Logo
        $image_file = 'tcpdf/Kartu Pendaftaran Universitas Budi Luhur_Header (1).jpg';
        $this->Image($image_file, 0, 0, 210, '', 'JPG', '', 'T', false, 400, '', false, false, 0, false, false, false);

    }

    // Page footer
    public function Footer()
    {
        // Position at 15 mm from bottom
        $this->SetY(-15.5);
        // Set font
        $this->SetFont('helvetica', 'I', 8);
        // Page number
        $image_file = 'tcpdf/Kartu Pendaftaran Universitas Budi Luhur_Footer (1).jpg';
        $this->Image($image_file, 0, '', 210, '', 'JPG', '', 'T', false, 400, '', false, false, 0, false, false, false);
    }
}

// create new PDF document
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Nicola Asuni');
$pdf->SetTitle('TCPDF Example 003');
$pdf->SetSubject('TCPDF Tutorial');
$pdf->SetKeywords('TCPDF, PDF, example, test, guide');

// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setHeaderFont(array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
$pdf->SetAutoPageBreak(true, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__) . '/lang/eng.php')) {
    require_once dirname(__FILE__) . '/lang/eng.php';
    $pdf->setLanguageArray($l);
}

// ---------------------------------------------------------

// set font
$pdf->SetFont('dejavusans', '', 10, '', true);

// add a page
$pdf->AddPage();

// set some text to print
$h = 0;

$nama = explode(' ', $nama);

$html = <<<EOD
<strong><p align="center" style="color:#1A3560;" >$datatext2</p></strong>

EOD;
$pdf->writeHTMLCell(217, 0, 0, $h += 35, $html, '', 0, 0, false, 'L', false);
// ---------------------------------------------------------
$html = <<<EOD
<p align="left" style="line-height: 110%;">
    Hi! {$nama[0]} Terimakasih telah memilih Universitas Budi Luhur - Akademi Budi Luhur sebagai tempat untuk
    berkuliah, berikut adalah detail $datatext1 online kamu:
</p>

EOD;
$nama = implode(' ', $nama);
$pdf->writeHTMLCell(0, 0, 10, $h += 25, $html, '', 0, 0, false, 'L', false);
$html = <<<EOD
<table border="0" >
    <tr>
        <td width="35%">Nomor Pendaftaran</td>
        <td width="2%">:</td>
        <td width="63%">$notest</td>
    </tr>
    <tr>
        <td>Nama Pendaftaran</td>
        <td>:</td>
        <td>$nama</td>
    </tr>
    <tr>
        <td>Fakultas</td>
        <td>:</td>
        <td>$nmfakultas</td>
    </tr>
    <tr>
        <td>Program Studi</td>
        <td>:</td>
        <td>$nmprodi</td>
    </tr>
    <tr>
        <td>Jenjang</td>
        <td>:</td>
        <td>$nmjenjang</td>
    </tr>
    <tr>
        <td>Jenis Daftar</td>
        <td>:</td>
        <td>$nmjnsdftr</td>
    </tr>
    $datatext5

</table>

EOD;
$now = tgl_indo(date("Y-m-d"));
$pdf->writeHTMLCell(150, 50, 15, $h += 10, $html, '', 0, 0, false, 'L', false);
$html = <<<EOD

<p align="left" style="line-height: 110%;">
    Salam Budi Luhur,<br>
    <br>
    Jakarta, $now <br>
    Panitia Penerimaan Mahasiswa Baru<br>
    UNIVERSITAS BUDI LUHUR - AKADEMI SEKRETARI BUDI LUHUR<BR>
    <hr>

</p>

EOD;


$pdf->writeHTMLCell(0, 0, 10, $h += 30, $html, '', 0, 0, false, 'L', false);
$html = <<<EOD

<strong>
    <p style="line-height: 110%;    color: #1A3560;" >Informasi dan Pertanyaan, Silahkan Hubungi :<br>
Telp: (021)5853753 - Ext. 285/286/288</p>
</strong>

EOD;
$pdf->writeHTMLCell(0, 0, 10, $h += 105, $html, '', 0, 0, false, 'L', false);

// print a block of text using Write()

// ---------------------------------------------------------

//Close and output PDF document
ob_end_clean();
$pdf->Output($filename, 'F');

//============================================================+
// END OF FILE
//============================================================+