<?php 
include "config.php";
include "func.php";
$notest  = $_GET['notest']; 
$query = "SELECT a.cnotest, a.cnama, b.cnmprogst, c.cnmfak,d.cnmjen, e.cnmjns  from mcalonmhs a, mprodi b, mfakultas c, mjenjang d, mjnsdaftar e where a.cnotest = '$notest'
and a.ckdprogst = b.ckdprogst
and a.ckdjen = b.ckdjen
and b.ckdjen = d.ckdjen
and b.ckdfak = c.ckdfak
and a.cstatus = e.ckdjns";
$qpre = $conn->prepare($query);
$qpre->execute();
$qdat = $qpre->fetch();
$notest = $qdat['CNOTEST'];
$nama = $qdat['CNAMA'];
$nama = explode(' ', $nama);
$nama1 = $nama[0];
$nama = implode(' ', $nama);
$nmjenjang = $qdat['CNMJEN'];
$nmprodi = $qdat['CNMPROGST'];
$nmjnsdftr = $qdat['CNMJNS'];

?>
<html>

<head>
    <meta charset="utf-8">
    <meta name="keywords"
        content="Universitas Budi Luhur, Kampus Cerdas Berbudi Luhur, Kampus Asik, Kampus Generasi Cerdas Berbudi Luhur">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" type="image/png" href="favicon.png">

    <link rel="stylesheet" type="text/css" href="./css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="style.css">
    <link rel="stylesheet" type="text/css" href="./css/animate.css">
    <link rel="stylesheet" type="text/css" href="./css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="./css/ionicons.min.css">
    <!-- <link href='./css/fontgoogleapis.css' rel='stylesheet' type='text/css'> -->
    <link href='https://fonts.googleapis.com/css?family=Lato:100,300,400,700,900&amp;subset=latin,latin-ext'
        rel='stylesheet' type='text/css'>
    <link href="./css/select2.css" rel="stylesheet" />
    <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap-glyphicons.css" rel="stylesheet">
    <style type="text/css">
        .a {
            margin-left: 50px;
            width: 800px;
        }

        .isi {
            font-size: 20px;
        }

        @page {
            margin: 0;
        }

        body {
            margin: 0mm 0mm 0mm 0mm;
        }
    </style>
</head>

<body>
    <div class="row">
        <img src="img/Kartu Pendaftaran Universitas Budi Luhur_Header.jpg" alt="logo budi luhur" class="img-responsive">
    </div>
    <div class="row ">
        <div width="100%" style="  font-family:arial; line-height:28px; width: 100%;">
            <div style="max-width: 700px;  margin: 0px auto; font-size: 14px">
                <div style="background: #fff;">
                    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
                        <tbody>
                            <tr>
                                <td style="" align="center">
                                    <h1><strong>Kartu Pendaftaran</strong></h1>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
                        <tbody>
                            <tr>
                                <td>
                                    <p> Hai, <?php echo $nama1; ?>. Terimakasih telah memilih Universitas Budi Luhur - Akademi Budi
                                        Luhur sebagai tempat untuk
                                        berkuliah, berikut adalah detail kartu pendaftaran online kamu :</p>
                                    <table>
                                        <tr>
                                            <td>Nomor Pendaftaran</td>
                                            <td>:</td>
                                            <td><?php echo $notest; ?></td>
                                        </tr>
                                        <tr>
                                            <td>Nama Pendaftar</td>
                                            <td>:</td>
                                            <td><?php echo $nama; ?></td>
                                        </tr>
                                        <tr>
                                            <td>Jenjang pendidikan</td>
                                            <td>:</td>
                                            <td><?php echo $nmjenjang ; ?></td>
                                        </tr>
                                        <tr>
                                            <td>Program Pilihan</td>
                                            <td>:</td>
                                            <td><?php echo $nmprodi; ?></td>
                                        </tr>
                                        <tr>
                                            <td>Jenis Pendaftaran</td>
                                            <td>:</td>
                                            <td> <?php echo $nmjnsdftr; ?></td>
                                        </tr>
                                    </table>
                                    <br>
                                    <br>
                                    <p align="left" style="line-height: 110%;">
                                        Salam Budi Luhur,<br>
                                        <br>
                                        Jakarta, <?php echo tgl_indo(date("Y-m-d")); ?> <br>
                                        Panitia Penerimaan Mahasiswa Baru<br>
                                        UNIVERSITAS BUDI LUHUR - AKADEMI SEKRETARI BUDI LUHUR<BR>
                                        <hr>

                                    </p>
                                    <table>
                                        <thead>
                                            <tr>
                                                Langkah pendaftaran
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>

                                                <td>
                                                    <ul>
                                                        <li>Melakukan Pendaftaran Dan Melakukan Pembayaran</li>
                                                        <li>Melengkapi Berkas Pendaftaran
                                                            <ol>
                                                                <li>Fotocopy Raport SMA/sederajat semester 3/4/5</li>
                                                                <li>Fotocopy KTP/Kartu Pelajar/SIM/Kartu Keluarga</li>
                                                                <li>Kartu Pendaftaran yang sudah dicetak melalui email
                                                                </li>
                                                                <li>Bukti Pembayaran uang pendaftaran</li>
                                                            </ol>
                                                        </li>

                                                    </ul>
                                                </td>

                                            </tr>
                                        </tbody>
                                    </table>
                                    <strong>
                                        <p style="line-height: 110%;    color: #1A3560;">Informasi dan Pertanyaan,
                                            Silahkan Hubungi :<br>
                                            Telp: (021)5853753 - Ext. 285/286/288</p>
                                    </strong>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="row"  style="margin-top: 270px;">
        <img src="img/Kartu Pendaftaran Universitas Budi Luhur_Footer.jpg" alt="logo budi luhur" class="img-responsive">
    </div>

</body>

</html>