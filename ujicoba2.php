<!-- Akhir halaman HTML yang akan di konvert -->

<!DOCTYPE html>
<html>

<head>
	<title> </title>
</head>

<body>
	<table style="">
		<tr>
			<td width="90px"><img width="805" style="margin-left: -50px; margin-top: -16px;"
					src="img/Kartu Pendaftaran Universitas Budi Luhur_Header.jpg"></td>
		</tr>
		<tr>
			<td>
				<h3>Kartu Pendaftaran</h3>
			</td>
		</tr>
		<tr>
			<td>
				<p>Hi! [ Nama Pendaftar] Terimakasih telah memilih Universitas Budi Luhur - Akademi Sekretari Budi Luhur
					sebagai tempat untuk berkuliah, berikut adalah detail kartu pendaftaran online kamu</p>
			</td>
		</tr>
		<tr>
			<td>

				<table>
					<tr>
						<td>Nomor Pendaftar</td>
						<td>:</td>
						<td>No</td>
					</tr>
					<tr>
						<td>Nama Pendaftar</td>
						<td>:</td>
						<td>Nama</td>
					</tr>
					<tr>
						<td>Fakultas</td>
						<td>:</td>
						<td>FTI</td>
					</tr>
					<tr>
						<td>Program Studi</td>
						<td>:</td>
						<td>TI</td>
					</tr>
					<tr>
						<td>Jenjang</td>
						<td>:</td>
						<td>Strata 1</td>
					</tr>
					<tr>
						<td>Username</td>
						<td>:</td>
						<td>notest</td>
					</tr>
					<tr>
						<td>Password</td>
						<td>:</td>
						<td>pass</td>
					</tr>
					<tr>
						<td>Jenis Daftar</td>
						<td>:</td>
						<td>Jalur Umum/Reguler</td>
					</tr>
				</table>
				<font>
					Salam Budi Luhur,
				</font>
			</td>
		</tr>
		<tr>
			<td>
				<p>
					Jakarta, 06 June 2018<br>
					Panitia Penerimaan Mahasiswa Baru<br>
					UNIVERSITAS BUDI LUHUR - AKADEMI SEKRETARI BUDI LUHUR
				</p>
			</td>
		</tr>
		<tr>
			<td>
				<hr style="">
			</td>
		</tr>
		<tr>
			<td>
				<table>
					<thead>
						<tr>
							<th>Langkah Pembayaran</th>
							<th>Langkah pendaftaran</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>
								<ul>
									<li>Via Atm
									<ol>
									<li>Pilih Menu <b>BAYAR/BELI</b></li>
									<li>Pilih <b>Sub Menu PENDIDIKAN</b></li>
									<li>Masukan <b> Kode Perusahaan/Institusi</b>, yaitu: <b>10067</b></li>
									<li>Masukan <b>Nomor Test</b></li>
									<li>Pilih <b>BENAR</b> jika Nomor Test sudah dimasukan</li>
									<li>Tampilkan Tagihan yang harus dibayar, masukkan <b> Nomor Item Pembayaran (No.)</b></li>
									<li>Transaksi Pembayaran <b> TELAH SELESAI </b> ( Dapat Melakukan Proses Berikutnya ,<b> Test Online </b> melalui https://pendaftaran.budiluhur.ac.id) </li>
									<li><b>Struk Tanda Bukti pembayaran </b>( Sebaiknya Disimpan)</li>
									</ol>
									</li>

									<li>Via Teller Mandiri
										<ol>
											<li>Gunakan Slip Multipayment</li>
											<li>Rekening Penerima = 10067 dan isikan Nomor Pendaftaran</li>
										</ol>
									</li>
									
								</ul>
							</td>
						
							<td>
								<ul>
									<li>Melakukan Pendaftaran Dan Melakukan Pembayaran</li>
									<li>Melengkapi Berkas Pendaftaran
										<ol>
										<li>Fotocopy Raport SMA/sederajat semester 3/4/5</li>
										<li>Fotocopy KTP/Kartu Pelajar/SIM/Kartu Keluarga</li>
										<li>Kartu Pendaftaran yang sudah dicetak melalui email</li>
										<li>Bukti Pembayaran uang pendaftaran</li>

										</ol>
									</li>
									
								</ul>
							</td>
						</tr>
					</tbody>
				</table>
			</td>
		</tr>
		<tr>
			<td width="90px"><img width="805" src="img/Kartu Pendaftaran Universitas Budi Luhur_Footer.jpg"></td>
		</tr>
	</table>

</body>

</html>