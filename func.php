<?php

function image($tempname)
{ 
    $response = [];
    $response['status']= 0;
    $status = 0;
    $allowxdat = array(
        'png' => 'image/png',
            'jpeg' => 'image/jpeg',
            'jpg' => 'image/jpeg',
            'bmp' => 'image/bmp',
            'pdf' => 'application/pdf',
           
    );
    foreach ($allowxdat as $key => $value) {
            if ($tempname == $value) {
                $response['status']= 1;
                $response['key']=$key;
                $response['value']=$value;
               
            }

    }
    return $response;

}

function tgl_indo($tanggal){
    // format = date("Y-m-d")
    $bulan = array (
        1 =>   'Januari',
        'Februari',
        'Maret',
        'April',
        'Mei',
        'Juni',
        'Juli',
        'Agustus',
        'September',
        'Oktober',
        'November',
        'Desember'
    );
    $pecahkan = explode('-', $tanggal);

    // variabel pecahkan 0 = tanggal
    // variabel pecahkan 1 = bulan
    // variabel pecahkan 2 = tahun

    return $pecahkan[2] . ' ' . $bulan[ (int)$pecahkan[1] ] . ' ' . $pecahkan[0];
}
