<?php
//  session_start();
include "config.php";
// if(empty($_GET['token'])){
// 	header('location: index.php');
// 	exit();
// }
// if($_SESSION['NOTEST']!=$_GET['notest']){
// 	$notest ="Link Tidak Valid";
// 	$disabled = "disabled";
// }
// $notest = $_GET['notest'];
$notest = "A143002198";
$query = "SELECT TO_CHAR(DTGLLHR, 'dd/mm/yyyy') as TGLLHR , mcalonmhs.* FROM MCALONMHS WHERE CNOTEST ='$notest' ";
$data = $conn->prepare($query);
$data->execute();
$q= $data->fetch();
$nama = $q['CNAMA'];
$email=$q['CEMAIL'];

if (isset($q['CJENKEL'])) {
	$jenkel = $q['CJENKEL'];
}else{
	$jenkel = "";
}
if(isset( $q['CTMPLHR'])){
	$tmptlhr = $q['CTMPLHR'];
}else{
	$tmptlhr ="";
}
if(isset($q['TGLLHR'])){
	$tgllhr = $q['TGLLHR'];
}else{
	$tgllhr="";
}
if(isset( $q['CALAMAT1'])){
	$alamat = $q['CALAMAT1'];
}else{
	$alamat="";
}
if(isset($q['CKDSMA'])){
	$kdsma = $q['CKDSMA'];
}else{
	$kdsma="";
}
if(isset($q['CSTATUS'])){
	$cstatus = $q['CSTATUS'];
}else{
	$cstatus="";
}

$query = "SELECT * FROM MFILEPMB WHERE CNOTEST ='$notest' ";
$data = $conn->prepare($query);
$data->execute();
$q= $data->fetch();
$f1 = $q['CRAPOR'];
$f3 = $q['CKK'];
$f4 = $q['CPERINGKAT'];
if($f1==1)
{
	$f1= "disabled";
}
else{
	$f1= "";
}
if($f3==1)
{
	$f3= "disabled";
}
else{
	$f3= "";
}
if($f4==1)
{
	$f4= "disabled";
}
else{
	$f4= "";
}
$disabled = "";
// print_r($_SESSION);

?><!-- bloc-12 -->
<div class="bloc bgc-white l-bloc" id="bloc-12">
	<div class="container bloc-sm">
		<div class="row">
			<div class="col-sm-12">
				<form id="form" action="xtestxpmb09A.php" method="POST" novalidate
					success-msg="Your message has been sent."
					fail-msg="Sorry it seems that our mail server is not responding, Sorry for the inconvenience!"
					enctype="multipart/form-data">
					<h3 class="text-center mg-lg  tc-army-green">
						Upload Dokumen
					</h3>
					<div class="form-group">
						<label class="labelinput">
							Nomor Pendaftaran 
						</label>
						<input class="form-control" type="text" name="notest" required id="notest" readonly value="<?php echo $notest; ?>" />
					</div>
					<div class="form-group">
						<label class="labelinput">
							Nama 
						</label>
						<input class="form-control" type="text" name="nama" required id="nama" readonly value="<?php echo $nama; ?>" />
					</div>
					<div class="form-group">
						<label class="labelinput">
							Jenis Kelamin	 
						</label>
						
						<select  class="form-control" name="jenkel" id="jenkel">
							<option value="P" <?php if($jenkel=="P"){ echo "selected"; } ?>>Laki - Laki</option>
							<option value="W" <?php if($jenkel=="W"){ echo "selected"; } ?>>Perempuan</option>
						</select>
					</div>
					<div class="form-group">

						<div class="row">
							<div class="col-md-5">
								<label class="labelinput">Tempat Lahir</label>
								<input type="text" name="tmptlhr" id="tmptlhr" class="form-control" value = "<?php echo $tmptlhr ?>">
							</div>
							<div class="col-md-5">
								<label class="labelinput">Tanggal Lahir</label>
								<div class='input-group date'>
									<input type='text' id="tgllhr" name="tgllhr" class="form-control datetimepicker" value="<?php echo $tgllhr; ?>"/>
									<span class="input-group-addon datetimepicker-addon">
										<span class="glyphicon glyphicon-calendar"></span>
									</span>
								</div>
							</div>
						</div>
					</div>
					<div class="form-group">
						<label class="labelinput">Alamat</label>
						<textarea class="form-control" name="alamat" id="alamat" cols="30" rows="10"><?php echo $alamat; ?></textarea>
					</div>
					<div class="form-group">
					 <label class="labelinput">Asal Sekolah</label>
					 
					 <select  class="form-control js-data-example-ajax" name="sma" id="sma">
					 
					 </select>
					</div>
					<div class="form-group">
						<label class="labelinput">Beasiswa</label>
						<select  class="form-control" name="beasiswa" id="beasiswa">
							<option value="1" <?php if($cstatus=="0011"){ echo "selected"; } ?> >Ya</option>
							<option value="0" <?php if($cstatus=="0001"){ echo "selected"; } ?> >Tidak</option>
						</select>
						<div class="alert alert-warning" role="alert">Silakan Pilih Ya apabila anda ingin mengikuti beasiswa akademik</div>
					</div>
					<!-- <div class="form-group">
						<label>
							Email
						</label>
						<input class="form-control" type="text" name="email" required id="email" />
					</div> -->
					<div class="form-group">
						<div class="row">
							<label class="labelinput">
								File
							</label>
							<!-- <span class="label label-warning">Maximal file 10mb</span> -->
						</div>
						<div class="row">

							<div class="alert alert-warning" role="alert">dokumen yang diunggah harus berupa png,  jpeg, jpg, atau bmp dan
								maksimal ukuran file adalah 1MB</div>
						</div>
						

						<div class="row" style="margin-top:10px">
						<div class="col-md-12">
						<div class="input-group">
								<span class="input-group-addon" id="sizing-addon2" style="width:200px">Foto Rapor 3/4/5</span>
								<input type="file" name="attach1" id="attach1" class="form-control" accept="" <?php echo $f1; ?> >
							</div>

</div>
						</div>
						<!-- <div class="row" style="margin-top:10px">
							<div class="input-group">
								<span class="input-group-addon" id="sizing-addon2" style="width:185px">Foto Bukti Pembayaran</span>
								<input type="file" name="attach2" id="attach2" class="form-control" accept="" >
							</div>

						</div> -->
						<div class="row" style="margin-top:10px">
							<div class="col-md-12">
							<div class="input-group">
								<span class="input-group-addon" id="sizing-addon2"style="width:200px">Foto Kartu Keluarga / KTP </span>
								<input type="file" name="attach3" id="attach3" class="form-control" accept="" <?php echo $f3; ?>>
							</div>
							</div>

						</div>
						<div class="row" style="margin-top:10px">
							<div class="col-md-6">
								<div class="input-group">
									<span class="input-group-addon" id="sizing-addon2" style="width:200px">Foto Keterangan
										Peringkat</span>
									<input type="file" name="attach4" id="attach4" class="form-control" accept=""
										<?php echo $f4; ?>>
									<!-- <span class="input-group-addon">opsional</span> -->
								</div>
							</div>
							<div class="col-md-1">
								<div class="input-group">
									<span class="label label-warning">opsional</span>
								</div>
							</div>

						</div>
					</div>

					<button class="bloc-button btn btn-lg btn-block btn-bleu-de-france" type="submit" <?php echo $disabled; ?>>
						Kirim
					</button>
					<a class="bloc-button btn btn-lg btn-block btn-bleu-de-france" href="logout.php">
					Keluar
					</a>
				</form>
			</div>
		</div>
	</div>
</div>
<pre id='aa'>
<?php 

print_r($_SESSION);

?>
</pre>
<!-- bloc-12 END -->
<script>
// function upload(name) {
// 	var fd = new form_3111();
// 	var idname = "#"+name;
// 	var files = $(idname)[0].files[0];

// 	$.ajax({
// 		url:'pmb09ATEST.php',
// 		type:'get',
// 		data:fd,
// 		contentType: false, 
// 		processData: false,
// 		success: function(response){ 
//                         if(response != 0){ 
//                            alert('file uploaded'); 
//                         } 
//                         else{ 
//                             alert('file not uploaded'); 
//                         } 
//                     },
// 	});
// }
	$(document).ready(function () {
		
		$(function () {
			$('.datetimepicker').datetimepicker(
				{format: 'DD/MM/YYYY',}
			);

			$('.datetimepicker-addon').on('click', function () {
				$(this).prev('input.datetimepicker').data('DateTimePicker').toggle();
			});
		});
		$('.js-data-example-ajax').select2({
			ajax: {
				url: 'pmb09C.php',
				dataType: 'json'
				// Additional AJAX parameters go here; see the end of this chapter for the full code of this example
			}
		});
		$("#attach1").change(function () { 
			cekfile("attach1");
		});
		$("#attach3").change(function () { 
			cekfile("attach3");
		});
		$("#attach4").change(function () { 
			cekfile("attach4");
		});
		
	});

	function cekfile(name) {
		let myForm = document.getElementById('form');
			let formData = new FormData(myForm);
			// var nama = $("#".name).val();
			$.ajax({
				url: "pmb09D.php?type="+name,
				type:'POST',
				data: new FormData(myForm),
				contentType: false,
				cache: false,
				processData: false,
				success: function (msg) {
					// $("#aa").html(msg);
					var obj=JSON.parse(msg);
					var nama = Object.keys(obj);
					var status,pesan,mstatus;
					// var status= obj.nama.status;
					// alert(obj.constructor.name);
					// alert(nama);
					if(nama == "attach1"){
						status = obj.attach1.error;
					}else if(nama == "attach3"){
						status = obj.attach3.error;
					}else if(nama == "attach4"){
						status = obj.attach4.error;
					}
					if(status ==1){
						// pesan = "Berhasil";
						// message = "Data yang dipilih sesuai";
					}else if(status==2){
						mstatus = "error"
						pesan = "Dokumen tidak sesuai";
						message = "Dokumen yang anda pilih adalah dokumen yang tidak sesuai";	
						alertmessage(mstatus,pesan,message);
					}
					else if(status==3){
						mstatus = "error"
						pesan = "Dokumen terlalu besar";
						message = "Dokumen yang anda pilih terlalu besar, maksimal dokumen adalah 1MB";	
						alertmessage(mstatus,pesan,message);
					}
					// alert(status);
					// alert(pesan);
					
					
				}
			});
	}
	function alertmessage(mstatus,pesan,message) {
		Swal.fire({
									title: pesan,
									type: mstatus,
									html: message,
									showCloseButton: true,
									focusConfirm: false,
								
								});
		
	}

	<?php
	$html="";
	if (isset($_SESSION['response'])) {
		$cek = 0;
		foreach ($_SESSION['response'] as $key => $value) {
			// echo "aa".$value['error'];
			if($key=="attach1"){
				$html .= "<li> Rapor :";	
			}elseif($key=="attach3"){
				$html .= "<li> Kartu Keluarga :";	
			}elseif($key=="attach4"){
				$html .= "<li> Peringkat :";	
			}
			if($value['error']==1){
				$html .= " Berhasil </li>";
			}elseif($value['error']==2){
				$html .= " Tipe data tidak sesuai </li>";
			}elseif($value['error']==3){
				$html .= " Ukuran file terlalu besar </li>";
			}elseif($value['error']==4){
				$html .= "  Terdapat Error Dengan Kode 4, Silakan hubungi Telp: 021-585-3753 atau WhatsApp: 08111-9394-79
				  </li>";
			}elseif($value['error']==5){
				$html .= " - </li>";
			}
			elseif( $value['error']==6){
				$html .= "  Tidak dapat mengubah file , file sudah diverifikasi </li>";
			}
			elseif($value['error']==11){
				$html .= "  Terdapat Error Dengan Kode 11, Silakan hubungi Telp: 021-585-3753 atau WhatsApp: 08111-9394-79
				  </li>";
			}
		}
		if((empty($_SESSION['response']['attach1']['error'])||$_SESSION['response']['attach1']['error'] == 1) &&$_SESSION['response']['attach3']['error'] == 1){
			$status = "Success";
			$pesan = "Berhasil";

		}elseif(isset($_SESSION['response']['attach1']['error'])&&($_SESSION['response']['attach1']['error'] == 5 &&$_SESSION['response']['attach3']['error'] == 5&&$_SESSION['response']['attach4']['error'] == 5)){
			$status = "Warning";
			$pesan = "Tidak ada file yang di unggah";

		}elseif (isset($_SESSION['response']['attach1']['error'])&&($_SESSION['response']['attach1']['error'] != 1 ||$_SESSION['response']['attach3']['error'] != 1)) {
			$status = "Warning";
			$pesan = "Terdapat dokumen yang tidak terunggah";

		}
		
		
	}
	


	if (isset($_GET['status']) && $_GET['status'] == "1" && isset($_SESSION['response'])) {
		?>
		$(document).ready(function () {
			// Swal.fire("Berhasil", "<strong>Silahkan Periksa Email anda</strong>", "success")
			Swal.fire({
				title: '<?php echo $pesan; ?>',
				type: '<?php echo $status; ?>',
				html: "<ul style='text-align:left;'>"+"<?php echo $html; ?>"+"</ul>",
				showCloseButton: true,
				focusConfirm: false,
			
			})
		});
		<?php
		unset($_SESSION['response']);
	}
	elseif(isset($_GET['data']) && $_GET['data'] == "false") {
		
		?>
		$(document).ready(function () {
			Swal.fire("Upload dibatalkan",
				"Tidak ada file yang diupload.<br> Pastikan Anda telah membayar uang pendaftaran dan nomor pendaftaran yang Anda masukan telah benar",
				"warning")
		}); 
		<?php
		unset($_SESSION['response']);
	}
	
	// echo "test";
	// print_r($_GET);
		?>
		

</script>