
<!-- bloc-25 -->
<div class="bloc bgc-white l-bloc" id="bloc-25">
	<div class="container bloc-sm">
		<div class="row">
			<div class="col-sm-12">
				<h2 class="text-center mg-md  tc-army-green">
					Brosur &amp; Form Pendaftaran
				</h2>
			</div>
		</div>
	</div>
</div>
<!-- bloc-25 END -->

<!-- bloc-26 -->
<div class="bloc bgc-white l-bloc" id="bloc-26">
	<div class="container bloc-sm">
		<div class="row">
			<div class="col-sm-12">
				<div class="text-center">
					<a href="https://pendaftaran.budiluhur.ac.id/doc/BROSUR-2018-web.pdf" class="btn btn-lg btn-clean btn-bleu-de-france btn-block">Brosur Universitas Budi Luhur (klik untuk download)</a>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- bloc-26 END -->

<!-- bloc-27 -->
<div class="bloc bgc-white l-bloc" id="bloc-27">
	<div class="container bloc-sm">
		<div class="row">
			<div class="col-sm-12">
				<div class="text-center">
					<a href="https://pendaftaran.budiluhur.ac.id/doc/Flyer%20Univ%20BL%202018-OK.pdf" class="btn btn-clean btn-lg btn-block btn-dark-midnight-blue">Flyer Universitas Budi Luhur (klik untuk download)</a>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- bloc-27 END -->

<!-- bloc-28 -->
<div class="bloc bgc-white l-bloc" id="bloc-28">
	<div class="container bloc-sm">
		<div class="row">
			<div class="col-sm-12">
				<div class="text-center">
					<a href="https://pendaftaran.budiluhur.ac.id/doc/Brosur%20Astri%202017.compressed.pdf" class="btn btn-clean btn-lg btn-deep-chestnut btn-block">Brosur Akademi Sekretari Budi Luhur (klik untuk download)</a>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- bloc-28 END -->

<!-- bloc-29 -->
<div class="bloc bgc-white l-bloc" id="bloc-29">
	<div class="container bloc-sm">
		<div class="row">
			<div class="col-sm-12">
				<div class="text-center">
					<a href="https://pendaftaran.budiluhur.ac.id/doc/flyer%20astri%202018-OK%20(1).pdf" class="btn btn-clean btn-lg btn-block btn-burgundy">Flyer Akademi Sekretari Budi Luhur (klik untuk download)</a>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- bloc-29 END -->

<!-- bloc-30 -->
<div class="bloc bgc-white l-bloc" id="bloc-30">
	<div class="container bloc-sm">
		<div class="row">
			<div class="col-sm-12">
				<div class="text-center">
					<a href="https://pendaftaran.budiluhur.ac.id/doc/Brosur-IP-Baru-2018.pdf" class="btn btn-clean btn-lg btn-block btn-anti-flash-white">Brosur Internasional Program (IBMS) (klik untuk download)</a>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- bloc-30 END -->

<!-- bloc-31 -->
<div class="bloc bgc-white l-bloc" id="bloc-31">
	<div class="container bloc-sm">
		<div class="row">
			<div class="col-sm-12">
				<div class="text-center">
					<a href="https://pendaftaran.budiluhur.ac.id/doc/FORM-PENDAFT-MANUAL.pdf" class="btn btn-clean btn-lg btn-block btn-light-gray">Formulir Pendaftaran Mahasiswa Baru</a>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- bloc-31 END -->

<!-- bloc-32 -->
<div class="bloc bgc-white l-bloc" id="bloc-32">
	<div class="container bloc-sm">
		<div class="row">
			<div class="col-sm-12">
				<h2 class="text-center mg-md  tc-army-green">
					Kini lebih mudah mendaftar online untuk masuk Universitas Budi Luhur&nbsp;<a class="ltc-bleu-de-france" href="index.php?page=daftar">disini</a>
				</h2>
			</div>
		</div>
	</div>
</div>
<!-- bloc-32 END -->
