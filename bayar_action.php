<?php
session_start();
include "config.php";
date_default_timezone_set('Asia/Jakarta');
$notest = $_POST['notest']; 


$jenisbayar = $_POST['jenisbayar']; 

if (empty($notest)) {
    header("location: index.php?input=$cek&page=bayar");
    exit();
}
	
	$query0 = $conn->prepare("select b.ckdfak, b.ckdprogst, initcap(namafak(b.ckdfak)), initcap(b.cnmprogst), decode(b.ckdjen,'50','Strata 1','30','Diploma 3','60','Strata 2') ,cemail,cnama from mcalonmhs a, mprodi b where a.ckdprogst = b.ckdprogst and a.ckdjen=b.ckdjen and a.cnotest='$notest' ");
	$query0->execute();
	$rs0 = $query0->fetch();
	
	
	$query1 = $conn->prepare("SELECT sistem.nobuktikwtreg FROM dual ");
	$query1->execute();
	$rs1 = $query1->fetch();
	
	$query2 = $conn->prepare("SELECT * from MGEL where sysdate >= dtmulai and sysdate<=dtselesai ");
	$query2->execute();
	$rs2 = $query2->fetch();

	$gel = $rs2['CGEL'];
	if (empty($gel)) {
		header("location: index.php?input=failure&gel=no");
		exit();
	}
	$fakultas = $rs0[0];
	$nmjenjang = $rs0[4];
	$nmprodi = $rs0[3];
	$nama = $rs0['CNAMA'];
	$email = $rs0['CEMAIL'];
	$emailcspmb = $_SESSION['XEMAIL'];
	$cthajar = $rs2['CTAHUN'];
	
	//parameter yang digunakan
	$xthajar = $cthajar . "/";
	$xthajar .= $cthajar + 1;

	$cthajar .= $cthajar + 1;

	$csmt = 'O';
    $nobukti = $rs1[0] ;
	$tgl = date("d/M/Y");	
	$jam = date("H:i");
	
	$d=strtotime("+5 Days");
	$tanggalakhir =  date("d-M-Y H:i:s", $d) ;


	$_SESSION['cekbayar'] = 'NO';
    $query6 = $conn->prepare("select count(*) from trbayar where cnim='$notest' 
    and cketbayar ='KULIAH' 
    and cthajar = '$cthajar' and csmt ='$csmt' ");
    
	$query6->execute();
	$rs6 = $query6->fetch();
	//echo $rs6[0]."ass";
	if ($rs6[0] > 0) {
		$_SESSION['cekbayar'] = 'DONE';
		header('location:0 index.php?page=status2&here=done');
		exit();
	}
	
	
   //INSERT TBYRREG
    if($jenisbayar == 'Tunai'){
		$query = $conn->prepare("SELECT DETILBAYARMHSBARU('".$notest."','1') FROM dual");
	}else{
		$query = $conn->prepare("SELECT DETILBAYARMHSBARU('".$notest."','2') FROM dual");
	}
	
	
	$query->execute();
	$rs = $query->fetch();
	if($query){
		$dt= $rs[0];
		$dt2 = explode(";",$dt);
	 
		foreach($dt2 as $no => $item){
			//delimiter kedua koma
			if($item!=''){
				$dt3 = explode(",",$item);
				if(count($dt3)>1 && $dt3[2]!=0){
					
					// if(!in_array($dt3[0],$noakunbayar)){
					// $noakunbayar[] = $dt3[0];
					if($jenisbayar != 'Tunai') {
						$dt3[0] = "2031291" ;
					}

					$sql4 = "INSERT INTO tbyrreg(cnobukti,cnoacc,njumbayar) VALUES('$nobukti','$dt3[0]','$dt3[2]')";
					$input4 = $conn->prepare($sql4);
					$input4->execute();

					
					$tabelbayar = $tabelbayar .  "<tr><td>".$dt3[1] . "</td>
					<td> : Rp </td><td align='right'>" . number_format($dt3[2] ,2,',','.') . "</td></tr>";
					
					$jumlahbayar += $dt3[2]   ;
					// }
				}else{
					$ketbayar = trim($dt3[0]);
				}
			}
			
		}
		$tabelbayar. "<tr><td><b>Jumlah Biaya Masuk + Kuliah</b></td>
		                  <td><b> : Rp </b></td>
		                  <td align='right'><b>" . number_format($jumlahbayar,2,',','.') . "</b></td></tr>";
	}


    $formatjumlahbayar = "Rp " . number_format($jumlahbayar,2,',','.');
   
   //INSERT TRBAYAR
    $sql3 = "INSERT INTO trbayar (cnobukti, cnim, dtgltrans, csmt, cthajar, cuserlog, dtgllog, cjamlog,CKDFAK,cketbayar,NJUMLAH,CTEMPAT) 
	VALUES ('$nobukti', '$notest', sysdate, '$csmt', '$cthajar', 'WEB', '$tgl', '$jam','$fakultas','$ketbayar','$jumlahbayar','3')";
	$input = $conn->prepare($sql3);
	$input->execute();
	//echo $sql3."<br>" ;
	
	$query18 = $conn->prepare("select dexpire from ubl_srvbank_trans where cnim ='$notest' and cnobukti='$nobukti' ");
	$query18->execute();
	$rs18 = $query18->fetch();
	$batasbayar = $rs18[0];
  
    
   //INSERT TANGURAN
if($jenisbayar != 'Tunai'){
	$query8 = $conn->prepare("SELECT detilangsmhsbaru('".$notest."') FROM dual");
	$query8->execute();
	$rs8 = $query8->fetch();
	if($query8){
		$xdt= $rs8[0];
		$xdt2 = explode(";",$xdt);
	 
		foreach($xdt2 as $no => $item){
			//delimiter kedua koma
			if($item!=''){
				$xdt3 = explode(",",$item);
				
				// if(count($xdt3)>1 && $xdt3[0]!=0){
				if( $xdt3[0] >= 1 && $xdt3[0] <= count($xdt2)-1 ){	
					// if(!in_array($xdt3[0],$xnoakunbayar)){
						// $xnoakunbayar[] = $xdt3[0];
						$tambahbulan = $xdt3[0] - 1;
						
						if($xdt3[0] == 1) {
							$tglhrsbayar = 7;
						} else {
							$tglhrsbayar =  ($xdt3[0]-1) * 30 ;
						}
						$sql7 = "INSERT INTO tformangsuran(cnotest,dtglhrsbyr,njumbayar,nangske,cjenisbyr, cnokwt,ntotalkwt,cstatus) 
						VALUES('$notest',sysdate +". $tglhrsbayar ." ,'".$xdt3[1]."','".$xdt3[0]."','1','$nobukti','$jumlahbayar','0029')";
						$input7 = $conn->prepare($sql7);
						$input7->execute();
						echo $sql7."<br>" ;

					
						// $sql10 = "update tformangsuran set dtglhrsbyr = '$tglhrsbayar' where cnokwt='$nobukti' and nangske='".$xdt3[0]."' ";
						// $input10 = $conn->prepare($sql10);
						// $input10->execute();
						// echo $sql10."<br>" ;
						
						if($xdt3[0] == 1){
							//cari nobuktibaru

							// $query14 = $conn->prepare("SELECT sistem.nobuktikwtreg FROM dual ");
							// $query14->execute();
							// $rs14 = $query14->fetch();
							// $nobukti2 = $rs14[0];

							//inserttrbayar

							// $sql13 = "INSERT INTO trbayar (cnobukti, cnim, dtgltrans, csmt, cthajar, cuserlog, dtgllog, cjamlog,CKDFAK,cketbayar,NJUMLAH,CTEMPAT) 
							// VALUES ('$nobukti2', '$notest', sysdate, '$csmt', '$cthajar', 'WEB', '$tgl', '$jam','$fakultas','ANGSURAN','".$xdt3[1] - $cdiskonbayar."','3')";
							// $input = $conn->prepare($sql13);
							// $input->execute();
							
							// echo $sql13."<br>" 

						}
					
					// }
				} else{
					$xketbayar = trim($xdt3[0]);
				}
			}
		}
	}
}
// echo "HAI";
// die ;

if($jenisbayar != 'Tunai'){
	
	$query20 = $conn->prepare("select NANGSKE, NJUMBAYAR, DTGLHRSBYR 
	from tformangsuran where cnotest='$notest'");
    
    $query20->execute();
    $rs20 = $query20->fetchAll();
    
    foreach ($rs20 as $key => $value) {
		$tabelangsur = $tabelangsur .  "<tr>
		<td>Ke-".$value['NANGSKE']."   </td>
		<td> ".$value['DTGLHRSBYR']."   </td>
		<td align='left'> Rp </td>
		<td align='right'>".number_format($value['NJUMBAYAR'],2,',','.') . "</td></tr>";

    }
	
}else{
	$tabelangsur = "";
	
}
	
	
	//INSERT LOGnya
	include "config.php";
	$ipadd = get_real_ip();
	$lastlog = $conn->prepare("SELECT ID FROM LOGPMB Order by ID Desc");
	$lastlog->execute();
	$ll = $lastlog->fetch(PDO::FETCH_ASSOC);
	$idlama = $ll['ID'] + 1;
	$userentry = $_SESSION['NOTEST'];
	$log = $conn->prepare("INSERT INTO LOGPMB (ID,USERNAME,IP,TANGGAL,KETERANGAN) VALUES ('$idlama','$userentry','$ipadd',SYSDATE,'CSPMB BIAYA KULIAH [ $notest ] $nama')");
	$log->execute();


function get_real_ip() {
    $clientip = isset($_SERVER['HTTP_CLIENT_IP']) && $_SERVER['HTTP_CLIENT_IP'] ?
        $_SERVER['HTTP_CLIENT_IP'] : false;
    $xforwarderfor = isset($_SERVER['HTTP_X_FORWARDED_FOR']) && $_SERVER['HTTP_X_FORWARDED_FOR'] ?
        $_SERVER['HTTP_X_FORWARDED_FOR'] : false;
    $xforwarded = isset($_SERVER['HTTP_X_FORWARDED']) && $_SERVER['HTTP_X_FORWARDED'] ?
        $_SERVER['HTTP_X_FORWARDED'] : false;
    $forwardedfor = isset($_SERVER['HTTP_FORWARDED_FOR']) && $_SERVER['HTTP_FORWARDED_FOR'] ?
        $_SERVER['HTTP_FORWARDED_FOR'] : false;
    $forwarded = isset($_SERVER['HTTP_FORWARDED']) && $_SERVER['HTTP_FORWARDED'] ?
        $_SERVER['HTTP_FORWARDED'] : false;
    $remoteadd = isset($_SERVER['REMOTE_ADDR']) && $_SERVER['REMOTE_ADDR'] ?
        $_SERVER['REMOTE_ADDR'] : false;

    // Function to get the client ip address
    if ($clientip !== false) {
        $ipaddress = $clientip;
    } elseif ($xforwarderfor !== false) {
        $ipaddress = $xforwarderfor;
    } elseif ($xforwarded !== false) {
        $ipaddress = $xforwarded;
    } elseif ($forwardedfor !== false) {
        $ipaddress = $forwardedfor;
    } elseif ($forwarded !== false) {
        $ipaddress = $forwarded;
    } elseif ($remoteadd !== false) {
        $ipaddress = $remoteadd;
    } else {
        $ipaddress = false; # unknown
    }
    return $ipaddress;
}

include_once("config.php"); //buat koneksi ke database
try {
	// $xthajar = $cthajar . "/";
	// $xthajar .= $cthajar + 1;
	//
    require 'PHPMailer/PHPMailerAutoload.php';
    $mail = new PHPMailer;
    $mail->isSMTP();                            // Set mailer to use SMTP
    $mail->Host = 'smtp.gmail.com';             // Specify main and backup SMTP servers
    $mail->SMTPAuth = true;                     // Enable SMTP authentication
    $mail->Username = 'pmb@budiluhur.ac.id';   	// SMTP username
    $password = "bud1luhurun1v3rs1ty";
    $mail->Password = $password;         		// SMTP password
    $mail->SMTPSecure = 'tls';                  // Enable TLS encryption, `ssl` also accepted
    $mail->Port = 587;                          // TCP port to connect to 
    $mail->SMTPOptions = array(
        'ssl' => array(
            'verify_peer' => false,
            'verify_peer_name' => false,
            'allow_self_signed' => true
        )
    );
    $mail->setFrom('pmb@budiluhur.ac.id', 'PMB UNIVERSITAS dan AKADEMI SEKRETARI BUDI LUHUR ');
    $mail->addReplyTo('pmb@budiluhur.ac.id', 'PMB UNIVERSITAS dan AKADEMI SEKRETARI BUDI LUHUR ');
    $mail->addAddress($email);   				// Add a recipient
    $mail->addBCC($emailcspmb);							// Add BCC
    $mail->addAddress("registrasi@budiluhur.ac.id");
    $mail->isHTML(true);  						// Set email format to HTML;
    include "emailbody_bayar.php";
    $mail->Subject = 'Pembayaran Kuliah Semeter 1 Universitas Budi Luhur';
    $mail->Body = $bodyContent;

    if (!$mail->send()) {
        echo 'Message could not be sent.';
        echo 'Mailer Error: ' . $mail->ErrorInfo;
    } else {

    }

} catch (HTML2PDF_exception $e) {
    echo $e;
}

// move to page status
$_SESSION['nobukti'] = $nobukti;
$_SESSION['nobukti'] = "HAI";
$_SESSION['email'] = $rs0['CEMAIL'];
$_SESSION['jumlahbayar'] = $jumlahbayar;
$_SESSION['thajar'] = $xthajar;

var_dump($_SESSION['thajar']);
die ;
// header('location: index.php?page=status2');

function DTI_decode($data) {
    $dec = "";
    for ($i = 0; $i < strlen($data); $i++) {
        $tmp = ord(substr($data, $i, 1));
        $tmp1 = (($tmp & 240) >> 4) + (($tmp & 15) << 4);
        $tmp1 = $tmp1 ^ 90;
        $tmp1 = (($tmp1 & 252) >> 2) + (($tmp1 & 3) << 6);
        $dec .= chr($tmp1);
    }
    return $dec;
} 

?>
