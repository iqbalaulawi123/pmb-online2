<?php


//if(isset($_GET['input'])&&$_GET['input']=="na"){
   // echo"<script>Swal.fire('Gagal Mendaftar', 'Mohon Maaf Untuk sementara ini pendaftaran untuk kelas tersebut sedang tidak dibuka', 'warning');</script>";
//}
//  include 'config.php'; 
?>
 
<div class="bloc bgc-white l-bloc" id="bloc-12">
    <div class="container bloc-sm">
        <div class="row">
            <div class="col-sm-12">
				
                <form id="form"13 action="bayar_action.php" method="POST" novalidate
                      success-msg="Your message has been sent."
                      fail-msg="Sorry it seems that our mail server is not responding, Sorry for the inconvenience!">
                   
                    <h3 class=" text-center mg-lg tc-army-green">
                        <span class="ion ion-clipboard"></span>&nbsp;Pilih Skema Pembayaran
                    </h3>
                    
                    <!-- <div class="form-group" id="fnotest">
                        <label class="labelinput">
                            Nomor Pendaftaran
                        </label>
                        <input id="xnotest" class="form-control" name="notest" required/>
                    </div>  -->
                    <div class="form-group" id="fnotest">
                        <label class="labelinput">Nomor Pendaftaran</label>

                        <select class="form-control js-data-example-ajax" name="notest" id="xnotest" style="width: 100%;" required>

                        </select>
                    </div>  
                    <div class="form-group">
                        <label class="labelinput">
                            Jenis Pembayaran
                        </label>
                        <select class="form-control"  name="jenisbayar" id="xjenisbayar" >
                            <option value='Tunai'> Pembayaran Secara Tunai</option>
                            <option value='Angsuran'> Pembayaran Secara Angsuran</option>
                       </select>
                    </div>
                    

                    <div class="text-center">
                        <button class="bloc-button btn btn-lg btn-bleu-de-france btn-block" type="button" id="viewdetail" disabled data-toggle="modal" data-target="#detail">
                            Buat Tagihan Biaya Kuliah
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


<!-- Modal -->
<!-- Trigger the modal with a button -->


<!-- Modal -->
<div id="detail" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Buat Tagihan Biaya Kuliah</h4>
      </div>
      <div class="modal-body">
        <p>Pastikan data sesuai dengan data pendaftaran</p>
        <table>
            <tr>
                <td class="col-lg-3">No. Pendaftaran</td>
                <td class="col-lg-1">:</td>
                <td id="dnotest"></td>
            </tr>
            <tr>
                <td class="col-lg-3">Nama Lengkap</td>
                <td class="col-lg-1">:</td>
                <td id="dnama"></td>
            </tr>
             <tr>
                <td class="col-lg-3">Email</td>
                <td class="col-lg-1">:</td>
                <td id="demail"></td>
            </tr>
            <tr>
                <td class="col-lg-3">Program Studi</td>
                <td class="col-lg-1">:</td>
                <td id="dprodi"></td>
            </tr>
            <tr>
                <td class="col-lg-3"><b>Keterangan</b></td>
                <td class="col-lg-1"><b>:</b></td>
                <td id="dket"></td>
            </tr>
            <tr>
                <td class="col-lg-3" colspan='3' id="dtabel"></td>
            </tr>
        </table>
      </div>
      <div class="modal-footer">
        <button type="button" id="daftar" name="daftar" class="btn  btn-primary"  data-dismiss="modal">Buat Tagihan Biaya Kuliah</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
      <!-- <br><br><p>Tombol 'Buat Tagihan Biaya Kuliah' akan aktif jika Nomor Pendaftaran benar dan calon mahasiswa <b>SUDAH Bayar Pendaftaran</b></p> -->
      </div>
        
    </div>
    

  </div>
</div>


<!-- bloc-6 END -->
<script>
function validatename(name) {
    // jika string adalah email maka return 1 jika salah return 2
    if (/^[0-9]+$/.test(name)) {
        return "1";
    } else {
        return "2";
    }
}
$(window).ready(function () {

            $('select').on('change', function(){
                var element = document.getElementById("fnotest");
                if($(this).val() == "") {
                    element.classList.add("has-error");
                    $("#viewdetail").attr("disabled", true);
                } else {
                    $("#viewdetail").removeAttr("disabled");
                    var element = document.getElementById("fnotest");
                    element.classList.remove("has-error");  
                }
            });

            $('.js-data-example-ajax').select2({
                width: 'resolve',
                minimumInputLength : 6,
                language: {
                    "noResults" : function(){
                            return "No Pendaftar tidak ditemukan!";
                    },
                    "inputTooShort" : function () {
                        return "Masukan No Pendaftaran";
                    }
                },

                ajax: {
                    url: 'getnotest.php',
                    dataType: 'json'
                    // Additional AJAX parameters go here; see the end of this chapter for the full code of this example
                }
            });

			$("#viewdetail").click(function(){
				//var val = $("#xnotest").val();
				var val = $("#xnotest").val();
				var val2 = $("#xjenisbayar").val();
                
				$.ajax({
				    url:"pmb00A.php?getdatabayar=detailbayar",
					data:"notest="+val+"&"+"jenisbayar="+val2,
					//data:"notest="+val,
					cache:false,
					success:function(msg){
                       var obj = JSON.parse(msg);
					   $("#dnotest").html(obj.cnotest);
					   $("#dnama").html(obj.cnama);
					   $("#demail").html(obj.cemail);
					   $("#dhp").html(obj.chp);
					   $("#djenjang").html(obj.jenjang);
					   $("#dprodi").html(obj.prodi);
					   $("#dkelas").html('Reguler');
					   $("#djumlah").html(obj.njumlah);
					   $("#dflagkbl").html(obj.dflagkbl);
					   $("#dket").html(obj.ket);
					   $("#dtabel").html(obj.tabel);
					   if (obj.dflagkbl == null) {
							document.getElementById("daftar").disabled = true;
					}else{
					     document.getElementById("daftar").disabled = false;
					  }
					   if (obj.cnotest == null && obj.error == null){
						   $("#dket").html('No. Pendaftaran Tidak Sesuai');
						}else{
							 $("#dket").html(obj.ket);
						}
			
					   //$("#dnotest").html($("#xnotest").val());    
					}
				})
			});
			
			
	// $("#xnotest").keyup(function (){
    //     if(validatename(this.value)==1){
    //         $("#viewdetail").removeAttr("disabled");
    //         var element = document.getElementById("fnotest");
    //         element.classList.remove("has-error");  
    //     }else{
    //         var element = document.getElementById("fnotest");
    //         element.classList.add("has-error");
    //         $("#viewdetail").attr("disabled",true);
    //     }

    // });
			
		$("#daftar").click(function(){
        if ($("#xnotest").val() == null || $("#xnotest").val()=="") {
            Swal.fire("Gagal Mendaftar", "Silakan isi Nomor Pendaftaran", "error");
        } else {
            $("#form").submit();
        }
    });
    
   
    
 });
</script>
