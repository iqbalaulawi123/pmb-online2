
<!-- bloc-11 -->
<div class="bloc bgc-white l-bloc" id="bloc-11">
	<div class="container bloc-sm">
		<div class="row">
			<div class="col-sm-12">
				<h2 class="text-center mg-md  tc-army-green">
					Tanyakan Sesuatu?
				</h2>
				<h3 class="mg-md  tc-army-green">
					<br>Fakultas dan jurusan apa saja yang ada di Universitas Budi Luhur ?
				</h3>
				<h3 class="mg-md  tc-army-green">
					<br>Jenjang Strata 1 (S1) :
				</h3>
				<p>
					<strong>1. Fakultas Teknologi Informasi</strong><br>- Program Studi Teknik Informatika <br>&nbsp; a. Network and Web Security<br>&nbsp; b. Programming Expert<br>- Program Studi Sistem Informasi<br>&nbsp; a. Technopreneurship<br>&nbsp; b. Enterprise System <br><br><strong>2. Fakultas Ekonomi &amp; Bisnis</strong><br>- Program Studi Akuntansi<br>&nbsp; a. Akuntansi Manajemen<br>&nbsp; b. Pengauditan<br>- Program Studi Manajemen<br>&nbsp; a. Keuangan<br>&nbsp; b. Pemasaran<br>&nbsp; c. Sumber Daya Manusia <br><br><strong>3. Fakultas Ilmu Sosial &amp; Politik</strong><br>- Program Studi Hubungan Internasional<br>- Program Studi Kriminologi<br>&nbsp; a. Cyber Crime<br>&nbsp; b. Crime Journalism <br><br><strong>4. Fakultas Ilmu Komunikasi</strong><br>- Broadcast Journalism<br>- Public Relations<br>- Visual Communication<br>- Digital Advertising <br><br><strong>5. Fakultas Teknik</strong><br>- Program Studi Arsitektur<br>- Program Studi Teknik Elektro<br>&nbsp; a. Telekomunikasi<br>&nbsp; b. Kontrol <br> <br><br>
				</p>
				<h3 class="mg-md  tc-army-green">
					Jenjang Diploma 3 (D3) :
				</h3>
				<p>
					<strong>1. D3 Unggulan</strong><br>- Program Studi Manajemen Informatika <br>&nbsp; a. Networking<br>&nbsp; b. Multimedia Graphic<br>- Program Studi Komputerisasi <br><br><strong>2. Akademi Sekretari</strong><br>- Program Studi Akademi Sekretari <br><br>Untuk Info mengenai Fakultas dan jurusan dapat Dilihat disini <br> <br><br>
				</p>
				<h3 class="mg-md  tc-army-green">
					Adakah program beasiswa di Universitas Budi Luhur ?<br>
				</h3>
				<p>
					<strong>Ada</strong>, Universitas Budi Luhur memiliki 5 program beasiswa, yaitu : <br><br><strong>1. Beasiswa Akademik</strong><br>Melalui Test Tertulis untuk mendapatkan ketentuan, seperti :<br>- Grade A : Bebas Biaya Kuliah*)<br>- Grade B : Bebas Biaya Investasi Pendidikan<br>- Grade C : Potongan 50% Biaya Investasi Pendidikan (BIP)<br>- Grade D : Potongan 40% Biaya Investasi Pendidikan (BIP) <br><br><strong>2. Beasiswa Atlet</strong> <br>Melalu Prestasi/Kejuaraan olahraga(minimal tingkat daerah) <br><br><strong>3. Beasiswa Peduli</strong><br>Beasiswa yang diberikan kepada anak yatim Piatu/Yatim/Yatim Piatu atau dari keluarga yang membutuhkan bantuan <br><br><strong>4. Beasiswa Daerah</strong><br>Melalui test tertulis untuk mendapatkan bebas biaya kuliah**) <br><br><strong>5. Beasiswa Ranking/ Peringkat Kelas <br></strong> <br><br>
				</p>
				<h3 class="mg-md  tc-army-green">
					Berapa biaya kuliah dan biaya semester yang harus dibayarkan ?<br>
				</h3><img src="img/lazyload-ph.png" data-src="img/rincian.jpg" class="img-responsive lazyload" />
				<h3 class="mg-md  tc-army-green">
					Syarat untuk melakukan pendaftaran apa saja ?<br>
				</h3>
				<p>
					<strong>Jalur Umum/Reguler :</strong><br>1. Fotocopy raport SMA/sederajat semester 3/4/5/6<br>2. Fotocopy KTP/Kartu Pelajar/SIM/Kartu Keluarga<br>3. Uang pendaftaran/bukti pembayaran apabila mendaftar melalui online <br><br><strong>Jalur Beasiswa Akademik :</strong><br>1. Fotocopy raport SMA/sederajat semester 3/4/5/6 yang telah dilegalisir asli oleh kepala sekolah <br>2. Fotocopy KT P/Kartu Pelajar/SlM/Kartu Keluarga<br>3. Pas Foto berwarna 3 x 4 cm (1 Iembar)<br>4. Uang pendaftaran/bukti pembayaran apabila mendaftar melalui online <br><br><strong>Jalur Beasiswa Peduli :</strong><br>1. Fotocopy raport SMA/sederajat semester 3/4/5/6 yang telah dilegalisir asli oleh kepala sekolah<br>2. Surat Keterangan Kurang Mampu/Yatim/Piatu/Yatim Piatu dari RT-RW-Kelurahan yang asli<br>3. Fotocopy KTP/Kartu Pelajar/SIM/Kartu Keluarga 4. Pas Foto berwarna 3 x 4 cm (1 lembar) <br><br><strong>Jalur Beasiswa Atlet :</strong><br>1. Fotocopy raport SMA/sederajat semester 3/4/5/6 yang telah dilegalisir asli oIeh kepala sekolah<br>2. Surat keterangan dari KONI (Komite Olahraga Nasional Indonesia)<br>3. Fotocopy piagam/sertiflkat kejuaraan (min. tingkat daerah)<br>4. Fotocopy KTP/Kartu Pelajar/SIM/Kartu Keluarga<br>5. Pas Foto berwarna 3 x 4 cm (1 Iembar) <br> <br><br>
				</p>
				<h3 class="mg-md  tc-army-green">
					Bagaimana proses pembayaran biaya kuliah, apakah bisa dilakukan secara angsur ?<br>
				</h3>
				<p>
					<strong>Bisa</strong>, proses pembayaran dilakukan setelah melakukan pendaftaran dan test. Pembayaran dapat dilakukan dengan 2 cara yaitu:<br><br>1. Pembayaran dapat dilakukan secara lunas/sekaligus. Jika membayar biaya kuliah secara lunas, maka mendapatkan potongan sebesar Rp. 500.000 <br>dan mendapatkan tambahan potongan Rp. 500.000 jika calon mahasiswa mempunyai saudara kandung mahasiswa/alumni Universitas/Akademi sekertari Budi Luhur.<br><br>2. Pembayaran dilakukan secara angsuran (bertahap) Pembayaran dapat diangsur sebanyak 3 kali dengan mengisi form perjanjian angsuran.&nbsp;Proses pembayaran diatas dapat dilakukan di Bank Mandiri atau melalui ATM dengan nomor rekening yang diberikan Customer Service atau yang tercantum di website pendaftaran online. <br> <br><br>
				</p>
				<h3 class="mg-md  tc-army-green">
					Pertanyaan lebih lanjut dapat menghubungi<br>
				</h3>
				<p>
					Telp: (021)585 3753 - Ext. 285/286/288/353 <br>Whatsapp: 08111 9394 79<br>Fax: 021-585 3752. <br> <br> <br>*) Syarat dan ketentuan berlaku <br>**) Daerah yang dipilih oleh Universitas Budi Luhur<br>***) Rincian biaya kuliah dan semester setiap tahun mengalami perubahan <br> <br><br>
				</p>
			</div>
		</div>
	</div>
</div>
<!-- bloc-11 END -->
