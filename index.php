<?php 
	date_default_timezone_set('Asia/Jakarta'); 
	session_start();
	
	if (isset($_GET['page']) && $_GET['page']=="bayar") {
		if (!isset($_SESSION['NOTEST'])) {
    		header('location: index.php?page=link');
		}
	}
?>

<!-- <!doctype html>
<title>Under Maintenance</title>
<style>
  body { text-align: center; padding: 150px; }
  h1 { font-size: 50px; }
  body { font: 20px Helvetica, sans-serif; color: #333; }
  article { display: block; text-align: left; width: 650px; margin: 0 auto; }
  a { color: #dc8100; text-decoration: none; }
  a:hover { color: #333; text-decoration: none; }
</style>

<article>
    <h1>#-Under Maintenance-#</h1>
    <div>
        <p>This site is under maintenance...Please come back later!</p>
        <p>&mdash; Web Team</p>
    </div>
</article> -->



<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="keywords" content="Universitas Budi Luhur, Kampus Cerdas Berbudi Luhur, Kampus Asik, Kampus Generasi Cerdas Berbudi Luhur">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" type="image/png" href="favicon.png">

	<link rel="stylesheet" type="text/css" href="./css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="style.css">
	<link rel="stylesheet" type="text/css" href="./css/animate.css">
	<link rel="stylesheet" type="text/css" href="./css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="./css/ionicons.min.css">
	<!-- <link href='./css/fontgoogleapis.css' rel='stylesheet' type='text/css'> -->
	<link href='https://fonts.googleapis.com/css?family=Lato:100,300,400,700,900&amp;subset=latin,latin-ext' rel='stylesheet' type='text/css'>
	<link href="./css/select2.css" rel="stylesheet" />
	<link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap-glyphicons.css" rel="stylesheet">
	<!-- <link href="./css/bootstrap-glyphicons.css" rel="stylesheet"> -->
	
	 <!-- <link rel="stylesheet" href="css/sweetalert/sweetalert.css"> -->
	 <style>.swal2-popup {
	 	font-size: 1.6rem !important;
	 }
	 .labelinput{
		 font-size:1.82rem !important;
		 font-weight:bold !important;
	 }
	 .labelradio{
		 font-size:1.75rem !important;	
		 
	 }
	 .green{	
		 color:#86d343;
	 }
	.select-size{
		font-size:1.8rem !important;
	}
	.nomortest{
		padding-left:10px;
		font-size:3.2rem !important;
	}
	.disable-click {
    pointer-events: none;
	}
	 </style>

	
	<script src="./js/jquery-2.1.0.js"></script>
	<script src="./js/bootstrap.js"></script>
	<script src="./js/blocs.js"></script>
	<script src="./js/jqBootstrapValidation.js"></script>
	<script src="./js/formHandler.js"></script>
	<script src="./js/lazysizes.min.js" defer></script>
	<script src="./js/select2.min.js"></script>
	<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script> -->
	<!-- <script src="js/sweetalert/sweetalert.min.js"></script> -->
	<!-- <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script> -->
	<script src="./js/sweetalert2.js"></script>
	<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/moment.min.js"></script>                        -->
	<script src="./js/moment.min.js"></script>                       
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js"></script> -->
<script src="./js/bootstrap-datetimepicker.min.js"></script>
<!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/css/bootstrap-datetimepicker.min.css" /> -->
<link rel="stylesheet" href="./css/bootstrap-datetimepicker.min.css" />
    <title>Beranda | PMB ONLINE Universitas Budi Luhur - Kampus Cerdas Berbudi Luhur</title>


<!-- Google Analytics -->

<!-- Google Analytics END -->

</head>
<body>
<!-- Main container -->
<div class="page-container">

<!-- bloc-0 -->
<div class="bloc bgc-white l-bloc " id="bloc-0">
	<div class="container bloc-sm ">
		<nav class="navbar row">
			<div class="navbar-header">
				<a class="navbar-brand" href="index.php"><span class="special-tag-for-editing-text-with-html"></span><img src="img/logo_ubl.png" alt="logo" /></a>
				<button id="nav-toggle" type="button" class="ui-navbar-toggle navbar-toggle" data-toggle="collapse" data-target=".navbar-1">
					<span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span>
				</button>
			</div>
			<div class="collapse navbar-collapse navbar-1">
				<ul class="site-navigation nav navbar-nav">
					
					<li>
						<a href="http://infopmb.budiluhur.ac.id">Informasi PMB</a>
					</li>
					
					<li>
						<a href="index.php?page=kirim">Kirim Ulang Formulir Pendaftaran</a>
					</li>
					<li>
						<a href="index.php?page=hasil">Hasil Test</a>
					</li>
					<li>
						<a href="index.php?page=link">Upload Dokumen</a>
					</li>
				</ul>
			</div>
		</nav>
	</div>
</div>
<!-- bloc-0 END -->
<?php



if (isset($_GET['page']) && $_GET['page']=="daftar") {
	include "pmb01.php";
}
else if(isset($_GET['page']) && $_GET['page']=="hasil"){
include "pmb02.php";
}
else if(isset($_GET['page']) && $_GET['page']=="kirim"){
include "pmb03.php";
}
else if(isset($_GET['page']) && $_GET['page']=="panduan"){
include "pmb04.php";
}
else if(isset($_GET['page']) && $_GET['page']=="info"){
include "pmb05.php";
}
else if(isset($_GET['page']) && $_GET['page']=="kontak"){
include "pmb06.php";
}
else if(isset($_GET['page']) && $_GET['page']=="faq"){
include "pmb07.php";
}
else if(isset($_GET['page']) && $_GET['page']=="brosur"){
include "pmb08.php";
}
elseif (isset($_GET['page']) && $_GET['page']=="kartu") {
include "pmb09.php";
}
elseif (isset($_GET['page']) && $_GET['page']=="bayar") {
	include "bayar_view.php";
}
else if(isset($_GET['page']) && $_GET['page']=="status"){
	include "PMB20.php";
}
else if(isset($_GET['page']) && $_GET['page']=="status2"){
	include "PMB202.php";
}
else if(isset($_GET['page']) && $_GET['page']=="link"){
	include "pmb10.php";
}else if(isset($_GET['xpage']) && $_GET['xpage']=="4xGbx3ZEII"){
	include "xtestxpmb09.php";
}
else{

include "pmb01.php";
}


 ?>


<!-- ScrollToTop Button -->
<a class="bloc-button btn btn-d scrollToTop" onclick="scrollToTarget('1')"><span class="fa fa-chevron-up"></span></a>
<!-- ScrollToTop Button END-->


<!-- Footer - bloc-33 -->
<div class="bloc bgc-dark-midnight-blue d-bloc" id="bloc-33" style="margin-top:100px;">
	<div class="container bloc-md">
		<div class="row">
			<div class="col-sm-4">
				<h3 class="mg-md ">
					Salam Budi Luhur!
				</h3>
				<p class=" mg-sm">
					<i>Cerdas berbudi luhur adalah dua hal yang tidak terpisahkan, kecerdasan tanpa dilandasi budi luhur akan cenderung digunakan untuk membodohi dan mencelakakan orang lain, sebaliknya budi luhur tanpa diimbangi kecerdasan akan merupakan sasaran kejahatan dan penindasan orang lain.</i><br><br>Drs. Djaetun H.S. (Pendiri Yayasan Pendidikan Budi Luhur Cakti)<br>
				</p>
			</div>
			<div class="col-sm-4">
				<h3 class="mg-md ">
					Lokasi Universitas Budi Luhur
				</h3>
				<p class=" mg-sm">
					Jl. Ciledug Raya, Petukangan Utara, <br>Jakarta Selatan, 12260, Indonesia. <br><br>WhatsApp:&nbsp;08111-9394-79<br>Telp: 021-585-3753 <br>Fax: 021-585-3752<br>
				</p>
			</div>
			<div class="col-sm-4">
				<h3 class="mg-md ">
					Kerja Sama
				</h3>
				<p class=" mg-sm">
					<a href="https://www.oracle.com/index.html" target="_blank">Oracle</a>&nbsp;<br><a href="https://www.cisco.com/" target="_blank">Cisco</a>&nbsp;<br><a href="https://id.jobsdb.com/id" target="_blank">JobsDB</a>&nbsp;<br><a href="https://www.microsoft.com/id-id/" target="_blank">Microsoft <br></a><a href="https://hz.nl/en/" target="_blank">HZ University <br></a><a href="http://www.nanya.edu.tw/home.aspx" target="_blank">Nanya Institute of Technology</a>&nbsp;<br><a href="http://www.bankmandiri.co.id/" target="_blank">Bank Mandiri</a><br>
				</p>
			</div>
		</div>
	</div>
</div>
<!-- Footer - bloc-33 END -->

<!-- Footer - bloc-34 -->
<div class="bloc bgc-dark-midnight-blue d-bloc" id="bloc-34">
	<div class="container bloc-md">
		<div class="row">
			<div class="col-sm-3">
				<div class="text-center">
					<a class="social" href="https://twitter.com/kampusbudiluhur" target="_blank"><span class="fa fa-twitter icon-md"></span></a>
				</div>
			</div>
			<div class="col-sm-3">
				<div class="text-center">
					<a class="social" href="https://www.facebook.com/groups/universitasbudiluhur/" target="_blank"><span class="fa fa-facebook icon-md"></span></a>
				</div>
			</div>
			<div class="col-sm-3">
				<div class="text-center">
					<a class="social" href="https://www.youtube.com/user/kampusbudiluhur" target="_blank"><span class="fa fa-youtube-play icon-md"></span></a>
				</div>
			</div>
			<div class="col-sm-3">
				<div class="text-center">
					<a class="social" href="https://www.instagram.com/kampusbudiluhur/" target="_blank"><span class="fa fa-instagram icon-md"></span></a>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- Footer - bloc-34 END -->

<!-- Footer - bloc-35 -->
<div class="bloc bgc-oxford-blue d-bloc" id="bloc-35">
	<div class="container bloc-sm">
		<div class="row">
			<div class="col-sm-12">
				<p class=" text-center">
					Panitia Penerimaan Mahasiswa Baru Universitas Budi Luhur © 2018&nbsp;<a href="http://www.budiluhur.ac.id/" target="_blank">Universitas Budi Luhur</a>. All rights reserved.
				</p>
			</div>
		</div>
	</div>
</div>
<!-- Footer - bloc-35 END -->

</div>
<!-- Main container END -->


</body>
</html>
<?php

if(isset($_GET['hasil'])&&$_GET['hasil']=='0011')
{
    ?>
    <script>
    swal(
  'Maaf!',
  'untuk sementara ini hasil test online belum tersedia, cobalah beberapa saat lagi!',
  'warning'
)
    </script>
    <?php 
}

?>