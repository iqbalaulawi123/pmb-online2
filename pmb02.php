<!-- bloc-24 -->
<?php
// header('location: index.php?hasil=0011');
?>
<div class="bloc bgc-white l-bloc" id="bloc-24">
	<div class="container bloc-sm">
		<div class="row">
			<div class="col-sm-12">
				<!-- <div class="alert alert-warning">
					Khusus untuk program <strong>2000 Beasiswa</strong> hasil test akan diumumkan pada tanggal <strong> 5 November 2019 Jam 16.00 WIB</strong>
				</div> -->
			</div>
		</div>
		<div class="row">
			<div class="col-sm-12">
				<form id="form_30521" novalidate success-msg="Your message has been sent." fail-msg="Sorry it seems that our mail server is not responding, Sorry for the inconvenience!">
					<h3 class="text-center mg-lg  tc-army-green">
						Hasil Test Ujian Masuk
					</h3>
					<div class="form-group">
						<label>
							No.pendaftaran
						</label>
						<input class="form-control" type="text" maxlength="10" required id="cnotest" />
					</div>
					<div class="form-group">
						<label>
							Nama
						</label>
						<input class="form-control" required id="cnama" type="text" readonly />
					</div>
					<div class="form-group">
						<label>
							Program Studi
						</label>
						<input class="form-control" required type="text" id="ckdprogst" readonly/>
					</div>
					<div class="form-group">
						<label>
							Fakultas
						</label>
						<input class="form-control" required type="text" id="ckdfak" readonly />
					</div>
					<div class="form-group">
						<label>
							Alamat
						</label>
						<input class="form-control" required type="text" id="calamat" readonly />
					</div>
					<div class="form-group">
						<label>
							Grade
						</label>
						<div class="row">
										 <div class="col-md-4"><input class="form-control" required type="text" id="cgrade" readonly /> 	</div>
										 <div class="col-md-8"><input class="form-control" required type="text" id="status" readonly /></div>
						</div>

					</div>
					<button class="bloc-button btn btn-lg btn-block btn-bleu-de-france" id="cari" type="button">
						Kirim
					</button>
					
				</form>
			</div>
		</div>
	</div>
</div>
<!-- bloc-24 END -->

 <script type="text/javascript">
         $('#cari').click(function () {
			// alert("asd");
			
           run();
        });

         var input = document.getElementById("cnotest");
         input.addEventListener("keyup", function(event) {
             event.preventDefault();
             if (event.keyCode === 13) {
                 run();
             }
         });
     function run()
     {
// 		swal.fire(
//   'Maaf!',
//   'untuk sementara ini hasil test online belum tersedia, cobalah beberapa saat lagi!',
//   'warning'
// );
         var cnotest = $("#cnotest").val();
         $.ajax({
             url: "pmb02A.php",
             data: "cnotest="+cnotest,
             async: false,
             cache: false,
             success: function(msg){

                 var tes = msg;
                 var tes2 = tes.replace("[",'');
                 var obj = jQuery.parseJSON(tes2.replace("]",''));

                 if (obj.cnotest==null) {
                     swal.fire('Maaf!','Nomor Pendaftaran salah','warning');
                     $('#cnama').val('');
                     $('#cgrade').val('');
                     $('#status').val('');
                     $('#ckdfak').val('');
                     $('#ckdprogst').val('');
                     $("#calamat").val('');
                 }
                 else
                 {
                     $('#cnama').val(obj.cnama);
                     $('#cgrade').val(obj.cgrade);
                     $('#status').val(obj.status);
                     $('#ckdfak').val(obj.cnmfak);
                     $('#ckdprogst').val(obj.cnmprogst);
                     $("#calamat").val(obj.calamat);
                     if (obj.cgrade!=null&&obj.cgrade!='E') {window.open('pmb02B.php?cnotest='+obj.cnotest, '_blank',1);}


                 }
             }
         });
     }
    </script>
