<?php
/*
 //
 //    ______    _   _           _  __      _   _     ____   ___
 //   |  ____|  | | | |         | |/ _|    | | | |   |___ \ / _ \
 //   | |__ __ _| |_| |__   __ _| | |_ __ _| |_| |__   __) | | | |
 //   |  __/ _` | __| '_ \ / _` | |  _/ _` | __| '_ \ |__ <| | | |
 //   | | | (_| | |_| | | | (_| | | || (_| | |_| | | |___) | |_| |
 //   |_|  \__,_|\__|_| |_|\__,_|_|_| \__,_|\__|_| |_|____/ \___/
 //
 // Licensed under GNU General Public License v3.0
 // http://www.gnu.org/licenses/gpl-3.0.txt
 // Written by Fathalfath30. Email : fathalfath30@gmail.com
 // Follow me on GithHub : https://github.com/Fathalfath30
 //
 // For the brave souls who get this far: You are the chosen ones,
 // the valiant knights of programming who toil away, without rest,
 // fixing our most awful code. To you, true saviors, kings of men,
 //
 // I say this: never gonna give you up, never gonna let you down,
 // never gonna run around and desert you. Never gonna make you cry,
 // never gonna say goodbye. Never gonna tell a lie and hurt you.
 //
*/

if ( isset($_POST) ) {
    require 'security_client.php';
	require 'security_server.php';

    $client = new security_client();
    $url = "";
	if (isset($_POST['url']))
		$url = $_POST['url'];
	
	if (isset($_POST['URL'])) {
		$url = $_POST['URL'];
	}
	
	
    if ( isset($_POST['xpassword']) ) {
        if ( isset($_POST['xnim']) ) {
            $_POST['xpassword'] = $client->encrypt_password($_POST['xpassword'], true);
        }

        if ( isset($_POST['xnip']) ) {
            $_POST['xpassword'] = $client->encrypt_password($_POST['xpassword'], false);
        }
    }

    unset($_POST['URL']);
    try {
        $curl = curl_init();
        curl_setopt_array($curl, [
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_URL            => $url,
            CURLOPT_POST           => true,
            CURLOPT_POSTFIELDS     => [
                'xdata' => $client->encrypt(json_encode($_POST))
            ]
        ]);
        $resp = curl_exec($curl);
        curl_close($curl);

    } catch ( Exception $e ) {
        echo $e->getMessage();
    }

    if ( strlen($resp) > 0 ) {
        $json = json_decode($resp);
        if ( json_last_error_msg() == "No error" ) {
            $plain_resp = json_encode([
				'data_user_plain' 	  =>  $_POST,
				'data_user_encrypted' => [
					'xdata'=>$client->encrypt($_POST)
				],
				'response_server_enc' => (array)$json,
				'response_server_dec' => json_decode($client->decrypt($json->xdata))
			]);
           
		   echo $plain_resp;

        } else {
            echo $resp;
        }
    } else {
        echo json_encode([
            'tidak ada data yang di terima'
        ]);
    }
}