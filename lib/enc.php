<?php
/*
 //
 //    ______    _   _           _  __      _   _     ____   ___
 //   |  ____|  | | | |         | |/ _|    | | | |   |___ \ / _ \
 //   | |__ __ _| |_| |__   __ _| | |_ __ _| |_| |__   __) | | | |
 //   |  __/ _` | __| '_ \ / _` | |  _/ _` | __| '_ \ |__ <| | | |
 //   | | | (_| | |_| | | | (_| | | || (_| | |_| | | |___) | |_| |
 //   |_|  \__,_|\__|_| |_|\__,_|_|_| \__,_|\__|_| |_|____/ \___/
 //
 // Licensed under GNU General Public License v3.0
 // http://www.gnu.org/licenses/gpl-3.0.txt
 // Written by Fathalfath30. Email : fathalfath30@gmail.com
 // Follow me on GithHub : https://github.com/Fathalfath30
 //
 // For the brave souls who get this far: You are the chosen ones,
 // the valiant knights of programming who toil away, without rest,
 // fixing our most awful code. To you, true saviors, kings of men,
 //
 // I say this: never gonna give you up, never gonna let you down,
 // never gonna run around and desert you. Never gonna make you cry,
 // never gonna say goodbye. Never gonna tell a lie and hurt you.
 //
*/
// $_POST['xdata'] = '3CAdqDcXk7EDqHQA/qCuh2TFGHBWyJbm3eAxXHaFkQAAy3gF4oCLXI0FI3AcXCiAYUCEqbMXC3E/XDWAhvCWX3oB07BJ2bhFkwB66GmlozE/XdzFnSAc6a/F3TC5dCBXhwEMF2kAReEmJBDXoZAlh5PX3TB82JvFOkAdqI/A4yBp60/As2ERJaFXOYB/P20m5yC5JdRXBvBkqDgAoZAlhc4m06DcX0nGdKAFydyldcDw2DgAOoCPTIbB31EQEcWlhvBmXARlE5BpNHtAYyATN3OFwwB82BiBdKDnd5zAoCDRTHtBCLAMxNC0T1ZlMzAsXdyldcCz2bfXhwDhuDFlC3E/XISAO3ALXE/mFwacP5hmHbB9EeplYrBk2BiAbdB9EJPGIhB82IxleVB9Edyld7E/h2iFhvALqBnAOYE/hA8AOoCoua/FUtAw2HQFKDAEd2gFzUDbqCpXYyATN3OFwwB82BiBdKDnd5zAoCDluIzYF6EWhDcYcSBk2GiA/qAP20FFC3AYycvGAzERF5zAVcD1EEJlg/Cgda/FtdBp6efXs2AxXIzXYrBpNeXFpZAqNbIFrUC5dCBXmdDNd3UG1uaCFHzX2kEjTayYaqD/FClX';
function ode($xdata){

    // header("Content-type: application/json");
    // require 'security_client.php';
    $client = new security_client();
    
    $plain = json_decode($client->decrypt($xdata),true);
    
    $result = ['status' => 'error', 'message' => 'Tidak dapat memproses data dari server.', 'data' => []];
    if (json_last_error() == JSON_ERROR_NONE) {
        $result = [
            'status' => 'ok', 
            'message' => '',
            'data' => array($plain)
        ];
    }
    return $result;
    
}