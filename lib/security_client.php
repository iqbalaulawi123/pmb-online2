<?php

class security_client {

    private function decode64 ($str) {
        $base = "ABCDEabcde012345FGHIJKLMNOfghijklmno6789PQRSTUVWXYZpqrstuvwxyz+/";
        $hasil = "";
        $n = 0;
        $n = strlen($str) / 2;
        $str = substr($str, $n, strlen($str) - $n) . substr($str, 0, $n);

        while ( strlen($str) > 0 ) {
            $x1 = substr($str, 0, 1);
            $x2 = substr($str, 1, 1);
            $x3 = substr($str, 2, 1);
            $x4 = substr($str, 3, 1);

            $n1 = strpos($base, $x1);
            $n2 = strpos($base, $x2);
            $z = ($n1 << 18) + ($n2 << 12);

            if ( $x3 != "%" ) {
                $n3 = strpos($base, $x3);
                $z = $z + ($n3 << 6);
            }
            if ( $x4 != "%" ) {
                $n4 = strpos($base, $x4);
                $z = $z + $n4;
            };
            $str1 = chr($z >> 16);
            $str2 = chr(gmp_strval(gmp_and(($z >> 8), 0x0ff)));
            $str3 = chr(gmp_strval(gmp_and($z, 0x0ff)));

            $hasil = $hasil . $str1;
            if ( $x3 != "%" )
                $hasil = $hasil . $str2;
            if ( $x4 != "%" )
                $hasil = $hasil . $str3;
            $str = substr($str, 4, strlen($str) - 4);
        }

        return $hasil;
    }

    private function hitung_cek_digit ($x) {

        $n = 0;
        for ( $i = 0; $i < strlen($x); $i++ ) {
            $n = $n ^ ord(substr($x, $i, 1));
        }

        return $n;
    }

    private function decryptRSA2 ($cipher) {
        $d = 3691; //pr
        $n = 5561; // mod
        $plain = "";
        for ( $i = 0; $i < strlen($cipher); $i += 4 ) {
            $temp1 = ord(substr($cipher, $i, 1));
            $temp2 = ord(substr($cipher, $i + 1, 1));
            $temp3 = ord(substr($cipher, $i + 2, 1));
            $temp4 = ord(substr($cipher, $i + 3, 1));
            $n1 = ($temp1 << 8) + $temp2;
            $n2 = ($temp3 << 8) + $temp4;

            $p1 = 1;
            for ( $j = 1; $j <= $d; $j++ ) {
                $p1 = $p1 * $n1;
                $p1 = $p1 % $n;
            }
            $p2 = 1;
            for ( $j = 1; $j <= $d; $j++ ) {
                $p2 = $p2 * $n2;
                $p2 = $p2 % $n;
            }

            $pa = gmp_strval((gmp_and($p1, 0xff0)) >> 4);
            $pb = gmp_strval((gmp_and($p1, 0xf) << 4) + (gmp_and($p2, 0xf00) >> 8));
            $pc = gmp_strval(gmp_and($p2, 0x0ff));

            if ( $pc == 255 && $pb == 255 ) {
                $plain = $plain . chr($pa);
            } else if ( $pc == 255 ) {
                $plain = $plain . chr($pa) . chr($pb);
            } else {
                $plain = $plain . chr($pa) . chr($pb) . chr($pc);
            }
        }

        return $plain;

    }

    private function encryptRSA ($plain) {
        $e = 433; //pb
        $n = 4187;  // mod
        $cipher = "";
        $x = 3 - (strlen($plain) % 3);

        if ( $x == 3 )
            $x = 0;
        $plain = $plain . chr(255) . chr(255) . chr(255);
        $plain = substr($plain, 0, strlen($plain) - 3 + $x);

        for ( $i = 0; $i < strlen($plain); $i += 3 ) {
            $temp1 = ord(substr($plain, $i, 1));
            $temp2 = ord(substr($plain, $i + 1, 1));
            $temp3 = ord(substr($plain, $i + 2, 1));

            $n1 = ($temp1 << 4) + (gmp_and($temp2, 0xf0) >> 4);
            $n2 = (gmp_and($temp2, 0x0f) << 8) + $temp3;
            $c1 = 1;
            for ( $j = 1; $j <= $e; $j++ ) {
                $c1 = $c1 * $n1;
                $c1 = $c1 % $n;
            }
            $c2 = 1;
            for ( $j = 1; $j <= $e; $j++ ) {
                $c2 = $c2 * $n2;
                $c2 = $c2 % $n;
            }

            $c1a = chr(gmp_strval(gmp_and($c1, 0xff00) >> 8));
            $c1b = chr(gmp_strval(gmp_and($c1, 0x00ff)));

            $c2a = chr(gmp_strval(gmp_and($c2, 0xff00) >> 8));
            $c2b = chr(gmp_strval(gmp_and($c2, 0x00ff)));

            $cipher = $cipher . $c1a . $c1b . $c2a . $c2b;
        }

        return $cipher;
    }

    private function cek_digit ($str) {
        $x = 0;
        for ( $i = 0; $i < strlen($str); $i++ ) {
            $x = $x ^ ord(substr($str, $i, 1));
        }
        $str = $str . chr($x);

        return $str;
    }

    private function encode64 ($str) {
        $base = "ABCDEabcde012345FGHIJKLMNOfghijklmno6789PQRSTUVWXYZpqrstuvwxyz+/";
        $hasil = "";
        $n = 0;
        while ( strlen($str) > 0 ) {
            $n = strlen($str);
            if ( $n > 3 )
                $n = 3;
            $x1 = 0;
            for ( $i = 0; $i < $n; $i++ ) {
                $x1 += ord(substr($str, $i, 1)) * gmp_strval(gmp_pow(256, 2 - $i));
            }

            $temp1 = $x1 >> 18;
            $temp2 = gmp_strval(gmp_and($x1, 0x3FFFF) >> 12);
            $temp3 = gmp_strval(gmp_and($x1, 0xFFF) >> 6);
            $temp4 = gmp_strval(gmp_and($x1, 0x03F));
            if ( $n == 3 )
                $hasil = $hasil . substr($base, $temp1, 1) . substr($base, $temp2, 1) . substr($base, $temp3, 1) . substr($base, $temp4, 1);
            if ( $n == 2 )
                $hasil = $hasil . substr($base, $temp1, 1) . substr($base, $temp2, 1) . substr($base, $temp3, 1) . "%";
            if ( $n == 1 )
                $hasil = $hasil . substr($base, $temp1, 1) . substr($base, $temp2, 1) . "%%";
            $str = substr($str, $n, strlen($str) - $n);
        }
        $n = strlen($hasil) / 2;
        $hasil = substr($hasil, $n, strlen($hasil) - $n) . substr($hasil, 0, $n);

        return $hasil;
    }

    public function encrypt ($plain) {
        if ( is_array($plain) || is_object($plain) ) {
            $plain = json_encode($plain);
        }

        $str = $this->encryptRSA($plain);
        $str = $this->cek_digit($str);
        $str = $this->encode64($str);

        return $str;
    }

    public function decrypt ($cipher) {
        $n = 1;
        $plain = "";

        $str = $this->decode64($cipher);
        $n = $this->hitung_cek_digit($str);

        if ( $n == 0 ) {
            $str = substr($str, 0, strlen($str) - 1);
            $str = $this->decryptRSA2($str);
            $plain = $str;
        }

        return $plain;
    }

    public function encrypt_password ($plain, $student = false) {
        $tanggal = date('d');
        $key = ($student) ? "!5tud!3nts" : "";

        $plain = md5(md5($plain . $key));
        $hash = hash('sha256', $tanggal . md5('XyZ!' . $plain)) . 'aBc$';

        return substr($hash, 33, 32) . substr($hash, 0, 32);
    }
}