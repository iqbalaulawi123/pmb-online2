<!-- bloc-7 -->
<div class="bloc bgc-white l-bloc" id="bloc-7">
	<div class="container bloc-sm">
		<div class="row">
			<div class="col-sm-12">
				<h2 class="text-center mg-md  tc-army-green">
					Panduan Registrasi Online
				</h2>
			</div>
		</div>
		<div class="row voffset">
			<div class="col-sm-3">
				<a href="#"><img src="img/lazyload-ph.png" data-src="img/img_1.png" class="img-responsive center-block lazyload" /></a>
				<h3 class="mg-md  text-center">
					1. Calon Mahasiswa Mendaftar Online
				</h3>
				<p class=" text-center">
					Daftar melalui website&nbsp;<a class="ltc-bleu-de-france" href="pendaftaran.budiluhur.ac.id" target="_blank">pendaftaran.budiluhur.ac.id</a>
				</p>
			</div>
			<div class="col-sm-3">
				<img src="img/lazyload-ph.png" data-src="img/img_2.png" class="img-responsive center-block lazyload" />
				<h3 class="mg-md  text-center">
					2. Calon Mahasiswa membayar uang pendaftaran melalui Bank Mandiri
				</h3>
				<p class=" text-center">
					Setelah mendaftar online Calon Mahasiswa membayar biaya pendaftaran melalui Bank Mandiri sesuai dengan nomor test yang didapat. (Panduan Pembayaran dapat dilihat&nbsp;<a class="ltc-bleu-de-france" href="https://pendaftaran.budiluhur.ac.id/panduan-pembayaran.pdf" target="_blank">disini</a>)
				</p>
			</div>
			<div class="col-sm-3">
				<img src="img/lazyload-ph.png" data-src="img/img_3.png" class="img-responsive center-block lazyload" />
				<h3 class="mg-md  text-center">
					3. Calon mahasiswa diharuskan melengkapi data ke Bagian Registrasi
				</h3>
				<p class=" text-center">
					Datang ke Kampus Universitas Budi Luhur untuk melengkapi data - data yang dibutuhkan
				</p>
			</div>
			<div class="col-sm-3">
				<img src="img/lazyload-ph.png" data-src="img/img_4.png" class="img-responsive center-block lazyload" />
				<h3 class="mg-md  text-center">
					4. Calon Mahasiswa diwajibkan mengikuti test ujian masuk
				</h3>
				<p class=" text-center">
					Setelah selesai melengkapi data, Calon Mahasiswa mengikuti ujian masuk sesuai dengan jadwal yang telah ditentukan saat melengkapi dokumen
				</p>
			</div>
		</div>
	</div>
</div>
<!-- bloc-7 END -->

<!-- bloc-8 -->
<div class="bloc bgc-white l-bloc" id="bloc-8">
	<div class="container bloc-sm">
		<div class="row">
			<div class="col-sm-12">
				<h4 class="mg-md  text-center">
					Apabila ada pertanyaan lebih lanjut silahkan klik&nbsp;<a class="ltc-bleu-de-france" href="index.php?page=faq">disini</a><br>
				</h4>
				<h4 class="mg-md text-center ">
					Panduan Pembayaran dapat dilihat / di download&nbsp;<a class="ltc-bleu-de-france" href="https://drive.google.com/file/d/0B81I8km1NaasNm5IZEswQjN6bkk/view">disini</a>
				</h4>
			</div>
		</div>
	</div>
</div>
<!-- bloc-8 END -->