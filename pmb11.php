<!-- bloc-12 -->
<div class="bloc bgc-white l-bloc" id="bloc-12">
    <div class="container bloc-sm">
        <div class="row">
            <div class="col-sm-12">
                <form id="form_3111" action="" method="POST" novalidate
                    success-msg="Your message has been sent."
                    fail-msg="Sorry it seems that our mail server is not responding, Sorry for the inconvenience!">
                    <h3 class="text-center mg-lg  tc-army-green">
                        Cek Status Pembayaran
                    </h3>
                    <div class="form-group">
                        <label>
                            Nomor Pendaftaran
                        </label>
                        <input class="form-control" type="text" name="nodaftar" required id="nodaftar" />
                    </div>
                    <button id="submit" class="bloc-button btn btn-lg btn-block btn-bleu-de-france" type="submit">
                        Kirim
                    </button>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- bloc-12 END -->
<!-- bloc-12 -->
<div class="bloc bgc-white l-bloc" id="bloc-12">
    <div class="container bloc-sm">
        <div class="row"> 
            <div class="col-sm-12">
                <table class="table table-bordered">
                    <tr>
                        <td>Nomor Pendaftaran</td>
                        <td>1234567890</td>
                    </tr>
                    <tr>
                        <td>Nama</td>
                        <td>Ardy</td>
                    </tr>
                    <tr>
                        <td>Program Studi</td>
                        <td>Teknik Informatika</td>
                    </tr>
                    <tr>
                        <td>Fakultas</td>
                        <td>Fakultas Teknologi Informasi</td>
                    </tr>
                    <tr>
                        <td>Status Pembayaran</td>
                        <td>Sudah Membayar </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>
<!-- bloc-12 END -->
<script>
$("#submit").click(function () { // ( CD02-1 )
        var nodaftar = $("#nodaftar").val();
        $.ajax({
            url: "pmb11A.php?getdata=bayar",
            data: "nodaftar=" + nodaftar,
            cache: false,
            success: function (msg) {
                $("#ckdfak").html(msg);
            }
        });
    });
</script>