<!-- bloc-12 -->
<div class="bloc bgc-white l-bloc" id="bloc-12">
	<div class="container bloc-sm">
		<div class="row">
			<div class="col-sm-12">
				<form id="form_3111" action="pmb03A.php" method="POST" novalidate
					success-msg="Your message has been sent."
					fail-msg="Sorry it seems that our mail server is not responding, Sorry for the inconvenience!">
					<h3 class="text-center mg-lg  tc-army-green">
						Kirim Ulang Kartu Pendaftaran
					</h3>
					<div class="form-group">
						<label>
							Nomor Pendaftaran
						</label>
						<input class="form-control" type="text" name="notest" required id="notest" />
					</div>
					<div class="form-group">
						<label>
							Email
						</label>
						<input class="form-control" required id="input_2942_5167_3111" type="email" name="email" />
					</div>
				
					<button class="bloc-button btn btn-lg btn-block btn-bleu-de-france" type="submit">
						Kirim
					</button>
				</form>
			</div>
		</div>
	</div>
</div>
<!-- bloc-12 END -->
  <script>
  	<?php if(isset($_GET['success'])&&$_GET['success']==true){ ?>
  $(document).ready(function(){
	Swal.fire("Berhasil", "Silakan Periksa Email Anda ", "success")
  });
  <?php }	elseif(isset($_GET['fail'])&&$_GET['fail']==true)	 { ?>
  	$(document).ready(function(){
   Swal.fire("Gagal Mengirim", "Data yang Anda masukan tidak sesuai.<br> Mohon periksa kembali nomor pendaftaran dan email Anda  ", "error")
  });
  	<?php 	} ?>
</script>