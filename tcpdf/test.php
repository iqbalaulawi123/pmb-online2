<?php
//============================================================+
// File name   : example_061.php
// Begin       : 2010-05-24
// Last Update : 2014-01-25
//
// Description : Example 061 for TCPDF class
//               XHTML + CSS
//
// Author: Nicola Asuni
//
// (c) Copyright:
//               Nicola Asuni
//               Tecnick.com LTD
//               www.tecnick.com
//               info@tecnick.com
//============================================================+

/**
 * Creates an example PDF TEST document using TCPDF
 * @package com.tecnick.tcpdf
 * @abstract TCPDF - Example: XHTML + CSS
 * @author Nicola Asuni
 * @since 2010-05-25
 */

// Include the main TCPDF library (search for installation path).
require_once('tcpdf.php');


// membuat dokumen PDF baru
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// menambahkan informasi dokumen
$pdf->SetCreator(PDF_CREATOR);


// mengeset bahasa
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
  require_once(dirname(__FILE__).'/lang/eng.php');
  $pdf->setLanguageArray($l);
}

// ---------------------------------------------------------

// mengeset font default untuk moda subsetting
$pdf->setFontSubsetting(true);
  $pdf->setPrintHeader(false);
$pdf->setPrintFooter(true);
class MYPDF extends TCPDF {

  

    // Page footer
    public function Footer() {
        // Position at 15 mm from bottom
        $this->SetY(-15);
        // Set font
        $this->SetFont('helvetica', 'I', 8);
        // Page number
        $this->Cell(0, 10, 'Page '.$this->getAliasNumPage().'/'.$this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
    }
}

// mengeset font
$pdf->SetFont('dejavusans', '', 11, '', true);

// menambahkan halaman baru, terdapat beberapa opsi, dapat dicek di dokumentasi
$pdf->AddPage('P', 'A4');
$pdf->Cell(0, 0, 'A4 PORTRAIT', 1, 1, 'C');
 $pdf->SetMargins(0, 0, 0,0);
//$pdf->Image('Kartu Pendaftaran Universitas Budi Luhur_Header (1).jpg', 0, 0, 20, 100, 'JPG', '', '', false, 300, '', false, false, 0);
// mencetak konten ke dalam PDF


// writeHTMLCell($w, $h, $x, $y, $html='', $border=0, $ln=0, $fill=0, $reseth=true, $align='', $autopadding=true)
$html = <<<EOD
<!DOCTYPE html>

  <img width="1300" src="Kartu Pendaftaran Universitas Budi Luhur_Header (1).jpg">

EOD;

// mencetak teks menggunakan writeHTMLCell( )
$h=0;
$pdf->writeHTMLCell(217, 0 , -2 , $h, $html, 0,0 , 0, false, 'L', false);

$html= <<<EOD
<strong><p align="center" style="color:#1A3560;" >KARTU PENDAFTARAN</p></strong>

EOD;
$pdf->writeHTMLCell(217,  0, 0 , $h+=35 , $html, '',0 , 0, false, 'L', false);
// ---------------------------------------------------------
$html= <<<EOD
<p align="left" style="line-height: 110%;">
    Hi! [Nama Pendaftar] Terimakasih telah memilih Universitas Budi Luhur - Akademi Budi Luhur sebagai tempat untuk
    berkuliah, berikut adalah detail kartu pendaftaran online kamu:
</p>

EOD;
$pdf->writeHTMLCell(0,  0, 10 , $h+=25, $html, '',0 , 0, false, 'L', false);
$html= <<<EOD
<table border="0" >
    <tr>
        <td width="35%">Nomor Pendaftaran</td>
        <td width="2%">:</td>
        <td width="63%">[Nomor Pendaftar]</td>
    </tr>
    <tr>
        <td>Nama Pendaftaran</td>
        <td>:</td>
        <td>[Nama Pendaftar]</td>
    </tr>
    <tr>
        <td>Fakultas</td>
        <td>:</td>
        <td>Fakultas Teknologi Informasi</td>
    </tr>
    <tr>
        <td>Program Studi</td>
        <td>:</td>
        <td>Teknik Informatika</td>
    </tr>
    <tr>
        <td>Jenjang</td>
        <td>:</td>
        <td>Strata 1</td>
    </tr>
    <tr>
        <td>Username</td>
        <td>:</td>
        <td>[Nomor Pendaftaran]</td>
    </tr>
    <tr>
        <td>Password</td>
        <td>:</td>
        <td>[ Password ]</td>
    </tr>
    <tr>
        <td>Jenis Daftar</td>
        <td>:</td>
        <td>Jalur Umum/Reguler</td>
    </tr>

</table>

EOD;
$pdf->writeHTMLCell(150,  50, 15 , $h+=20, $html, '',0 , 0, false, 'L', false);
$html= <<<EOD

<p align="left" style="line-height: 110%;">
    Salam Budi Luhur,<br>
    <br>
    Jakarta,06 june 2018<br>
    Panitia Penerimaan Mahasiswa Baru<br>
    UNIVERSITAS BUDI LUHUR -AKADDEMI SEKRETARI BUDI LUHUR<BR>
    <hr>

</p>

EOD;
$pdf->writeHTMLCell(0, 0 , 10 , $h+=50, $html, '',0 , 0, false, 'L', false);
$html= <<<EOD
<p  style="line-height: 110%;">
    Catatan <br>
    1. Setelah mengisi biodata calon Mahasiswa harus melakukan pembayaran formulir pendaftaran sebesar Rp. 250.000,- di Bank Mandiri melalui :
</p>


EOD;
$pdf->writeHTMLCell(0, 0 , 10 , $h+=35, $html, '',0 , 0, false, 'L', false);
$html= <<<EOD

<strong>
    <p style="line-height: 110%;    color: #1A3560;" >Transfer ATM/Teller dengan cara memasukan kode perusahaan Budi Luhur = 10067</p>
</strong>


EOD;
$pdf->writeHTMLCell(0, 0 , 10 , $h+=6  , $html, '',0 , 0, false, 'L', false);
$html= <<<EOD


    <p line-height: 110%;>

    2.Calon mahasiswa diharuskan melengkapi data ke Bagian Registrasi<br>
    dengan membawa:<br>
    &nbsp;&nbsp;&nbsp;a) Fotocopy raport SMA/sederajat semester 3/4/5<br>
    &nbsp;&nbsp;&nbsp;b) Fotocopy KTP/Kartu Pelajar/SIM/Kartu Keluarga<br>
    &nbsp;&nbsp;&nbsp;c) Kartu pendaftaran Yang sudah dicetak melalui email<br>
    &nbsp;&nbsp;&nbsp;d) Bukti pembayaran uang pendaftaran

</p>



EOD;
$pdf->writeHTMLCell(0, 0 , 10 , $h+=7, $html, '',0 , 0, false, 'L', false);
$html= <<<EOD

<strong>
    <p style="line-height: 110%;    color: #1A3560;" >Informasi dan Pertanyaan, Silahkan Hubungi :<br>
Telp: (021)5853753 - Ext. 285/286/288</p>
</strong>

EOD;
$pdf->writeHTMLCell(0, 0 , 10 , $h+=45  , $html, '',0 , 0, false, 'L', false);
// Menutup dan mengeluarkan dokumen PDF
// $html = <<<EOD
// <!DOCTYPE html>
//
//   <img width="1300" src="Kartu Pendaftaran Universitas Budi Luhur_Footer (1).jpg">
//
// EOD;

// mencetak teks menggunakan writeHTMLCell()

// $pdf->writeHTMLCell(217, 0 , -2 , $h+39, $html, 0,0 , 0, false, 'L', false);
// writeHTMLCell($w, $h, $x, $y, $html='', $border=0, $ln=0, $fill=0, $reseth=true, $align='', $autopadding=true)
// $pdf->writeHTMLCell(120, 5 , 0 , 270, $html, 1, 2 , 0, false, 'L', false);
$pdf->Output('example_001.pdf', 'I');
// $html;

//============================================================+
// END OF FILE
//============================================================+
