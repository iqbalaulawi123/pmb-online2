<?php
//============================================================+
// File name   : example_003.php
// Begin       : 2008-03-04
// Last Update : 2013-05-14
//
// Description : Example 003 for TCPDF class
//               Custom Header and Footer
//
// Author: Nicola Asuni
//
// (c) Copyright:
//               Nicola Asuni
//               Tecnick.com LTD
//               www.tecnick.com
//               info@tecnick.com
//============================================================+

/**
 * Creates an example PDF TEST document using TCPDF
 * @package com.tecnick.tcpdf
 * @abstract TCPDF - Example: Custom Header and Footer
 * @author Nicola Asuni
 * @since 2008-03-04
 */

// Include the main TCPDF library (search for installation path).
require_once('tcpdf.php');


// Extend the TCPDF class to create custom Header and Footer
class MYPDF extends TCPDF {

    //Page header
    public function Header() {
        // Logo
        $image_file = 'Kartu Pendaftaran Universitas Budi Luhur_Header (1).jpg';
        $this->Image($image_file, 0, 0, 210, '', 'JPG', '', 'T', false, 400, '', false, false, 0, false, false, false);

    }

    // Page footer
    public function Footer() {
        // Position at 15 mm from bottom
        $this->SetY(-30);
        // Set font
        $this->SetFont('helvetica', 'I', 8);
        // Page number
        $image_file = 'Kartu Pendaftaran Universitas Budi Luhur_Footer (1).jpg';
        $this->Image($image_file, 0, '', 210, '', 'JPG', '', 'T', false, 400, '', false, false, 0, false, false, false);
    }
}

// create new PDF document
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Nicola Asuni');
$pdf->SetTitle('TCPDF Example 003');
$pdf->SetSubject('TCPDF Tutorial');
$pdf->SetKeywords('TCPDF, PDF, example, test, guide');

// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
    require_once(dirname(__FILE__).'/lang/eng.php');
    $pdf->setLanguageArray($l);
}

// ---------------------------------------------------------

// set font
$pdf->SetFont('dejavusans', '', 11, '', true);


// add a page
//p for potrait
// l for landscape

$pdf->AddPage('P');

// set some text to print
$h=0;


$html= <<<EOD
<strong><p align="center" style="color:#1A3560;" >KARTU PENDAFTARAN</p></strong>

EOD;
$pdf->writeHTMLCell(217,  0, 0 , $h+=35 , $html, '',0 , 0, false, 'L', false);
// ---------------------------------------------------------
$html= <<<EOD
<p align="left" style="line-height: 110%;">
    Hi! [Nama Pendaftar] Terimakasih telah memilih Universitas Budi Luhur - Akademi Budi Luhur sebagai tempat untuk
    berkuliah, berikut adalah detail kartu pendaftaran online kamu:
</p>

EOD;
$pdf->writeHTMLCell(0,  0, 10 , $h+=25, $html, '',0 , 0, false, 'L', false);
$html= <<<EOD
<table border="0" >
    <tr>
        <td width="35%">Nomor Pendaftaran</td>
        <td width="2%">:</td>
        <td width="63%">[Nomor Pendaftar]</td>
    </tr>
    <tr>
        <td>Nama Pendaftaran</td>
        <td>:</td>
        <td>[Nama Pendaftar]</td>
    </tr>
    <tr>
        <td>Fakultas</td>
        <td>:</td>
        <td>Fakultas Teknologi Informasi</td>
    </tr>
    <tr>
        <td>Program Studi</td>
        <td>:</td>
        <td>Teknik Informatika</td>
    </tr>
    <tr>
        <td>Jenjang</td>
        <td>:</td>
        <td>Strata 1</td>
    </tr>
    <tr>
        <td>Username</td>
        <td>:</td>
        <td>[Nomor Pendaftaran]</td>
    </tr>
    <tr>
        <td>Password</td>
        <td>:</td>
        <td>[ Password ]</td>
    </tr>
    <tr>
        <td>Jenis Daftar</td>
        <td>:</td>
        <td>Jalur Umum/Reguler</td>
    </tr>

</table>

EOD;
$pdf->writeHTMLCell(150,  50, 15 , $h+=20, $html, '',0 , 0, false, 'L', false);
$html= <<<EOD

<p align="left" style="line-height: 110%;">
    Salam Budi Luhur,<br>
    <br>
    Jakarta,06 june 2018<br>
    Panitia Penerimaan Mahasiswa Baru<br>
    UNIVERSITAS BUDI LUHUR -AKADDEMI SEKRETARI BUDI LUHUR<BR>
    <hr>

</p>

EOD;
$pdf->writeHTMLCell(0, 0 , 10 , $h+=50, $html, '',0 , 0, false, 'L', false);
$html= <<<EOD
<p  style="line-height: 110%;">
    Catatan <br>
    1. Setelah mengisi biodata calon Mahasiswa harus melakukan pembayaran formulir pendaftaran sebesar Rp. 250.000,- di Bank Mandiri melalui :
</p>


EOD;
$pdf->writeHTMLCell(0, 0 , 10 , $h+=35, $html, '',0 , 0, false, 'L', false);
$html= <<<EOD

<strong>
    <p style="line-height: 110%;    color: #1A3560;" >Transfer ATM/Teller dengan cara memasukan kode perusahaan Budi Luhur = 10067</p>
</strong>


EOD;
$pdf->writeHTMLCell(0, 0 , 10 , $h+=6  , $html, '',0 , 0, false, 'L', false);
$html= <<<EOD


    <p line-height: 110%;>

    2.Calon mahasiswa diharuskan melengkapi data ke Bagian Registrasi<br>
    dengan membawa:<br>
    &nbsp;&nbsp;&nbsp;a) Fotocopy raport SMA/sederajat semester 3/4/5<br>
    &nbsp;&nbsp;&nbsp;b) Fotocopy KTP/Kartu Pelajar/SIM/Kartu Keluarga<br>
    &nbsp;&nbsp;&nbsp;c) Kartu pendaftaran Yang sudah dicetak melalui email<br>
    &nbsp;&nbsp;&nbsp;d) Bukti pembayaran uang pendaftaran

</p>



EOD;
$pdf->writeHTMLCell(0, 0 , 10 , $h+=7, $html, '',0 , 0, false, 'L', false);
$html= <<<EOD

<strong>
    <p style="line-height: 110%;    color: #1A3560;" >Informasi dan Pertanyaan, Silahkan Hubungi :<br>
Telp: (021)5853753 - Ext. 285/286/288</p>
</strong>

EOD;
$pdf->writeHTMLCell(0, 0 , 10 , $h+=45  , $html, '',0 , 0, false, 'L', false);

// print a block of text using Write()


// ---------------------------------------------------------

//Close and output PDF document
$pdf->Output('example_003.pdf', 'I');

//============================================================+
// END OF FILE
//============================================================+
