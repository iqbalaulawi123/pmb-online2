
<!-- bloc-13 -->
<div class="bloc bgc-white l-bloc" id="bloc-13">
	<div class="container bloc-sm">
		<div class="row">
			<div class="col-sm-12">
				<h2 class="text-center mg-md  tc-army-green">
					Info Fakultas
				</h2>
			</div>
		</div>
	</div>
</div>
<!-- bloc-13 END -->

<!-- bloc-14 -->
<div class="bloc bgc-white l-bloc" id="bloc-14">
	<div class="container bloc-sm">
		<div class="row">
			<div class="col-sm-3">
				<a class="btn btn-lg btn-block btn-clean btn-ultramarine-blue" onclick="scrollToTarget('#fakultas-teknologi-informasi')">Fakultas Teknologi Informasi</a><a class="btn btn-lg btn-block btn-clean btn-mikado-yellow" onclick="scrollToTarget('#fakultas-ilmu-komunikasi')">Fakultas Ilmu Komunikasi</a>
			</div>
			<div class="col-sm-3">
				<a class="btn btn-lg btn-block btn-clean btn-mountain-meadow" onclick="scrollToTarget('#fakultas-ekonomi-bisnis')">Fakultas Ekonomi &amp; Bisnis</a><a class="btn btn-lg btn-block btn-clean btn-neon-carrot" onclick="scrollToTarget('#fakultas-ilmu-sosial-politik')">Fakultas Ilmu Sosial &amp; Politik</a>
			</div>
			<div class="col-sm-3">
				<a class="btn btn-lg btn-block btn-clean btn-liver" onclick="scrollToTarget('#fakultas-teknik')">Fakultas Teknik</a><a class="btn btn-lg btn-block btn-clean btn-dark-midnight-blue" onclick="scrollToTarget('#pascasarjana')">Pascasarjana</a>
			</div>
			<div class="col-sm-3">
				<a class="btn btn-lg btn-block btn-clean btn-bleu-de-france" onclick="scrollToTarget('#diploma-3-d3-')">Diploma 3 (D3) Unggulan</a><a class="btn btn-lg btn-block btn-clean btn-deep-chestnut" onclick="scrollToTarget('#akademi-sekretari')">Akademi Sekretari</a>
			</div>
		</div>
	</div>
</div>
<!-- bloc-14 END -->

<!-- bloc-15 -->
<div class="bloc bgc-anti-flash-white l-bloc" id="bloc-15">
	<div class="container bloc-md">
		<div class="row bounce">
			<div class="col-sm-12">
				<div class="embed-responsive embed-responsive-16by9">
					<video controls class="embed-responsive-item lazyload">
						<source src="vid/TVC%20UBL%20%28Versi%20Narasi%29.mp4" type="video/mp4" />Your browser does not support HTML5 video.
					</video>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- bloc-15 END -->

<!-- fakultas-teknologi-informasi -->
<div class="bloc bgc-white tc-army-green l-bloc" id="fakultas-teknologi-informasi">
	<div class="container bloc-md">
		<div class="row">
			<div class="col-sm-12">
				<h3 class="mg-md  tc-ultramarine-blue">
					Fakultas Teknologi Informasi
				</h3><img src="img/lazyload-ph.png" data-src="img/headerjpg-11.jpg" class="img-responsive lazyload" />
				<p>
					<strong><br>Teknik Informatika</strong> <br><br>Menyiapkan tenaga - tenaga profesional yang berbudi luhur dalam bidang rekayasa perangkat lunak dengan kurikulum yang disesuaikan dengan perkembangan dunia informatika terkini dalam bidang Pemrograman, Database, Jaringan Komputer dan Security<br><br>Kompetensi Network and Web Security<br>Mampu mendesain dan mengimplementasikan keamanan jaringan dan aplikasi berbasis web sesuai dengan kebutuhan<br><br>Kompetensi Programming Expert<br>Mampu mendesain dan mengimplementasikan algoritma untuk kebutuhan yang dikompleksitasnya tinggi<br><br>Prospek Kerja Teknik Informatika<br><i>Software Developer, Web Developer, IT Consultant, IT Program/Project Manager, UI and UX Designer, Computer Programmers, Mobile Application Developer, Game Developer, Network Engineer, Computer Forensic Investigator, System Administrator, Database Administrator, Security Specialist <br></i><br><br><strong>Sistem Informasi</strong> <br><br>Mampu merancang bangun sistem informasi dari skala kecil hingga enterprise, dari perencanaan, analisa kondisi saat ini, analisa kebutuhan, analisa sistem yang akan datang, impementasi, testing serta maintenance dengan menggunakan konsep dan tool - toolnya<br><br>Peminatan Enterprise System<br>Mampu mendesain dan mengimplementasikan keamanan jaringan dan aplikasi berbasis web sesuai dengan kebutuhan<br><br>Peminatan Technopreneurship<br>Mampu merancang bangun sistem informasi perdagangan berbasis elektronik/internet (e-commerce) beserta proses marketing dan periklanannya dengan menggunakan multi channel / social media serta dapat membuat usahanya sendiri (enterpreneur).<br><br>Prospek Kerja Sistem Informasi<br><i>Software Developer, Web Developer, IT Consultant, IT Program/Project Manager, UI and UX Designer, Computer Programmers, Mobile Application Developer, Game Developer, Network Engineer, Computer Forensic Investigator, System Administrator, Database Administrator, Security Specialist</i> <br><br><br><strong>Sistem Komputer</strong> <br><br>Program Studi Sistem Komputer menghasilkan lulusan yang ahli dalam bidang rekayasa komputer dn kontrol, Rekayasa jaringan komputer statis kejaringan yang bergerak (mobile), rekayasa robotika dari sistem mekanik ke sistem yang lebih pintar (intelegent), serta mampu menganalisa, mendesain dan mengendalikan jaring komputer dengan spektrum yang sangaat beragam.<br><br>Prospek Kerja Sistem Komputer<br><i>Ahli dibidang rekayasa sistem kontrol industri (System Engineer), Ahli dibidang rekayasa jaringan komputer (Network Engineer), ahli dibidang computer based (Computer Specialist), Engineering Consultant(Problem Solution), ahli dibidang riset dan pengembangan sistem berbasis komputer (R&amp;D Engineer)</i><br>
				</p>
			</div>
		</div>
	</div>
</div>
<!-- fakultas-teknologi-informasi END -->

<!-- fakultas-ilmu-komunikasi -->
<div class="bloc bgc-white tc-army-green l-bloc" id="fakultas-ilmu-komunikasi">
	<div class="container bloc-sm">
		<div class="row">
			<div class="col-sm-12">
				<h3 class="mg-md  tc-mikado-yellow">
					Fakultas Ilmu Komunikasi
				</h3><img src="img/lazyload-ph.png" data-src="img/headerjpg-12.jpg" class="img-responsive lazyload" />
				<p>
					<strong><br>Broadcast Journalism</strong><br><br>Peserta studi akan diarahkan menjadi seorang Ahli Komunikasi sekaligus #siapkreatif untuk melahirkan produk-produk jurnalistik penyiaran yang dibutuhkan masyarakat dan tentu sesuai kode etik jurnalistik. Konsep dan teori komunikasi yang diterima peserta studi akan ditambah dengan pemahaman konsep jurnalistik penyiaran. Mengikuti perkembangan teknologi komunikasi saat ini, peserta studi tidak hanya diajak untuk buktikan #siapkreatif dalam melahirkan produk jurnalistik untuk televisi, radio melainkan juga mewujudkan produk jurnalistik untuk new media. Peserta studi dipersiapkan untuk dapat masuk ke industri penyiaran masa depan seperti menjadi : Reporter, Camera Person, News Anchor, News Producer, Program Director. Selain itu menghadapi era new media, peserta studi juga akan dirancang menjadi seorang multi talenta, seperti Video Journalist sekaligus siap menjadi pengelola portal berita online.<br><br>Prospek Kerja Broadcast Journalism<br><i>Producer TV dan Radio, Video Journalist, Camera Person, Reporter, Anchor, Video Editor, Penyiar Radio, Produser Program Radio, Program Director, Station Manager/Kepala Stasiun Radio, Pengelola Radio Komunitas, Pengelola Website Berita / Portal Berita, Pengelola Media Online</i><br><br><br><strong>Public Relation</strong><br><br>Mahasiswa diharapkan menjadi Ahli Komunikasi dengan kompetensi melahirkan solusi dan strategi - strategi Public Relation yang berguna bagi lembaga/organisasi, baik pemerintah atau swasta, juga untuk kebutuhan personalbranding seorang profesional. Mengikuti perkembangan teknik komunikasi saat ini, mahasiswa juga akan dibekali kemampuan mengelola teknik humas di new media, seperti pengelolaan social media, pemanfaatan new media sebagai sarana penyampaian pesan (komunikasi publik)<br><br>Prospek Kerja Public Relations<br><i>Lulusannya mampu berkarir di bidang PR Officer, juru Bicara, Event Supervisor, Pengelola Media Internal, Campaign Manager, Personal Branding Consultant.</i><br><br> <br><strong>Visual Communication</strong> <br><br>Melalui peminatan ini, peserta studi akan diajak untuk dapat menjadi seorang Ahli Komunikasi yang multi talenta dalam penerapannya di ruang lingkup Komunikasi Visual. Peserta studi akan dipersiapkan untuk terjun ke dalam industri kreatif yang peluang berkembangnya saat ini sangat besar apalagi didukung dengan program pemerintah. Peminatan ini akan fokus membentuk peserta studi agar #siapkreatif melahirkan karya komunikasi visual yang bermanfaat untuk masyarakat seperti mampu mengantisipasi persoalan komunikasi dalam masyarakat melalui visualisasi. Lulusan nantinya diharapkan menjadi Digital Animator, Sinematografer, Web Designer, Game Designer, Digital Illustrator, tim Creative Design. Hasil karya peserta studi berupa desain, animasi, infogras, videogras, motion graphic diharapkan dapat dimanfaatkan untuk media televisi, media internet, media game bahkan juga media lm <br><br>Prospek Kerja Visual Communication<br><i>Kreator Desain, Creative Director (Industri Media, Periklanan), Konseptor Kegiatan Promosi, Ilustrator.</i><br><br> <br><strong>Digital Adversiting</strong> <br><br>Industri periklanan semakin berkembang, tidak hanya menyajikan produk dalam bentuk media cetak ataupun media konvensional seperti radio dan televisi. Industri periklanan saat ini juga telah bergerak masuk ke dalam ranah new media, dengan pola masuk ke dalam mobile ads. Peminatan ini juga berharap peserta studi akan mampu membuat konsep periklanan baru yang #siapkreatif dan kompetitif, sehingga mampu bersaing. Peserta studi akan dipersiapkan menjadi Ahil Komunikasi di ruang lingkup periklanan, yang tidak hanya mampu mengelola pesan kreatif dalam bentuk karya cipta iklan. Tetapi, juga peserta studi akan diarahkan mampu melahirkan konsep komunikasi pemasaran yang lebih luas. <br><br>Prospek Kerja Digital Advertising<br><i>MarComm Ocer, Brand Supervisor, Event Supervisor, Konseptor Media Campaign, Strategic Planner.</i><br><br> <br><strong>Komunikasi Pariwisata</strong> <br><br>Peminatan Komunikasi Pariwisata merupakan salah satu konsentrasi dari Prodi Ilmu Komunikasi dibuka untuk mahasiswa yang berminat dibidang Komunikasi Pariwisata, yang didasari oleh Ilmu Komunikasi dan penggunaan Teknologi di bidang Informasi. Lulusan peminatan komunikasi pariwisata diharapkan dapat menjawab dan mengantisipasi permasalahan yang berkaitan dengan Pariwisata, baik persoalan Pemasaran, Promosi, Brand dan pencitraan, pengelolaan kelembagaan dan media yang digunakan untuk perencanaan dan pengembangan pariwisata <br><br>Prospek Kerja Komunikasi Pariwisata<br><i>Analis Bidang Pariwisata, Content Writing, Pengelola Promosi Pariwisata, Event Planner, Konsultan Bidang Pariwisata.</i><br><br> <br><strong>Digital Media / New Media</strong> <br><br>Perkembangan teknologi informasi dan komunikasi yang saat ini sangat cepat dan ditunjukkan dengan pemanfaatan teknologi internet dan new media yang semakin tinggi, menciptakan berkembangnya pola komunikasi. Dalam konsentrasi baru ini, peserta studi akan diajak untuk melaksanakan kajian terhadap hadirnya new media, mengembangkan konsep kreatif aplikasi new media, selain itu juga melakukan analisis konsep tur aplikasi new media dan cara penggunaannya. Melalui konsentrasi ini, peserta studi yang termasuk dalam generasi millenials, akan diajak untuk mengembangkan ide kreatif komunikasi melalui new media. Pemanfaatan social media, pengembangan social media, optimalisasi penyampain pesan dalam konvergensi media. <br><br>Prospek Kerja Digital Media / New Media<br><i>Social Media Specialist, Content Writer, Direktur Kreatif, Penulis/kritikus Media, Kolaborator Kreatif, Pengembang Bisnis Kreatif, Manajer Media.</i><br>
				</p>
			</div>
		</div>
	</div>
</div>
<!-- fakultas-ilmu-komunikasi END -->

<!-- fakultas-ekonomi-bisnis -->
<div class="bloc bgc-white tc-army-green l-bloc" id="fakultas-ekonomi-bisnis">
	<div class="container bloc-sm">
		<div class="row">
			<div class="col-sm-12">
				<h3 class="mg-md  tc-mountain-meadow">
					Fakultas Ekonomi &amp; Bisnis
				</h3><img src="img/lazyload-ph.png" data-src="img/headerjpg-13.jpg" class="img-responsive lazyload" />
				<p>
					<strong><br>Akuntansi</strong><br><br>Konsentrasi<br>- Akuntansi Manajemen<br>- Pengauditan<br>Dalam beberapa dekade terakhir, globalisasi bisnis menyebabkan munculnya persaingan dan berkembangnya teknologi. hal ini memaksa para pelaku bisnis mengevaluasi praktik bisnis mereka melalui akutansi dengan menggunakan laporan keuangan<br><br>Program Studi Akuntansi menyiapkan mahasiswa memiliki kemampuan mengolah data keuangan dan non keuangan menjadi informasi dalam bentuk laporan keuangan yang sesuai dengan prinsip - prinsip akuntansi yang berlaku umum, dan juga mampu memanfaatkan laporan keuangan untuk pengambilan keputusan secara cepat dan tepat.<br><br>Prospek Kerja Akuntansi<br><i>Konsultan Akuntansi &amp; Perpajakan, Auditor di kantor, Akuntan Publik, Auditor Pemerintah, Auditor Internal Perusahaan, Financial Planner, Financial Analyst Financial Consultant, Marketing Manager, Investment Manager, HRD Manager, Wirausahawan</i><br><br> <br><strong>Manajemen</strong><br><br>Konsentrasi<br>- Manajemen Keuangan<br>- Manajemen Pemaasaran<br>- Manajemen Sumber Daya Manusia<br>Program Studi Manajemen Menyiapkan mahasiswa untuk dapat mengembangkan diri menjadi profesional atau wirausahawan yang berintegritas, berintelektual, memiliki kemampuan berkomunikasi, daya analisis dalam pengambian keputusan dan penyelsaian permasalahan dalam bidang bisnis di passar domestik maupun global dengan berlandaskan pada nilai - nilai kebudiluhuran.<br> <br>Prospek Kerja Manajemen&nbsp;<br>Konsultan Akuntansi &amp; Perpajakan, Auditor di kantor Akuntan Publik, Auditor Pemerintah, Auditor Internal Perusahaan, Financial Planner, Financial Analyst, Financial Consultant, Marketing Manager, Investment Manager, HRD Manager, Wirausahawan<br><br><br><strong>International Bussines &amp; Management Studies</strong><br><br>Tidak dapat dipungkiri bahwa kuliah di luar negeri telah menjadi impian banyak orang. Hal ini dikarenakan selain menawarkan pendidikan Yang berkualitas, perguruan tinggi luar negeri dikenal telah menghasilkan lulusan dengan wawasan dan kualiökasi internasional sehingga memiliki daya saing yang tinggi untuk menembus pasar kerja internasional.<br><br>Berdasarkan hal tersebut banyak orang tua yang ingin mengirimkan putera - puterinya kuliah di luar negeri, terutama negara-negara di Eropa.<br><br>Salah satu negara Eropa yang menjadi tujuan para pelajar di dunia termasuk Indonesia adalah negara Belanda. Negara ini mempunyai banyak keunikan terutama dibidang pendidikan. Dengan kata lain, pendidikan yang ada di Belanda merupakan pendidikan berkualitas tinggi dengan ragam kultur/budaya dan bahasa, yang dikenal dengan istilah polulernya pendidikan multikultural.<br><br>Sistem pendidikan Belanda memiliki reputasi dunia dari sisi kualitas yang dicapai melalui sistem regulasi dan jaminan kualitas. Hampir 90% universitas yang ada di Belanda masuk dalam jajaran universitas universitas yang ada di Belanda masuk dalam jajaran universitas of Applied Sciences.<br><br>Namun tidak semua orang memiliki kesempatan kuliah di Belanda. Banyak hal yang harus dipertimbangkan, mulai dari biaya, sampai kekuatiran orang tua terdapat kemandirian putera - puteri mereka yang masih dalam usia remaja.<br>
				</p>
			</div>
		</div>
	</div>
</div>
<!-- fakultas-ekonomi-bisnis END -->

<!-- fakultas-ilmu-sosial-politik -->
<div class="bloc bgc-white tc-army-green l-bloc" id="fakultas-ilmu-sosial-politik">
	<div class="container bloc-sm">
		<div class="row">
			<div class="col-sm-12">
				<h3 class="mg-md  tc-neon-carrot">
					Fakultas Ilmu Sosial &amp; Politik
				</h3><img src="img/lazyload-ph.png" data-src="img/headerjpg-14.jpg" class="img-responsive lazyload" />
				<p>
					<strong><br>Ilmu Hubungan Internasional</strong><br><br>Perkembangan dunia internasional akan sangan berpengaruh pada lingkup kehidupan nasional suatu negara. ini disebabkan oleh kecenderungan dunia internasional dan mempengaruhi satu sama lainnya.<br><br>Program Studi Hubungan Internasional akan menjadi pusat pendidikan dan penelitian serta pengabdian kepada masyarakat baik di bidang diplomatik, studi kawasan, maupun ekonomi politik internasional.<br><br>Prospek Kerja Ilmu Hubungan Internasional<br><i>Sarjana Ilmu Hubungan Internasional dapat berkiprah di berbagai sektor pemerintah, nin pemerintah (LSM) Tingkat nasional/Internasional, Perusahaan asing maupun nasional, sebagai akademisi atau peneliti yang terkait dengan hubungan internasional, lembaga swasta yang memiliki devisi hubungan internasional, lembaga keuangan internasional, Internasional Relationship, Jurnalis Internasional, Aktivis Non Govermental Organization(NGO),Diplomat, Atase Negara dan Staff Organisasi Internasional seperti UNDP,UNESCO,UNICEF</i><br><br><br><strong>Kriminologi</strong><br><br>Program Studi Kriminologi merupakan studi multidisiplin ilmu - ilmu sosial yang terkait erat dengan sosiologi, psikologi, forensik, antropologi sosial, hukum dan teknologi informasi. studi ini mempelajari berbagai penyebab kejahatan dari sudut pandang pelaku, korban, dan reaksi masyarakat. Begitu juga upaya kontrol serta pencegahan kejahatan baik dalam tingkatan individu maupun sosial.<br><br>Cyber Crime<br>Program Cyber Crime mengajarkan penerapan aspek pengaman siber, jaringan komputer, dan informasi. Mengkaji konsep - konsep teknis terkait pembuatan kode rahasisa (encryption), mendeteksi penyusup pada sebuah system dan kejahatan cyber lainnya. Lulusannya dibekali pengenalan metode, teknik pertahanan, dan tindakan perlawanannya<br><br>Crime Journalism Investigation<br>Program Crime Journalism Investigation menekankan studi jurnalistik dalam lingkup peliputan peristiwa kejahatan.lulusannya dibekali pengetahuan dan ketrampilan menghimpun data melalui teknik investigasi untuk mengupas fakta kriminal, serta melaporkan kepada publik dalam bentuk berita.<br><br>Prospek Kerja Kriminologi<br><i>Reporter,Presenter,Editor Media, Scriptwriter TV khususnya berita kriminal,Penyelidik dan Penyelidik Kepolisian, Konsultan Publik dan Privat di bidang pengaman, staf ahli, Investigator publik &amp; Privat, aktivis LSM, Peneliti dan Pengajar, Petugas lapas, Analisa keamana sber, Spesialis Forensik, Pekerja Sosial, Penulis, Lembaga Pemerintah non Pemerintah seperti LPSK, Kompoolnas,BNN,DLL</i><br>
				</p>
			</div>
		</div>
	</div>
</div>
<!-- fakultas-ilmu-sosial-politik END -->

<!-- fakultas-teknik -->
<div class="bloc bgc-white tc-army-green l-bloc" id="fakultas-teknik">
	<div class="container bloc-sm">
		<div class="row">
			<div class="col-sm-12">
				<h3 class="mg-md  tc-liver">
					Fakultas Teknik
				</h3><img src="img/lazyload-ph.png" data-src="img/headerjpg-15.jpg" class="img-responsive lazyload" />
				<p>
					<strong><br>Teknik Elektro</strong><br><br>Menghasilkan Sarjana Teknik Yang cerdas dan berbudi luhur serta mampu bersaing di pasar kerja maupun wirausaha di bidang teknik elektro.<br><br>Konsentrasi Teknik Telekomunikasi<br>Menekankan pada penguasaan sistem dan teknologi telekomunikasi berbasis multimedia.<br><br>Konsentrasi Teknik Kontrol<br>Menekankan pada penguasaan teknologi sistem kontrol dan otomasi industri berbasis komputer, Programmable Logic Controller(PLC), dan Embedded Controller.<br><br>Prospek Kerja Teknik Elektro<br><i>Telecommunication Engineer, Cellular Engineer, Broadcast Engineer,System Network Engineer, Automation Engineer, Electronic Manufacture Engineer, Electrical Engineer, Konsultan Teknik Elektro, Wirausaha Di bidang Teknik Elektro, Pengajar dan Peneliti, Birokrasi(Pemerintahan).</i><br><br><br><strong>Arsitektur</strong><br><br>Fakultas Teknik Program Studi Arsitektur Universitas Budi Luhur Menghasilkan Sarjana Arsitektur yang cerdas berbudi luhur, dengan kemampuan menerapkan IPTEKS dalam perencanaan, perancangan dan pengelolaan lingkungan binaan, di bidang pemukiman, dengan senantiasa komunikasi. mampu melakukan penelitian dan pengembangan diri, untuk meningkatkan ketrampilan secara aplikatif dan inovatif dalam bidang arsitektur.<br><br>Prospek Kerja Arsitektur<br><i>Kebutuhan pasar terbuka luas untuk profesi arsitek baik swasta ataupun pemerintahan, seperti; konsultan perencana bangunan, Konsultan pengawas, Manajemen Konstruksi, Developer, Perancangan, Manajemen konstruksi dan realestate, Marketing Properti, Analisis Kredit Properti Perbankkan, Estimator maupun wirausaha dibidang properti &amp; Interior.</i><br>
				</p>
			</div>
		</div>
	</div>
</div>
<!-- fakultas-teknik END -->

<!-- pascasarjana -->
<div class="bloc bgc-white tc-army-green l-bloc" id="pascasarjana">
	<div class="container bloc-sm">
		<div class="row">
			<div class="col-sm-12">
				<h3 class="mg-md  tc-dark-midnight-blue">
					Pascasarjana
				</h3><img src="img/lazyload-ph.png" data-src="img/headerjpg-16.jpg" class="img-responsive lazyload" />
				<p>
					<strong><br>Magister Manajemen</strong><br><br>Konsentrasi:<br>- Manajemen Keuangan<br>- Manajemen Pemasaran<br>- Manajemen Sistem Informasi<br>- Manajemen Sumber Daya Manusia<br>- Manajemen Pendidikan<br>Kualitas SDM merupakan unsur penting dalam menghadapi era globalisasi. Pendidikan tinggi terus dibina dan dikembangkan guna menyiapkan peserta didik menjadi anggota masyarakat yang memiliki kemampuan akademis dan professional, kemampuan kepemimpunan, tanggap terhadap kebutuhan pembangunan serta pengembangan ilmu pengetahuan dan teknologi, berjiwa penuh pengabdian dan memiliki rasa tanggung jawab yang besar terhadap masa depan bangsa dan negara.<br> <br><br><strong>Magister Akuntansi</strong><br><br>Konsentrasi:<br>- Akuntansi Keuangan<br>- Sistem Informasi Akuntansi<br>- Pengauditan<br>- Perpajakan<br>Perbuahan paradigma organisasi dan manajemen saat ini menuntut sistem informasi yang semakin canggih sehingga mereorientaskan fungsi akuntansi dari accountability oriented function menuju decision oriented function. Program Magister Akuntansi Universitas Budi Luhur didirikan dalam rangka turut berperan serta memajukan sistem pendidikan akuntansi di indonesia, serta memenuhi kebutuhan dunia usaha akan pendidikan lanjutan berorientasi pada profesi.<br><br> <br><strong>Magister Ilmu Komputer</strong><br><br>Konsentrasi:<br>- Information System Technology<br>- Applied Computing Engineering<br>Keunggulan kompetitif yang harus dimiliki suatu perusahaan adalah kemampuan menyiapkan dan memiliki informasi yang cepat dan akurat. kemampuat tersebut harus dipersiapkan dan dikelola oleh sumber daya manusia yang handal. guna memenuhi kebutuhan akan SDM yang handal di bidang teknologi komunikasi, media dan informatika (telematika) serta dalam rangka percepatan pendayagunaan teknologi telematika sesuai dengan hasil sidang world summit on the information Society(WSIS),maka Universitas Budi Luhur menyelenggarakan program ilmu komputer (S2) untuk mempersiapkan SDM yang berkualitas dan kompeten dalam mempersiapkan, membangun dan mengelola sistem informasi perusahaan.<br><br> <br><strong>Magister Ilmu Komunikasi</strong><br><br>Bidang Spesialisasi:<br>- Corporate Public Relations<br>- Marketing Communication<br>- Media Industry Communication<br>Program Studi Magister Ilmu Komunikasi UBL mencetak profesional dengan kemampuan:<br><br>- Komunikasi personal, komunikasi persuasi dan mengasai ICT(Cyber Communication)<br>- Cepat beradaptasi pada perkembangan industri dan bisnis serta berjiwa enterpreneurship.<br>- Melakukan riset komunikasi dan menghasilkan karya komunikatif kreatif.<br>- Kepemimpinan, berfikir dan bertindak strategis, mengidentifikasi dan memecahkan masalah.<br>Membangun jaringan (networking), mengelola program dan kegiatan komunikasi (event management).<br>Peningkat rasa percaya diri, berbudi luhur, dan mengedepankan etika.<br>
				</p>
			</div>
		</div>
	</div>
</div>
<!-- pascasarjana END -->

<!-- diploma-3-d3- -->
<div class="bloc bgc-white tc-army-green l-bloc" id="diploma-3-d3-">
	<div class="container bloc-sm">
		<div class="row">
			<div class="col-sm-12">
				<h3 class="mg-md  tc-bleu-de-france">
					Diploma 3 (D3)
				</h3><img src="img/lazyload-ph.png" data-src="img/headerjpg-17.jpg" class="img-responsive lazyload" />
				<p>
					<strong><br>Manajemen Informatika</strong> <br><br><strong>Peminatan Computer Network<br>Peminatan Multimedia Graphic<br>Komputerisasi Akuntansi</strong> <br><br>Menghasilkan lulusan yang siap bekerja di bidang informatika, spesialisasi pada jaringan ICT, multimedia graphic, dan komputerisasi akuntansi.<br><br>Belajar dengan pendekatan aplikatif (praktik) dimana praktikum diprioritaskan, dengan perkuliahan langsung dilaksanakan dilaboratorium.<br><br>Secara umum lulusan D3 Unggulan mampu mengembangkan dan mengimplementasi sistem informasi berbasis web dan mampu melakukan pengembangan dan manajemen basis data.<br><br>D3 Unggulan menerapkan konsep belajar modern dengan e-learning dan dipadu dengan link &amp; match dari praktisi ICT. dengan konsep belajar ini maka mahasiswa mendapatkan nilai lebih dari model pembelajaran aplikatif.<br><br>Karakteristik Manajemen Informatika <br><br><strong>Peminatan Computer Network</strong><br>- Menghasilkan Lulusan siap kerja dibidang jaringan komputer, mulai dari desain, implementasi hingga manajemen jaringan.<br>- Terintegrasi dengan Cisco Academy Internasional.<br>- Kompetensi tambahan berupa Administrasi Jaringan dengan OS linux. <br><br><strong>Peminatan Multimedia Graphic</strong><br>- Menghasilkan lulusan siap kerja di bidang pengembangan multimedia grafis(multimedia animation : html 5/flash dan branding perusahaan/produk).<br>- Dengan pendekatan pembelajaran berbasiskan software Adobe Suite.<br>- Kompetensi tambahan berhubungan dengan desain cetak. <br><br><strong>Karakteristik Komputerisasi Akuntansi</strong><br>Menghasilkan ulusan siap kerja dibidang komputerisasi akuntansi, antara ain ; memahami siklus akuntansu secara umum untuk proses bisnis organisasi, mampu melaksanakan proses aplikatif akuntansi dan mampu menggunakan software pendukung proses akuntansi.<br>
				</p>
			</div>
		</div>
	</div>
</div>
<!-- diploma-3-d3- END -->

<!-- akademi-sekretari -->
<div class="bloc bgc-white tc-army-green l-bloc" id="akademi-sekretari">
	<div class="container bloc-sm">
		<div class="row">
			<div class="col-sm-12">
				<h3 class="mg-md  tc-deep-chestnut">
					Akademi Sekretari
				</h3><img src="img/lazyload-ph.png" data-src="img/headerjpg-18.jpg" class="img-responsive lazyload" />
				<p>
					<br>Berkomitmen tinggi menghasilkan Sekretaris modern yang cerdas dan berbudi luhur (strong personality),dengan kompetensi : <br><br>a. Mampu menyelesaikan pekerjaan tugas-tugas internal dan eksternal. <br>b. Mampu bekerja sama dan berkomunikasi dalam bahasa Indonesia dan bahasa Inggris dengan baik.<br>c. Mampu memanfaatkan teknologi informasi yang mendukung pekerjaan kesekretarisan.<br><br><strong>KURIKULUM</strong><br>Kurikulum yang mengacu pada Kerangka Kualiökasi Nasional Indonesia (KKNI) dirancang sesuai dengan kebutuhan dunia kerja sekretaris modern. Dengan konsentrasi utama memiliki kualiökasi kompetensi muatan administrasi dan kesekretarisan, teknologi informasi dan bahasa. Serta konsentrasi pendukung dengan muatan moral dan bisnis. Perkuliahan (teori dan praktek) dikemas dalam waktu 2.5 tahun (5 semester). Pada semester 6 mahasiswa mengikuti program magang dan siap memasuki dunia kerja.<br><br><strong>PROSPEK KERJA</strong><br>Berbekal pengetahuan dan kompetensi di bidang kesekretarisan, ekonomi/bisnis, bahasa, dan ICT (Information Communication Technology), maka lulusan Akademi Sekretari Budi Luhur siap memenangkan persaingan dunia kerja, sebagai sekretaris profesional bergelar Ahli Madya (A.Md).<br>
				</p>
			</div>
		</div>
	</div>
</div>
<!-- akademi-sekretari END -->
