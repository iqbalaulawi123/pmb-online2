<?php

$bodyContent = <<<EOD
<!DOCTYPE html
    PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta name="viewport" content="width=device-width" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Eliteadmin Responsive web app kit</title>
</head>

<body style="margin:0px; background: #f8f8f8; ">
    <div width="100%"
        style="background: #f8f8f8; padding: 0px 0px; font-family:arial; line-height:28px; height:100%;  width: 100%; color: #514d6a;">
        <div style="max-width: 700px; padding:50px 0;  margin: 0px auto; font-size: 14px">
            <table border="0" cellpadding="0" cellspacing="0" style="width: 100%; margin-bottom: 20px">
                <tbody>
                    <tr>
                        <td style="vertical-align: top; padding-bottom:30px;" align="center"><a href="#"
                                target="_blank"><img src="https://beasiswa.budiluhur.ac.id/img/logo_ubl.png" alt="Universitas Budi Luhur dan Akademi Sekretari Budi Luhur"
                                    style="border:none"><br />
                            </a> </td>
                    </tr>
                </tbody>
            </table>
            <div style="padding: 40px; background: #fff;">
                <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
                    <tbody>
                        <tr>
                            <td><b>Salam Budi Luhur,</b><br>
                                <p>Terima kasih telah menyelesaikan proses pendaftaran mahasiswa baru Universitas - Akademi Sekretari Budi Luhur, berikut adalah biaya perkuliahan Semester 1 Tahun Ajaran 2020/2021 ;</p>
                                <table>
                                    <tr>
                                        <td>Nomor Pendaftaran</td>
                                        <td>:</td>
                                        <td>$notest</td>
                                    </tr>
                                    <tr>
                                        <td>Nama Pendaftar</td>
                                        <td>:</td>
                                        <td>$nama</td>
                                    </tr>
                                   
                                    <tr>
                                        <td>Program Studi</td>
                                        <td>:</td>
                                        <td>$nmprodi ( $nmjenjang )</td>
                                    </tr>
                                    <tr>
                                        <td><b>No. Bukti/ Kwitansi</b></td>
                                        <td><b>:</b></td>
                                        <td><b>$nobukti</b></td>
                                    </tr>
                                    
                                    <tr>
                                        <td><b>Jumlah Biaya Masuk + Kuliah</b></td>
                                        <td><b>:</b></td>
                                        <td><b>$formatjumlahbayar</b></td>
                                    </tr>
                                   
                                </table>
                                <br>
                               
                                <b>Rincian Biaya Masuk + Kuliah Semester 1 :</b>
                                <table border ='0'>
									$tabelbayar
                                </table>
                                <br>
                                <b>Skema Angsuran :</b>
                                <table border ='0'>
									$tabelangsur
                                </table>
                              
                              
                                
                                 <p>Pembayaran Biaya Pendaftaran dapat dilakukan melalui Bank Mandiri atau transfer antar bank </p>
									<a href="https://cspmb.budiluhur.ac.id/PanduanPembayaran.pdf"
                                    style="display: inline-block; padding: 11px 30px; margin: 20px 0px 30px; font-size: 15px; color: #fff; background: #00c0c8; border-radius: 60px; text-decoration:none;">
                                    Panduan Pembayaran</a>
								<p>Email ini dibuat secara otomatis, mohon untuk tidak membalas pesan ke email ini, untuk informasi lebih lanjut dapat menghubungi kami di WhatsApp: 0811-1939-479 atau 0811-871-4455 atau melalui Telp : 021-585-3753 (Hunting)</p>

                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</body>
</html>
EOD;

?>
