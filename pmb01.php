
<!-- bloc-6 -->

<?php
if(isset($_GET['input'])&&$_GET['input']=="na"){
    echo"<script>Swal.fire('Gagal Mendaftar', 'Mohon Maaf Untuk sementara ini pendaftaran untuk kelas tersebut sedang tidak dibuka', 'warning');</script>";
}
//  include 'config.php'; ?>
<div class="bloc bgc-white l-bloc">
    <div class="container bloc-sm">
        <div class="col-sm-12">
        <img src="img/Banner_pendaftaran_budiluhur.ac.id.jpg" class="img-responsive" alt="beasiswa">
        </div>
    </div>
</div>  
<div class="bloc bgc-white l-bloc" id="bloc-6">
    <div class="container bloc-sm">
        <div class="row">
           
            <div class="col-sm-12">
                <form id="form"13 action="pmb00B.php" method="POST" novalidate
                      success-msg="Your message has been sent."
                      fail-msg="Sorry it seems that our mail server is not responding, Sorry for the inconvenience!">
                    <h3 class=" text-center mg-lg tc-army-green">
                        <span class="ion ion-clipboard"></span>&nbsp;Form Registrasi Online Universitas Budi Luhur
                    </h3>
                    <div class="form-group" id="fnama">
                        <label class="labelinput">
                            Nama Lengkap (Sesuai Dengan Akta Kelahiran)
                        </label>
                        <input id="xnama" class="form-control" name="nmcmhs" required/>
                    </div>
                    <div id="femail" class=" form-group">
                        <label class="labelinput">
                            Email<br>
                        </label>
                        <input class="form-control" required id="email" type="email" name="email"/>
                    </div>
                    <div id="ffemail" class="form-group">
                        <label class="labelinput">
                          Konfirmasi  Email<br>
                        </label>
                        <input class="form-control" required id="kemail" type="email" name="kemail"/>
                    </div>
                    <div class="form-group">
                        <label class="labelinput">
                            Nomor Handphone<br>
                        </label>    
                        <input class="form-control" required id="xhp" maxlength="20" type="number" name="hp"/>
                        <label>
                        Contoh: 6281297434832
                        </label>
                    </div>
                
                    <div class="form-group">
                        <label class="labelinput">Pilih Kelas</label>
                        <div class="aradio">
                            <div class="aradio-primary">
                                <input type="radio" name="kelas" id="ikelas" value="1" />
                                <label for="radio1" class="labelradio">Kelas Reguler (Diploma 3 Dan Strata 1 )</label>
                            </div>
                                <!-- <div class="aradio-primary">
                                    <input type="radio" name="kelas" id="ikelas" value="2" />
                                    <label for="radio2" class="labelradio">Kelas Karyawan</label>
                                </div> -->
                            <div class="aradio-primary">
                                <input type="radio" name="kelas" id="ikelas" value="3" />
                                <label for="radio2" class="labelradio">Kelas International</label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="btnbeasiswa" class="labelinput">
                            Anda dapat mengikuti program Beasiswa dengan cara menekan tombol 
                        </label>
                        <a href="https://beasiswa.budiluhur.ac.id" class="btn btn-primary">Beasiswa</a>
                    </div>
                    <div class="form-group">
                        <label class="labelinput">
                            Pilihan Program Studi
                        </label>
                        <select class="form-control" required id="ckdprogst" name="prodi">
                            <option selected disabled> Silakan pilih kelas terlebih dahulu</option>


                        </select>
                    </div>
                    <div class="form-group">
                        <label class="labelinput">Asal Sekolah</label>

                        <select class="form-control js-data-example-ajax" name="sma" id="sma" style="display:block"
                            required>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="csma" class="labelinput">Lainya</label>
                        <input type="checkbox" name="csma" id="csma">
                    </div>
                    <div class="form-group">
                        <label for="tsma" class="labelinput">Masukan Nama SMA</label>
                        <input type="text" name="tsma" id="tsma"
                            placeholder="anda tidak perlu mengisi ini jika sma anda terdapat pada menu diatas"
                            class="form-control" disabled>
                    </div>
                    <div class="text-center">
                        <button class="bloc-button btn btn-lg btn-bleu-de-france btn-block" type="button" id="viewdetail" disabled data-toggle="modal" data-target="#detail">
                            Saya Ingin Bergabung
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


<!-- Modal -->
<!-- Trigger the modal with a button -->
<

<!-- Modal -->
<div id="detail" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Pendaftaran</h4>
      </div>
      <div class="modal-body">
        <p>Pastikan data yang kamu masukan telah sesuai.</p>
        <table>
            <tr>
                <td class="col-lg-3">Nama</td>
                <td class="col-lg-1">:</td>
                <td id="dnama"></td>
            </tr>
            <tr>
                <td class="col-lg-3">Email</td>
                <td class="col-lg-1">:</td>
                <td id="demail"></td>
            </tr>
            <tr>
                <td class="col-lg-5">Nomor Handphone</td>
                <td class="col-lg-1">:</td>
                <td id="dhp"></td>
            </tr>
            <tr>
                <td class="col-lg-3">Jenjang</td>
                <td class="col-lg-1">:</td>
                <td id="djenjang"></td>
            </tr>
            <tr>
                <td class="col-lg-3">Program Studi</td>
                <td class="col-lg-1">:</td>
                <td id="dprodi"></td>
            </tr>
            <tr>
                <td class="col-lg-3">Kelas</td>
                <td class="col-lg-1">:</td>
                <td id="dkelas"></td>
            </tr>
        </table>
        
      </div>
      <div class="modal-footer">
        <button type="button" id="daftar" class="btn  btn-primary"  data-dismiss="modal">Daftar Sekarang</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

<!-- bloc-6 END -->
<script>
function validatename(name) {
    // jika string adalah email maka return 1 jika salah return 2
    if (/^[a-zA-Z ]+$/.test(name)) {
        return "1";
    } else {
        return "2";
    }
    
}
$(window).ready(function () {
   
    $('#csma').click(function () {
        if ($('#csma').is(':checked')) {
            $("#sma").prop('disabled', true);
            // $( "#sma" ).addClass( "hidden" );
            document.getElementById('sma').style.display = 'block';
            // $("#sma").prop('disabled', false);
            $("#sma").prop('selected', false);
            $("#tsma").prop('disabled', false);
            $("#tsma").attr("placeholder","silakan isi nama sma anda disini jika tidak terdapat pada menu diatas");
        }else{
            $("#sma").prop('disabled', false);
            $("#tsma").prop('disabled', true);
            $("#tsma").attr("placeholder","anda tidak perlu mengisi ini jika sma anda terdapat pada menu diatas");
            document.getElementById('sma').style.display = 'none';
        }
    });

    $('.js-data-example-ajax').select2({
            width: 'resolve' ,
			ajax: {
				url: 'pmb09C.php',
				dataType: 'json'
				// Additional AJAX parameters go here; see the end of this chapter for the full code of this example
			}
		});
       
    
    
    $("#xnama").keyup(function (){
        if(validatename(this.value)==1){
            $("#viewdetail").removeAttr("disabled");
            var element = document.getElementById("fnama");
            element.classList.remove("has-error");  
        }else{
            var element = document.getElementById("fnama");
            element.classList.add("has-error");
            $("#viewdetail").attr("disabled",true);
        }
        if($("#kemail").val()==null ||$("#kemail").val()==""){
        {
            $("#viewdetail").removeAttr("disabled");
        }

    }
    });
    $("#daftar").click(function(){
        
        if ($("#xnama").val() == null || $("#xnama").val()=="") {
            Swal.fire("Gagal Mendaftar", "Silakan isi nama Anda ", "error");
        }else if($("#email").val()==null || $("#email").val()==""){
            Swal.fire("Gagal Mendaftar", "Silakan isi email Anda ", "error");
        }else if($("#xhp").val()==null || $("#xhp").val()==""){
            Swal.fire("Gagal Mendaftar", "Silakan isi nomor HP Anda ", "error");
        }else if( $("#ckdprogst").val()==null){
            Swal.fire("Gagal Mendaftar", "Silakan Pilih Program Studi Anda ", "error");
        } else {
            $("#form").submit();
        }
    });
    function validateEmail(email) {
        var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    }
    $("#viewdetail").click(function(){
        var val = $("#ckdprogst").val();
        $.ajax({
            url:"pmb00A.php?getdata=detailprodi",
            data:"prodi="+val,
            cache:false,
            success:function(msg){
                var obj = JSON.parse(msg);
                var kls = $("input[name='kelas']:checked").val();;
               $("#dnama").html($("#xnama").val()); 
               $("#demail").html($("#email").val()); 
               $("#dhp").html($("#xhp").val());
               $("#djenjang").html(obj.ckdjen);
               $("#dprodi").html(obj.ckdprogst);
              
               if(kls==1){
                    $("#dkelas").html("Reguler");
               }else if(kls==2){
                    $("#dkelas").html("Karyawan");
                   
               }else if(kls==3){
                    $("#dkelas").html("Reguler");
                   
               }
            }
        })
    });
    $('input[type=radio][name=kelas]').change(function () {
        var val = this.value;
        
        $.ajax({
            url:"pmb00A.php?getdata=kelas",
            data:"kelas="+val,
            cache:false,
            success:function(msg){
                $("#ckdprogst").html(msg);
            }
        });
    });
    $("#email").keyup(function(){
        var valid =  ValidateEmail(this.value);
        // var konfirm = konfirmemail(this.val,$("#kemail").val);
        if(valid == true ){
         var element = document.getElementById("femail");
         element.classList.remove("has-error");
         $("#kemail").val("");
        //  $("#viewdetail").removeAttr("disabled");
         
         
        }else{
         var element = document.getElementById("femail");
         element.classList.add("has-error");
         $("#viewdetail").attr("disabled",true);
         
        }
    });
    $("#email").focusout(function (){
        var email = ValidateEmail(this.value);
        
        if(email==false){
            $("#email").focus();
            $("#viewdetail").attr("disabled",true);
        }
    });
    $("#kemail").keyup(function(){
        // var valid =  ValidateEmail(this.value);
        var konfirm = konfirmemail(this.value,$("#email").val());
        if(konfirm == true ){
         var element = document.getElementById("ffemail");
         element.classList.remove("has-error");
         $("#viewdetail").removeAttr("disabled");       
        }else{
         var element = document.getElementById("ffemail");
         element.classList.add("has-error");
         $("#viewdetail").attr("disabled",true);
        }
    });
        $("#kemail").focusout(function (){
            var email = konfirmemail(this.value,$("#email").val());;
            if(email==false){
                $("#viewdetail").attr("disabled",true);
            }
            else{
                $("#viewdetail").removeAttr("disabled");
            }
        });
    $("#ckdfak").change(function () { // ( CD02-1 )
        var ckdjen = $("#ckdjen").val();
        var ckdfak = $("#ckdfak").val();
        $.ajax({
            url: "pmb00A.php?getdata=prodi",
            data: "ckdjen=" + ckdjen + "&ckdfak=" + ckdfak,
            cache: false,
            success: function (msg) {
                $("#ckdprogst").html(msg);
            }
        });
    });
    $("#ckdjen").change(function () { // ( CD02-1 )
        var ckdjen = $("#ckdjen").val();

        $.ajax({
            url: "pmb00A.php?getdata=fakultas",
            data: "ckdjen=" + ckdjen,
            cache: false,
            success: function (msg) {
                $("#ckdfak").html(msg);
            }
        });
    });
});
function konfirmemail(emaila,emailb){
   
    if(emaila==emailb)
    {
        return(true)
    }else{
        return(false)
    }
}
function ValidateEmail(mail) {
      if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(mail)) {
         return (true)
      } else {
         return (false)
      }
   }
</script>