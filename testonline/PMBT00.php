<?php
session_start();
include "config.php";


$notest = $_SESSION['notest'];
$getuserinfo = $conn->prepare("SELECT * from mcalonmhs where CNOTEST = '$notest'");
$getuserinfo->execute();
$gu = $getuserinfo->fetch(PDO::FETCH_ASSOC);
$nama = $gu['CNAMA'];
$fk = $gu['CKDPROGST'];
$jenn = $gu['CKDJEN'];
$jnsdaftar = $gu['CSTATUS'];
$international = $gu['CINTERNATIONAL'];
       //mengambil tanggal ujian
$kdjadtest = $gu['CKDJAD'];

$getjadinfo = $conn->prepare("SELECT CJAMTEST,DTGLTEST from TJADPMB where CKDJAD = '$kdjadtest'");
$getjadinfo->execute();
$gji = $getjadinfo->fetch(PDO::FETCH_ASSOC);
$timetest = $gji['CJAMTEST'];
$jadtest = $gji['DTGLTEST'];
$jadtest = date('d-m-Y', strtotime($jadtest));


      //mengambil kode fakultas
$getuserinfo = $conn->prepare("SELECT CKDFAK,CNMPROGST from mprodi where CKDPROGST = '$fk' and CKDJEN ='$jenn'");
$getuserinfo->execute();
$fkk = $getuserinfo->fetch(PDO::FETCH_ASSOC);
$fak = $fkk['CKDFAK'];
$nmprodi = $fkk['CNMPROGST'];
$nmprodi = strtolower($nmprodi);
$nmprodi = ucwords($nmprodi);




$getfakultasinfo = $conn->prepare("SELECT DISTINCT f.cnmfak FROM mprodi p , mfakultas f WHERE p.ckdfak = '$fak' AND p.ckdfak = f.ckdfak");
$getfakultasinfo->execute();
$fakinfo = $getfakultasinfo->fetch(PDO::FETCH_ASSOC);
$nmfakultas = $fakinfo['CNMFAK'];
$nmfakultas = strtolower($nmfakultas);
$nmfakultas = ucwords($nmfakultas);


$getjenjanginfo = $conn->prepare(" SELECT DISTINCT f.cnmjen FROM mprodi p , mjenjang f WHERE p.ckdjen = '$jenn' AND p.ckdjen = f.ckdjen");
$getjenjanginfo->execute();
$jeninfo = $getjenjanginfo->fetch(PDO::FETCH_ASSOC);
$nmjenjang = $jeninfo['CNMJEN'];
$nmjenjang = strtolower($nmjenjang);
$nmjenjang = ucwords($nmjenjang);




       /*-- cek calon mahasiswa sudah membayar uang registrasi atau belum --*/
if (substr($notest, 0, 1) == 'A') {
    $gsb = $conn->prepare("SELECT B.cnobukti,B.CNOACC, A.CNIM, A.DFLAGKBL FROM  trbayar A ,  tbyrreg B WHERE A.cnim = '$notest' AND A.cnobukti = B.cnobukti AND B.CNOACC='2031434'");

} else {
    $gsb = $conn->prepare("SELECT B.cnobukti,B.CNOACC, A.CNIM, A.DFLAGKBL FROM  trbayar A ,  tbyrreg B WHERE A.cnim = '$notest' AND A.cnobukti = B.cnobukti AND B.CNOACC='2031403'");

}
$gsb->execute();
$cek = $gsb->fetch(PDO::FETCH_ASSOC);

$_SESSION['bayar'] = $cek['DFLAGKBL'];
$pembayaran = $_SESSION['bayar'];

if (empty($_SESSION['notest'])) {
    header("location:index.php?forbidden=denied");
}

?>
<!DOCTYPE html>
<html>
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <title>Test Online - Universitas Budi Luhur</title>
      <!-- Bootstrap Core CSS -->
      <link href="css/bootstrap.css" rel="stylesheet">
      <!-- MetisMenu CSS -->
      <link href="bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">
      <!-- Timeline CSS -->
      <link href="dist/css/timeline.css" rel="stylesheet">
      <!-- Custom CSS -->
      <link href="dist/css/sb-admin-2.css" rel="stylesheet">
      <link href="bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
      <!-- Morris Charts CSS -->
      <link href="bower_components/morrisjs/morris.css" rel="stylesheet">
   </head>
   <body>
      <div id="wrapper">
         <div class="row">
            <div class="col-md-12" style="background-color: #00ADEF;">
               <a href="">
                  <div class="hidden-xs col-sm-2 col-md-2">
                     <img src="rsz_1logo.png" class="img-responsive img-header">
                  </div>
               </a>
               <div class="col-md-6 col-md-push-4">
                  <p class="text-right header-budiluhur-1" style="color: white; font-family: tahoma; font-weight: bold;"><a href="index.php" style="color: white; text-decoration: none;"><em>UNIVERSITAS BUDI LUHUR</em></a></p>
                  <p class="text-right header-budiluhur-2" style="margin-top: -10px;  color: white; font-family: tahoma;"><em>& Akademi Sekretari Universitas Budi Luhur<br><small>Cerdas Berbudi Luhur</small></em></p>
               </div>
            </div>
         </div>
         <nav class="navbar navbar-default navbar-static-top normalizer" role="navigation" style="margin-bottom: 0;">
            <div class="navbar-header">
               <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
               <span class="sr-only">Toggle navigation</span>
               <span class="icon-bar"></span>
               <span class="icon-bar"></span>
               <span class="icon-bar"></span>
               </button>
               <a class="navbar-brand" href="index.php">Selamat datang <?php echo $nama . " ( " . $_SESSION['notest'] . " )"; ?>  Pukul <label id="time"></label></a>
            </div>
            <div class="navbar-default sidebar" role="navigation">
               <div class="sidebar-nav navbar-collapse">
                  <ul class="nav" id="side-menu">
                     <li>
                        <a href="?PAGE=home"><i class="fa fa-home fa-fw"></i> Halaman Utama</a>
                     </li>
                     <li>
                        <a href="?PAGE=PMBT01"><i class="fa fa-bar-chart-o fa-fw"></i> Test Online</a>
                     </li>
                     <li>
                        <a href="?PAGE=hasil"><i class="fa fa-user fa-fw"></i> Hasil Test</a>
                     </li>
                     <li>
                        <a href="?PAGE=info"><i class="fa fa-sign-out fa-fw"></i>Info</a>
                     </li>
                     <li>
                        <a href="PMBT10.php"><i class="fa fa-sign-out fa-fw"></i> Log Out</a>
                     </li>
                     <li><?php //error_reporting(E_ALL ^ (E_NOTICE| E_WARNING));  if (isset($_GET['PAGE'])and$_GET['PAGE']=='PMBT01') include"test.php"; ?></li>
                  </ul>
               </div>
            </div>
         </nav>
         <div id="page-wrapper" >
            <div class="row">
               <?php error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));
                if ($_GET['PAGE'] == null || $_GET['PAGE'] == 'home') {
                    include "PMBT02.php";
                } elseif ($_GET['PAGE'] == 'PMBT01') {
                    if (!in_array($jnsdaftar,array('0011','0022','0023'))) {
                        header("location:PMBT00.php?error=denied");
                    }
                    $now = date("d-m-Y");
                    $timenow = date("Hi");
                    if ($notest == 'A142000757' || $notest =='2211000324' ) {
                    } elseif ($_SESSION['bayar'] == null) {
                        header("location:PMBT00.php?error=bayar");
                    }
                    //jika jadwal belum ditentukan
                    elseif ($jadtest == null) {
                        header("location:PMBT00.php?time=no");
                    }
                    //jika belum di hari test
                    elseif (strtotime($now) < strtotime($jadtest)) {
                        header("location:PMBT00.php?time=f&date=$jadtest");
                        // echo "<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>dsdsdd".$jadtest;
                    }
                    //jika hari terlewat
                    elseif (strtotime($now) > strtotime($jadtest)) {
                        header("location:PMBT00.php?time=fl&date=$jadtest");
                    }
                    //jika waktunya belum mulai
                    elseif ($timenow < $timetest) {
                        header("location:PMBT00.php?time=wf&timetest=$timetest");
                    }
                    //khusus untuk beasiswa luar kota
                    //  elseif ($jnsdaftar=='0011' and strtotime($now)!=strtotime("18-01-2017")){
                    // header ("location:PMBT00.php?time=f");
                        
                    // }

                    ?>
               <ul class="nav nav-tabs">
                  <?php
                     //dibagian sini taro pemisah untuk beasiswa dan non beasiswa \
                    if ($jnsdaftar == '0011') {
                        if ($fak == 01 or $international == 'I') //
                        { ?>
                  <li <?php if ($_GET['SOAL'] == '18203') {
                            echo "class='active'";
                        } ?>><a  href="?PAGE=PMBT01&SOAL=18203&NOSOAL=1" ><strong>Logika</strong></a></li>
                  <?php

                }
                
                if ( $fak == 03) { ?>
                    <li <?php if ($_GET['SOAL'] == '18103') {
                              echo "class='active'";
                          } ?>><a  href="?PAGE=PMBT01&SOAL=18103&NOSOAL=1" ><strong>Matematika fe</strong></a></li>
                    <?php
  
                  }
                if ($fak == 01 or $fak == 05 ) { ?>
                  <li <?php if ($_GET['SOAL'] == '18113') {
                            echo "class='active'";
                        } ?>><a  href="?PAGE=PMBT01&SOAL=18113&NOSOAL=1" ><strong>Matematika fti</strong></a></li>
                  <?php

                }

                if ($international != 'I' and $fak == 03 or $fak == 05 or $fak == 07 or $fak == 04 or $fak == 02) //
                { ?>
                  <li <?php if ($_GET['SOAL'] == '18513') {
                            echo "class='active'";
                        } ?>><a  href="?PAGE=PMBT01&SOAL=18513&NOSOAL=1" ><strong>Bahasa Indonesia</strong></a></li>
                  <?php

                }
                if ($fak == 07 or $fak == 04 or $fak == 02) { ?>
                  <li <?php if ($_GET['SOAL'] == '18133') {
                            echo "class='active'";
                        } ?>><a  href="?PAGE=PMBT01&SOAL=18133&NOSOAL=1" ><strong>Ilmu Pengetahuan Umum  </strong></a></li>
                  <?php

                }
                        //
                ?>
                  <li <?php if ($_GET['SOAL'] == '18499') {
                            echo "class='active'";
                        } ?>><a  href="?PAGE=PMBT01&SOAL=18499&NOSOAL=1" ><strong>Bahasa Inggris</strong></a></li>
                  <?php
                    // end 0011 beasiswa akademik
                }
                  
                else {
                    if ($fak == 01 or $fak == 05 or $fak == 03) { ?>
                  <li <?php if ($_GET['SOAL'] == 'MTKD') {
                            echo "class='active'";
                        } ?>><a  href="?PAGE=PMBT01&SOAL=MTKD&NOSOAL=1" ><strong>Matematika Dasar</strong></a></li>
                  <?php

                }
                if ($fak == 01 or $fak == 05 or $fak == 03 or $fak == 04 or $fak == 07) {
                    ?>
                  <li <?php if ($_GET['SOAL'] == 'BINGU') {
                            echo "class='active'";
                        } ?>><a  href="?PAGE=PMBT01&SOAL=BINGU&NOSOAL=1" ><strong>Bahasa Inggris</strong></a></li>
                  <?php

                }

                if ($fak == 01 or $fak == 05) {
                    ?>
                  <li <?php if ($_GET['SOAL'] == 'LOGIK') {
                            echo "class='active'";
                        } ?>><a href="?PAGE=PMBT01&SOAL=LOGIK&NOSOAL=1" ><strong>Logika Dasar</strong></a></li>
                  <?php

                }

                if ($fak == 04 or $fak == 07) {
                    ?>
                  <li <?php if ($_GET['SOAL'] == 'PINGU') {
                            echo "class='active'";
                        } ?>><a  href="?PAGE=PMBT01&SOAL=PINGU&NOSOAL=1" ><strong>Pengetahuan Umum</strong></a></li>
                  <?php

                }

                if ($fak == 02) { ?>
                  <li <?php if ($_GET['SOAL'] == 'BINGA') {
                            echo "class='active'";
                        } ?>><a  href="?PAGE=PMBT01&SOAL=BINGA&NOSOAL=1" ><strong>Bahasa Inggris ( ASTRI )</strong> </a></li>
                  <?php

                }
            }

            ?>
               </ul>
               <br>
               <?php
                if ($_GET['SOAL'] == 'LOGIK') $KDSOAL = 'LOGIK';
                if ($_GET['SOAL'] == 'BINGU') $KDSOAL = 'BINGU';
                if ($_GET['SOAL'] == 'BINGA') $KDSOAL = 'BINGA';
                if ($_GET['SOAL'] == 'MTKD') $KDSOAL = 'MTKD';
                if ($_GET['SOAL'] == 'PINGU') $KDSOAL = 'PINGU';
                  //beasiswa
                if ($_GET['SOAL'] == '18113') $KDSOAL = '18113';
                if ($_GET['SOAL'] == '18103') $KDSOAL = '18103';
                if ($_GET['SOAL'] == '18203') $KDSOAL = '18203';
                if ($_GET['SOAL'] == '18133') $KDSOAL = '18133';
                if ($_GET['SOAL'] == '18499') $KDSOAL = '18499';
                if ($_GET['SOAL'] == '18513') $KDSOAL = '18513';





                $cekdatasoal = $conn->prepare("SELECT * from MJAWAB where CNOTEST = '$notest' and CKDSOAL = '$KDSOAL'");
                $cekdatasoal->execute();
                $cds = $cekdatasoal->fetch(PDO::FETCH_ASSOC);


                if ($cds['CNOTEST'] == null and $cds['CKDSOAL'] == null) {



                          //input soal

                    $waktu = 0;
                    if ($KDSOAL == "BINGU") $waktu = 30 * 60;
                    if ($KDSOAL == "LOGIK") $waktu = 30 * 60;
                    if ($KDSOAL == "BINGA") $waktu = 25 * 60;
                    if ($KDSOAL == "MTKD") $waktu = 45 * 60;
                    if ($KDSOAL == "PINGU") $waktu = 20 * 60;
                    if ($KDSOAL == "PINGU") $waktu = 20 * 60;
                          //beasiswa
                    if ($_GET['SOAL'] == '18113') $waktu = 2400;
                    if ($_GET['SOAL'] == '18103') $waktu = 2400;
                    if ($_GET['SOAL'] == '18203') $waktu = 3600;
                    if ($_GET['SOAL'] == '18133') $waktu = 3600;
                    if ($_GET['SOAL'] == '18499') $waktu = 3600;
                    if ($_GET['SOAL'] == '18513') $waktu = 3600;


                    $wktmulai = date("Y-m-d H:i:s");
                    $getuserinfo = $conn->prepare("INSERT INTO MJAWAB (CNOTEST,CKDSOAL,NSELESAI,CWAKTUMULAI) values ('$notest','$KDSOAL', $waktu, '$wktmulai')");
                    $getuserinfo->execute();


                    $inputsoal = $conn->prepare("SELECT * FROM MSOALTEST WHERE CKDSOAL = '$KDSOAL' Order by dbms_random.value");
                    $inputsoal->execute();
                    $urut = 1;
                    while ($k = $inputsoal->fetch(PDO::FETCH_ASSOC)) {

                        $CKDSOAL = $k['CKDSOAL'];
                        $CNOSOAL = $k['CNOSOAL'];
                        $insertsoal = $conn->prepare("INSERT INTO DJAWABANTEST (CNOTEST,CNOURUT,CKDSOAL,CNOSOAL) VALUES ('$notest','$urut','$CKDSOAL','$CNOSOAL')");
                        $insertsoal->execute();
                        $urut++;
                    }




                }
                if (isset($cds['NSELESAI']) and $cds['NSELESAI'] <= 0) {
                    ?>
               <div class="alert alert-danger">
                  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                  <strong>Peringatan!</strong> Anda Sudah Menyelsaikan ujian ini
               </div>
               <?php

            } else {
                if ($_GET['SOAL'] == 'LOGIK') echo '<h4>Logika Dasar</h4><br><br><br>';
                if ($_GET['SOAL'] == 'BINGU') echo '<h4>Bahasa Inggris</h4><br><br><br>';
                if ($_GET['SOAL'] == 'BINGA') echo '<h4>Bahasa Inggris</h4><br><br><br>';
                if ($_GET['SOAL'] == 'MTKD') echo '<h4>Matematika Dasar</h4><br><br><br>';
                if ($_GET['SOAL'] == 'PINGU') echo '<h4>Pengetahuan Umum</h4><br><br><br>';
                  //beasiswa
                if ($_GET['SOAL'] == '18203') echo '<h4>Logika</h4><br><br><br>';
                if ($_GET['SOAL'] == '18113') echo '<h4>Matematika</h4><br><br><br>';
                if ($_GET['SOAL'] == '18103') echo '<h4>Matematika</h4><br><br><br>';
                if ($_GET['SOAL'] == '18513') echo '<h4>Bahasa Indonesia</h4><br><br><br>';
                if ($_GET['SOAL'] == '18133') echo '<h4>Ilmu Pengetahuan Umum</h4><br><br><br>';
                if ($_GET['SOAL'] == '18499') echo '<h4>Bahasa Inggris</h4><br><br><br>';
                include "PMBT03.php";
                include "PMBT033.php";
                if ($cds['CNOTEST'] != null and $cds['CKDSOAL'] != null) {
                    ?>
               <br><br>
               <center>
                  <?php $sblm = $curent - 1;
                    $sesdh = $curent + 1;
                    if ($sblm == 1) {
                        $snlm = 1;
                    }
                    if ($sesdh >= $maxi) {
                        $sesdh = $maxi - 1;
                    } ?>
                  <?php if ($jnsdaftar != '0011') { ?>
                  <a href="?PAGE=PMBT01&SOAL=<?php echo $KDSOAL; ?>&NOSOAL=<?php echo $sblm; ?>" class="btn btn-primary" style="margin-right: 20px;">Sebelumnya</a><?php 
                                                                                                                                                            } ?>
                  <input type="submit" name="submit-all" class="selesai btn btn-danger" value="Selesai Mengerjakan" style="margin-right: 20px;">
                  <?php if ($jnsdaftar != '0011') { ?><a href="?PAGE=PMBT01&SOAL=<?php echo $KDSOAL; ?>&NOSOAL=<?php echo $sesdh; ?>" class="btn btn-primary" style="margin-right: 20px;">Selanjutnya</a><?php 
                                                                                                                                                                                                } ?>
                  <br>
                  <br>
                  <div class="alert alert-info">
                     <strong>Info!</strong> bila Anda ingin melihat soal lain atau mengubah jawaban, silakan klik angka yang ada didalam tabel dibawah ini
                  </div>
               </center>
               <?php

            }
        }

    } elseif ($_GET['PAGE'] == 'hasil') {

        include "PMBT035.php";
    } 




    ?>
            </div>
         </div>
      </div>
      <!-- confirm jawaban Modal content-->
      <div class="modal fade" id="delete_confirmation_modal" role="dialog" style="display: none;">
         <div class="modal-dialog" style="margin-top: 260.5px;">
            <div class="modal-content">
               <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">×</button>
                  <h4 class="modal-title">Apakah anda yakin dengan semua jawaban anda  ?</h4>
               </div>
               <form role="form" method="post" action="PMBT034.php" id="delete_data">
                  <input type="hidden" id="delete_item_id" name="id" value="12">
                  <input type="hidden"  name="kdsoal" value="<?php echo $KDSOAL; ?>">
                  <div class="modal-footer">
                     <button type="submit" class="btn btn-danger">Yes</button>
                     <button type="button" class="btn btn-primary" data-dismiss="modal">No</button>
                  </div>
               </form>
            </div>
         </div>
      </div>
   </body>
   <script src="http://code.jquery.com/jquery-1.10.2.min.js" type="text/javascript"></script>
   <script>jQuery.noConflict();</script>
   <script src="bower_components/jquery/dist/jquery.min.js"></script>
   <script>
      $(document).ready(function()
      {
          $(".selesai").click(function()
          {
              //Say - $('p').get(0).id - this delete item id
              $("#delete_item_id").val( $('p').get(0).id );
              $('#delete_confirmation_modal').modal('show');
          });
      });
   </script>
   <!-- for time server -->
   <script type="text/javascript">
      $(document).ready(function(){
      function svTime () {
            var xmlHttp;
            try {
                /* Firefox, Opera, Safari, Google Chrome */
                xmlHttp = new XMLHttpRequest ();

            } catch (e1) {
                /* Internet Explorer */
                try {
                    xmlHttp = new ActiveXObject ("Msxml2.XMLHTTP");
                } catch (e2) {
                    try {
                        xmlHttp = new ActiveXObject ("Microsoft.XMLHTTP");
                    } catch (e3) {
                        /* Ajax not supported, set xmlHttp to null for localtime */
                        xmlHttp = null;
                        console.warn ("Ajax not supported, use CPU (local) time.");
                    }
                }
            }

            if (xmlHttp === null) {
                return new Date ();
            } else {
                xmlHttp.open ("HEAD", window.location.href.toString (), false);
                xmlHttp.setRequestHeader ("Content-Type", "text/html");
                xmlHttp.send ("");
                return new Date (xmlHttp.getResponseHeader ("Date"));
            }
        };

        function zeroLead (number) {
            if (number < 10)
                number = "0" + number;

            return number;
        }

         function jalanin_jam () {
            var hari_ini = svTime();
            var j = hari_ini.getHours ();
            var m = zeroLead (hari_ini.getMinutes ());
            var d = zeroLead (hari_ini.getSeconds ());
            document.getElementById ("time").innerHTML = j + ":" + m + ":" + d;
        }

        setInterval(jalanin_jam, 1000);


      })

   </script>
   <script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
   <script src="bower_components/metisMenu/dist/metisMenu.min.js"></script>
   <script src="dist/js/sb-admin-2.js"></script>
</html>
