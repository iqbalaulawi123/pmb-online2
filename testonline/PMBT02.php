<!--
   ========================================================================================================
   ============Bagian awal cek Status pembayaran mahasiswa=================================================
   ========================================================================================================
   bagian class ini hanya akan muncul saat calon mahasiswa mencoba mengikuti test online disaat dia belum membayar uang pendaftaran
   
    -->
<?php 
   session_start();
   if ($_GET['error']=='bayar')
   
   {
   	?>
      <br>
<div class="alert alert-danger">
   <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
   <strong>Peringatan!</strong> Anda tidak dapat melakukan Test karena anda belum melakukan pembayaran, silahkan melakukan pembayaran sesuai dengan panduan pembayaran 
</div>
<!--
   ========================================================================================================
   ============Bagian akhir cek Status pembayaran mahasiswa================================================
   ========================================================================================================
   
    -->
<!--
   ========================================================================================================
   ============Bagian awal saat pertama kali membuka web ==================================================
   ========================================================================================================
   
    -->
<?php 
   }

   else if($_GET['time']=='f')
   {
?>
<br>
<div class="alert alert-danger">
   <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
   <strong>Peringatan!</strong> Waktu Test Belum Dimulai, Test akan dimulai pada tanggal <?php echo $_GET['date']; ?> 
</div>
<?php
 }

   else if($_GET['time']=='fl')
   {
?>
<br>
<div class="alert alert-danger">
   <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
   <strong>Peringatan!</strong> Waktu test anda sudah berakhir pada tanggal <?php echo $_GET['date']; ?> 
</div>
<?php

  }

   else if($_GET['time']=='wf')
   {
?>
<br>
<div class="alert alert-danger">
   <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
   <strong>Peringatan!</strong> Test akan dimulai pada jam <?php echo substr($_GET['timetest'],0,2).":".substr($_GET['timetest'],-2); ?> WIB
</div>
<?php

    }

   else if($_GET['time']=='no')
   {
?>
<br>
<div class="alert alert-danger">
   <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
   <strong>Peringatan!</strong> Anda Belum mendapatkan jadwal test, silahkan Hubungi bagian Penerimaan mahasiswa baru
</div>
<?php

   }
   ?>
<br>
<div id="main-content row">
   <div class="row alert alert-success col-lg-10 col-lg-push-1">
      <center> <strong> Selamat datang di Universitas Budi Luhur dan Akademi Sekretari Budi Luhur </strong></center>
   </div>
   <br>
   <div class='row col-lg-5 col-lg-push-1'>
      <table>
         <tr height="30px">
            <td >Nomor Pendaftaran</td>
            <td>&nbsp;:&nbsp; </td>
            <td><?php echo $notest; ?></td>
         </tr>
         <tr height="30px">
            <td>Nama</td>
            <td>&nbsp;:&nbsp;</td>
            <td><?php echo $nama; ?></td>
         </tr>
         <tr height="30px">
            <td>Jenjang</td>
            <td>&nbsp;:&nbsp;</td>
            <td><?php echo $nmjenjang	;?></td>
         </tr>
         <tr height="30px">
            <td>Fakultas</td>
            <td>&nbsp;:&nbsp;</td>
            <td><?php echo $nmfakultas	;?></td>
         </tr>
         <tr height="30px">
            <td>Program Studi</td>
            <td>&nbsp;:&nbsp;</td>
            <td><?php echo $nmprodi	;?></td>
         </tr>
         <tr height="30px">
            <td>Status Pembayaran</td>
            <td>&nbsp;:&nbsp;</td>
            <td><?php if(empty($pembayaran))echo"<span class='label label-danger'>Belum Melakukan pembayaran</span>	"; else echo "<span class='label label-success'>Selesai</span>	"; ?></td>
         </tr>
      </table>
   </div>
   <div class='col-lg-5 col-lg-push-1'>
      <blockquote>
         <strong>Note</strong>
         <br>
         <ul>  
            <li>Test akan dibuka sesuai dengan jadwal</li>
            <li>Bagi calon mahasiswa yang melakukan kecurangan maka akan dianggap gagal dalam test ini</li>
         </ul>
      </blockquote>
   </div>
 <div class=' col-lg-10 col-lg-push-1' style="margin-top: 5%;">
 
 <div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="panel-title">
     
        Panduan Singkat Test
      </h4>
    </div>
    <div >
      <div class="panel-body" style="list-style: none;">
      	<ul>
      		<li>
     
      			<p>Pilih soal yang ingin anda kerjakan terlebih dahulu (anda akan langsung diarahkan ke soal dan langsung memulai ujiannya)</p>
      			<img src="img/panduan/menu2.jpg" width="500px">
      		</li>
      		<li>
      			<p>jawab soal dengan klik tombol seperti gambar dibawah ini</p>
      			<img src="img/panduan/soal2.jpg" width="500px">
      		</li>
      		<li>
      			<p>setelah anda menjawab soal, anda akan di arahkan ke soal selanjutnya( <strong>Khusus Untuk Mahasiswa Beasiswa Akademik yang berada diluar daerah Jakarta. Jawaban Anda tidak dapat diubah, pastikan jawaban anda sudah benar</strong> )</p>
      		</li>
            <li>
               <p><Strong>Pastikan anda sudah menjawab soal dengan benar,</Strong></p>
            </li>

      		<li>
      			<p>setelah anda selesai menjawab semua soal, klik tombol "Selesai mengerjakan" (Setelah anda menekan tombol ini anda tidak bisa mengubah jawaban anda)</p>

      			<img src="img/panduan/selesai.jpg" width="500px">
      		</li>
            <li>
               <p><Strong>Khusus untuk calon mahasiswa beasiswa akademik yang berada diluar daerah Jakarta, anda tidak dapat berpindah soal lain</Strong></p>
            </li>

      		      		
      	</ul>

      </div>
    </div>
  </div>
 </div>
</div>

<!--
   ========================================================================================================
   ============Bagian akhir saat pertama kali membuka web =================================================
   ========================================================================================================
   
    -->