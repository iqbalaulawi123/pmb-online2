/*
 //
 //    ______    _   _           _  __      _   _     ____   ___
 //   |  ____|  | | | |         | |/ _|    | | | |   |___ \ / _ \
 //   | |__ __ _| |_| |__   __ _| | |_ __ _| |_| |__   __) | | | |
 //   |  __/ _` | __| '_ \ / _` | |  _/ _` | __| '_ \ |__ <| | | |
 //   | | | (_| | |_| | | | (_| | | || (_| | |_| | | |___) | |_| |
 //   |_|  \__,_|\__|_| |_|\__,_|_|_| \__,_|\__|_| |_|____/ \___/
 //
 // Licensed under GNU General Public License v3.0
 // http://www.gnu.org/licenses/gpl-3.0.txt
 // Written by Fathalfath30. Email : fathalfath30@gmail.com
 // Follow me on GithHub : https://github.com/Fathalfath30
 //
 // For the brave souls who get this far: You are the chosen ones,
 // the valiant knights of programming who toil away, without rest,
 // fixing our most awful code. To you, true saviors, kings of men,
 //
 // I say this: never gonna give you up, never gonna let you down,
 // never gonna run around and desert you. Never gonna make you cry,
 // never gonna say goodbye. Never gonna tell a lie and hurt you.
 //
*/

function FScript (selector) {
    var self = {};


    self.svTime = function () {
        var xmlHttp;
        try {
            /* Firefox, Opera, Safari, Google Chrome */
            xmlHttp = new XMLHttpRequest ();

        } catch (e1) {
            /* Internet Explorer */
            try {
                xmlHttp = new ActiveXObject ("Msxml2.XMLHTTP");
            } catch (e2) {
                try {
                    xmlHttp = new ActiveXObject ("Microsoft.XMLHTTP");
                } catch (e3) {
                    /* Ajax not supported, set xmlHttp to null for localtime */
                    xmlHttp = null;
                    console.warn ("Ajax not supported, use CPU (local) time.");
                }
            }
        }

        if (xmlHttp === null) {
            return new Date ();
        } else {
            xmlHttp.open ("HEAD", window.location.href.toString (), false);
            xmlHttp.setRequestHeader ("Content-Type", "text/html");
            xmlHttp.send ("");
            return new Date (xmlHttp.getResponseHeader ("Date"));
        }
    };

    self.MakeCookies = function (name, value, exp) {
        var ct = new Date (self.svTime ());
        var tn = ct.getTime ();

        tn += (parseInt (exp) * 60) * 1000;
        ct.setTime (tn);

        document.cookie = name + "=" + value + "; expires=" + ct.toUTCString () + "; path=/";


    };

    self.getCookie = function(cname) {
        var name = cname + "=";
        var decodedCookie = decodeURIComponent(document.cookie);
        var ca = decodedCookie.split(';');
        for(var i = 0; i <ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    };

    self.cookieExists = function(name) {
        if (self.getCookie(name) !== "") {
            return true;
        } else {
            return false;
        }
    };

    return self;
}