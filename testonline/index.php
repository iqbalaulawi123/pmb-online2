<?php
session_start();
 
   if(isset($_SESSION['notest']))
{
  header("location:PMBT00.php");
}

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
	<title>Test Online - Universitas dan Akademik Sekretari Budi Luhur</title>
    <link rel="shortcut icon" type="image/png" href="../pmb/img/logobl.png" />

    <title>Signin</title>
    <!-- Bootstrap core CSS -->
   <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <!-- Custom styles for this template -->
    <!-- <link href="https://getbootstrap.com/examples/signin/signin.css" rel="stylesheet"> -->
   <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
   


   
  </head>

  <body onload="">

    <div class="container">
   <?php 
           error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));
            if(isset($_GET['forbidden']))
            {
            ?>
			<div class="alert alert-warning" role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  <strong>Maaf!</strong> Anda belum login. silahkan login bedasarkan data yang dikirim melalui email Anda.
</div>
			<?php
            }
            elseif(isset($_GET['pass']))
            {
                ?>
			   <div class="alert alert-danger" role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  <strong>Gagal Login.</strong> Pastikan Username dan Password sudah benar
</div>
             <?php
            }
            else
            {
                ?>
         <div class="alert alert-info alert-dismissible" role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  <strong>Info!</strong> Silahkan login bedasarkan data yang dikirim melalui email Anda.
</div>
               <?php
            }
           ?>
      <form class="form-signin" action="PMBT01.php" method="GET">
        <h2 class="form-signin-heading">Silakan Login</h2>
        <label for="inputEmail" class="sr-only">No. Pendaftaran: </label>
        <input type="text" name="username" id="username" class="form-control" placeholder="No. Pendaftaran " required>
        <p></p>
        <label for="inputPassword" class="sr-only">Password: </label>
        <input type="password" name="password" id="password" class="form-control" placeholder="Password" required>
        <!-- <input type="text" name="time" id="time" class="form-control" required> -->

                    <input class="btn btn-lg btn-primary btn-block" type="submit" value="Masuk" name="">

      </form>

    </div> <!-- /container -->


    
  </body>
</html>
<script src="http://code.jquery.com/jquery-1.10.2.min.js" type="text/javascript"></script>
<script type="text/javascript" href="js/fathalfath30.js"></script>
<script type="text/javascript"> 
  $(document).ready(function(){
 function svTime () {
        var xmlHttp;
        try {
            /* Firefox, Opera, Safari, Google Chrome */
            xmlHttp = new XMLHttpRequest ();

        } catch (e1) {
            /* Internet Explorer */
            try {
                xmlHttp = new ActiveXObject ("Msxml2.XMLHTTP");
            } catch (e2) {
                try {
                    xmlHttp = new ActiveXObject ("Microsoft.XMLHTTP");
                } catch (e3) {
                    /* Ajax not supported, set xmlHttp to null for localtime */
                    xmlHttp = null;
                    console.warn ("Ajax not supported, use CPU (local) time.");
                }
            }
        }

        if (xmlHttp === null) {
            return new Date ();
        } else {
            xmlHttp.open ("HEAD", window.location.href.toString (), false);
            xmlHttp.setRequestHeader ("Content-Type", "text/html");
            xmlHttp.send ("");
            return new Date (xmlHttp.getResponseHeader ("Date"));
        }
    };

    function zeroLead (number) {
        if (number < 10)
            number = "0" + number;

        return number;
    }

     function jalanin_jam () {
        var hari_ini = svTime();
        var j = hari_ini.getHours ();
        var m = zeroLead (hari_ini.getMinutes ());
        var d = zeroLead (hari_ini.getSeconds ());
        document.getElementById ("time").innerHTML = j + ":" + m + ":" + d;
    }

    setInterval(jalanin_jam, 1000);


  })

</script>
