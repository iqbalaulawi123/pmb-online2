<!DOCTYPE html>
<html>
<head>
	<title>Test Online - Pendaftaran Mahasiswa Baru Universitas dan Akademi Sekretari Budi Luhur</title>

	<script type="text/javascript" src="jquery.js"></script>
	<style type="text/css">

 .to_top{

      position: fixed;
      right: 10px;
      cursor: help;
      bottom: 10px;
      font-weight: bold;
      color:#FFF;
      background-color:#f22;
      padding: 5px;
      border-radius:3px 3px 3px 3px;
  }

	</style>
 
	
</head>
<body>
<span class="to_top">Pastikan anda menekan tombol selesai jika anda sudah selesai, jika tidak maka anda akan dianggap tidak mengerjakan </span>

	
	<div class="wrap">
		<!--

========================================================================================================
============Bagian awal scrip untuk menampilkan soal  ==================================================
========================================================================================================
Soal ditampilkan 1 soal perhalaman

 -->
<?php
session_start();
if (!isset($_GET['NOSOAL']) || !isset($_GET['SOAL'])) {
    header("location: PMBT00.php");
}
if (isset($_GET['NOSOAL']) && isset($_GET['SOAL'])) {
    $curent = $_GET['NOSOAL'];// mengambil kode halaman yang digunakan untuk mengambil soal bedasarkan nomor urut soal 
    $kdsoal = strtoupper($_GET['SOAL']); // mengabil kode soal yang akan digunakan untuk mengambil soal dari master soal

    if (!is_numeric($curent)) {
        $curent = 1;
    }
    if ($curent < 1 || $curent > 45) {
        $curent = 1;
    }

    $ada = false;
    if ($kdsoal == "BINGU") $ada = true;
    if ($kdsoal == "LOGIK") $ada = true;
    if ($kdsoal == "BINGA") $ada = true;
    if ($kdsoal == "MTKD") $ada = true;
    if ($kdsoal == "PINGU") $ada = true;

	//beasiswa
    if ($_GET['SOAL'] == '2APPT') $ada = true;
    if ($_GET['SOAL'] == 'MTKTB') $ada = true;
    if ($_GET['SOAL'] == 'BINDO') $ada = true;
    if ($_GET['SOAL'] == 'ILMPU') $ada = true;
    if ($_GET['SOAL'] == 'BINGR') $ada = true;


    if (!$ada) {
        header("location:PMBT00.php");
    }
//	$query="SELECT * FROM mjawab WHERE cnotest = '$notest' and CKDSOAL = '$kdsoal'";
//	$q= $conn->query($query); //cek apakah pernah test
//	if (count($q->fetchALL() == 0)) {
//		$waktumulai = date("Y-m-d H:i:s");
//		$waktu=0;
//		if ($kdsoal=="BINGU") $waktu=30 * 60;
//		if ($kdsoal=="LOGIK") $waktu=30 * 60;
//		if ($kdsoal=="BINGA") $waktu=25 * 60;
//		if ($kdsoal=="MTKD")  $waktu=45 * 60;
//		if ($kdsoal=="PINGU") $waktu=20 * 60;
//		$q= $conn->query("insert into mjawab (cnotest, ckdsoal, njumbenar, nselesai, dwaktumulai) values ('$notest', '$kdsoal', 0, $waktu, '$waktumulai')");
//	}
//	else {
//		$qq = $q->fetch(PDO::FETCH_ASSOC);
//		$waktu = $qq['nselesai'];
//		if ($waktu <= 0) {
//			$jenis="";
//			if ($kdsoal=="BINGU") $jenis="Bahasa Inggris";
//			if ($kdsoal=="LOGIK") $jenis="Logika";
//			if ($kdsoal=="BINGA") $jenis="Bahasa Inggris";
//			if ($kdsoal=="MTKD")  $jenis="Matematika";
//			if ($kdsoal=="PINGU") $jenis="Pengetahuan Umum";
//			Echo "Waktu mengerjakan test $jenis Anda sudah habis";
//			exit();
//		}
//	}

    $query = "SELECT cnosoal, cjawab FROM djawabantest WHERE CKDSOAL='$kdsoal' and CNOTEST='$notest' and CNOURUT=$curent";
    $q = $conn->query($query); 
	//mengambil soal yang sudah diacak sebelumnya
    $qq = $q->fetch();
    $nomersoal = $qq['CNOSOAL'];
    $showsoal2 = $conn->prepare("SELECT * FROM MSOALTEST WHERE CKDSOAL = '$kdsoal' and CNOSOAL='$nomersoal'");
    $showsoal2->execute();
    $data = $showsoal2->fetch(PDO::FETCH_ASSOC);
    $wktmulai = date("YmdHis");
    $_SESSION["mulai"] = $wktmulai;
    //mengambil soal bedasarkan kodesoal
    ?>

	

		<form method="post" class="form-user col-lg-push-1 col-lg-10" >	
		
			<table  border=0>
				<tr>
					<td colspan="2">
						<input type="hidden" name="kdsoal" value="<?php echo $data['CKDSOAL']; ?>">
						<input type="text" nomorid='' name='nomorss' value="<?php echo $curent . "."; ?>" style="border: none; width:25px;" readonly> <?php echo $data['CSOALP']; ?>
					</td>
				</tr>
<?php
if ($data['CJAWABE'] == "") {
    ?>
				<tr>
					<td><input class="tombol-simpan" name="jawab"  type="radio" value="A" <?php if ($qq['CJAWAB'] == 'A') {
                                                                                echo 'checked';
                                                                            } ?>/> A. &nbsp;<?php echo $data['CJAWABA']; ?><br></td>
	               	<td><input class="tombol-simpan" name="jawab" type="radio" value="C" <?php if ($qq['CJAWAB'] == 'C') {
                                                                                            echo 'checked';
                                                                                        } ?>/> C. &nbsp;<?php echo $data['CJAWABC']; ?><br></td>
    	        </tr>
        	    <tr>
            		<td><input class="tombol-simpan" name="jawab" type="radio" value="B" <?php if ($qq['CJAWAB'] == 'B') {
                                                                                        echo 'checked';
                                                                                    } ?>> B. &nbsp;<?php echo $data['CJAWABB']; ?><br></td>
	               	<td><input class="tombol-simpan" name="jawab" type="radio" value="D" <?php if ($qq['CJAWAB'] == 'D') {
                                                                                            echo 'checked';
                                                                                        } ?>> D. &nbsp;<?php echo $data['CJAWABD']; ?><br></td>
	            </tr>
<?PHP

} else {
    ?>		
				<tr>
					<td><input class="tombol-simpan" name="jawab"  type="radio" value="A" <?php if ($qq['CJAWAB'] == 'A') {
                                                                                echo 'checked';
                                                                            } ?>/> A. &nbsp;<?php echo $data['CJAWABA']; ?><br></td>
	               	<td><input class="tombol-simpan" name="jawab" type="radio" value="D" <?php if ($qq['CJAWAB'] == 'D') {
                                                                                            echo 'checked';
                                                                                        } ?>/> D. &nbsp;<?php echo $data['CJAWABD']; ?><br></td>
    	        </tr>
        	    <tr>
            		<td><input class="tombol-simpan" name="jawab" type="radio" value="B" <?php if ($qq['CJAWAB'] == 'B') {
                                                                                        echo 'checked';
                                                                                    } ?>> B. &nbsp;<?php echo $data['CJAWABB']; ?><br></td>
	               	<td><input class="tombol-simpan" name="jawab" type="radio" value="E" <?php if ($qq['CJAWAB'] == 'E') {
                                                                                            echo 'checked';
                                                                                        } ?>> E. &nbsp;<?php echo $data['CJAWABE']; ?><br></td>
	            </tr>
    	        <tr>
        	    	<td><input class="tombol-simpan" name="jawab" type="radio" value="C" <?php if ($qq['CJAWAB'] == 'C') {
                                                                                        echo 'checked';
                                                                                    } ?>> C. &nbsp;<?php echo $data['CJAWABC']; ?><br></td>
	            	<td></td>
				</tr>
			

<?php

}
}
?>
</table>
		</form>
<!--

========================================================================================================
============Bagian akhir scrip untuk menampilkan soal  =================================================
========================================================================================================


 -->

 <!--

========================================================================================================
============Bagian awal untuk menampilkan halaman soal  ================================================
========================================================================================================


 -->
 <br><br><br>
<div class="tampildata"></div>
<div class="row col-lg-push-1 col-md-10">

	<ul class="pagination">
	<?php

$ambilsoal = $conn->query("SELECT CKDSOAL FROM DJAWABANTEST WHERE CKDSOAL = '$KDSOAL' and CNOTEST ='$notest' Order by CNOURUT");
$jmlsoal = count($ambilsoal->fetchALL());
?>
    <table border="1">
    <tr align="center">
<?php
for ($i = 1; $i <= $jmlsoal; $i++) {
		if($jnsdaftar!='0011'){
    echo "<td width=28><a href='PMBT00.php?PAGE=PMBT01&SOAL=$kdsoal&NOSOAL=$i'>$i</a></td>";
    	}
    	else
    	{
    		echo "<td width=28>$i</td>";
    	}
}
$maxi = $i;//buat penteuan batas halaman
?>	
	</tr>
	<tr align="center">
<?php
$ambilsoal = $conn->query("SELECT * FROM DJAWABANTEST WHERE CKDSOAL = '$KDSOAL' and CNOTEST ='$notest' Order by CNOURUT");
$j = 1;
while ($s = $ambilsoal->fetch(PDO::FETCH_ASSOC)) {
    $nomersoal = $s['CNOURUT'];
    $jawaban = $s['CJAWAB'];
    if ($jawaban == "") {
        echo "<td>.</td>";
    } else {
        echo "<td>$jawaban</td>";
    }
        
        // echo "<li><a href='?PAGE=PMBT01&SOAL=$kodesoal&NOSOAL=$nomersoal'><font class='color' color='$red'>$nomersoal $jawawban</font></a></l
}
echo "</tr>";

?>
	</ul>
		
	</div>
	<div class="col-md-12">
	
	</div>
<?php
$i = $_GET['NOSOAL'] + 1;
if ($i >= $maxi)
    $i = $maxi - 1;

?>
	<script type="text/javascript">
	$(document).ready(function(){
		$(".tombol-simpan").click(function(){
			var data = $('.form-user').serialize();
			 var x = "<?= $i ?>";
			  var z = "<?= $kdsoal ?>";
			$.ajax({
				type: 'POST',
				url: "PMBT031.php",
				data: data,
				success: function() {
					window.location.replace("PMBT00.php?PAGE=PMBT01&SOAL="+z+"&NOSOAL="+x);

					
				}
			});
		});

               
	});
	</script>
</body>
</html>

	<div id="timer"><?php  ?></div>