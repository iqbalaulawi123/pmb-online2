<?php


$bodyContent = <<<EOD
<!DOCTYPE html
    PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta name="viewport" content="width=device-width" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Eliteadmin Responsive web app kit</title>
</head>

<body style="margin:0px; background: #f8f8f8; ">
    <div width="100%"
        style="background: #f8f8f8; padding: 0px 0px; font-family:arial; line-height:28px; height:100%;  width: 100%; color: #514d6a;">
        <div style="max-width: 700px; padding:50px 0;  margin: 0px auto; font-size: 14px">
            <table border="0" cellpadding="0" cellspacing="0" style="width: 100%; margin-bottom: 20px">
                <tbody>
                    <tr>
                        <td style="vertical-align: top; padding-bottom:30px;" align="center"><a href="#"
                                target="_blank"><img src="https://beasiswa.budiluhur.ac.id/img/logo_ubl.png" alt="Univesrsitas Bud Luhur Dan Akademi Sekretari Budi Luhur"
                                    style="border:none"><br />
                            </a> </td>
                    </tr>
                </tbody>
            </table>
            <div style="padding: 40px; background: #fff;">
                <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
                    <tbody>
                        <tr>
                            <td><b>Salam Budi Luhur,</b>
                                <p>Selamat Adik telah terdaftar, di ucapkan terima kasih telah memilih Kampus Budi Luhur sebagai tempat untuk melanjutkan sekolah di pendidikan tinggi, dalam eMail ini kami lampirkan Formulir Pendaftaran serta Panduan Pembayaran Biaya Pendaftaran.</p>
                                <p>Silakan melakukan Pembayaran Biaya Pendaftaran, setelah melakukan pembayaran silakan klik <strong> <a href="https://pendaftaran.budiluhur.ac.id/panduan_pembayaran.pdf">di sini</a></strong> untuk menggunggah dokumen pendukung. (Khusus pembayaran antar bank bisa konfirmasi ke nomor whatsapp yang tertera)</p>
                                <!-- <table>
                                    <tr>
                                        <td>Nomor Pendaftaran</td>
                                        <td>:</td>
                                        <td>$notest</td>
                                    </tr>
                                    <tr>
                                        <td>Username</td>
                                        <td>:</td>
                                        <td>$notest</td>
                                    </tr>
                                    <tr>
                                        <td>Password</td>
                                        <td>:</td>
                                        <td>$passpl</td>
                                    </tr>
                                    <tr>
                                        <td>Nama Pendaftar</td>
                                        <td>:</td>
                                        <td>$nama</td>
                                    </tr>
                                    <tr>
                                        <td>Jenjang pendidikan</td>
                                        <td>:</td>
                                        <td>$nmjenjang</td>
                                    </tr>
                                    <tr>
                                        <td>Program Pilihan</td>
                                        <td>:</td>
                                        <td>$nmprodi</td>
                                    </tr>
                                    <tr>
                                        <td>Jenis Pendaftaran</td>
                                        <td>:</td>
                                        <td>$nmjnsdftr</td>
                                    </tr>
                                    <tr>
                                        <td>Jadwal Test</td>
                                        <td>:</td>
                                        <td>-</td>
                                    </tr>
                                </table> -->
                                <hr>
                                <h2>Pembayaran Paling lambat Tanggal $tanggalakhir</h2>
                                <a href="https://pendaftaran.budiluhur.ac.id/panduan_pembayaran.pdf"
                                    style="display: inline-block; padding: 11px 30px; margin: 20px 0px 30px; font-size: 15px; color: #fff; background: #00c0c8; border-radius: 60px; text-decoration:none;">
                                    Panduan Pembayaran </a>
                                <!-- <a href="https://pendaftaran.budiluhur.ac.id/kartu.php?notest=A142000757"
                                    style="display: inline-block; padding: 11px 30px; margin: 20px 0px 30px; font-size: 15px; color: #fff; background: #00c0c8; border-radius: 60px; text-decoration:none;">
                                    Cetak Kartu Pendaftaran </a> -->
                                <p style="color:#f00;">Email ini dibuat secara otomatis, mohon untuk tidak membalas pesan ke email ini,
                                    untuk informasi lebih lanjut dapat menghubungi kami di
                                    WhatsApp: 0811-1939-479 atau Telp: 021-585-3753</p>
                                <b></b>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</body>
</html>
EOD;

?>