  <?php
//untuk mengambil tahun ajar saat ini

include 'config.php';
$date = date("m/d/Y");
$query = $conn->prepare("SELECT * FROM mgel WHERE  dtmulai <= TO_DATE('$date', 'mm/dd/yyyy') AND dtselesai >= TO_DATE('$date', 'mm/dd/yyyy')");
$query->execute();
$tampil = $query->fetch();

$cthajar = $tampil['CTAHUN'];
$cthajar2=$cthajar + 1; 
$cthajar = "$cthajar / ".$cthajar2;
// $cthajar .= 
$gel = $tampil['CGEL'];
$d1 = date('d F Y', strtotime($tampil['DTMULAI']));
$d2 = date('d F Y', strtotime($tampil['DTSELESAI']));
$cstatus = $d1 . " - " . $d2;
echo "<!-- Gelombang $gel -->";
echo "<!-- Tahun" . $tampil['CTAHUN'] . " -->";

if (isset($_GET['input']) && $_GET['input'] == "failure" && empty($_GET['gel'])) {
    ?>

	<script type="text/javascript">
		swal("Maaf Pendaftaran Anda Dibatalkan!", "sedang terjadi beberapa kesalahan dalam sistem, mohon melakukan pendaftaran ulang atau info lebih lanjut hubungi kami di WhatsApp: 08111-9394-79 atau Telp: 021-585-3753 ", "error");
	</script>
	<?php
} elseif (isset($_GET['input']) && $_GET['input'] == "failure" && isset($_GET['gel'])) {
    ?>

    <script type="text/javascript">
        swal("Pendaftaran Masih belum dibuka!", " Mohon Maaf untuk saat ini pendaftaran belum dibuka, silahkan coba beberapa saat lagi atau info lebih lanjut hubungi kami di WhatsApp: 08111-9394-79 atau Telp: 021-585-3753  ", "warning");
    </script>
    <?php
}?>
<!-- bloc-1 -->
<div class="bloc bgc-white bg-Main-Hero-Background l-bloc" id="bloc-1">
	<div class="container bloc-lg">
		<div class="row">
			<div class="col-sm-12">
				<h1 class=" text-center mg-lg tc-white">
					UNIVERSITAS BUDI LUHUR&nbsp;DAN AKADEMI SEKRETARI BUDI LUHUR<br>
				</h1>
                <?php
if (empty($tampil['CGEL'])) {
    ?>
                    <h3 class="text-center  mg-lg tc-white">
                        Pendaftaran Belum Dibuka
                    </h3>

                <?php
} else {

    ?>
                    <h3 class="text-center  mg-lg tc-white">
                        Penerimaan Mahasiswa Baru&nbsp;Tahun Ajar <?php echo $cthajar; ?><br>Gelombang <?php echo $gel;  ?> : <?php echo $cstatus; ?><br>
                    </h3>
                    <div class="text-center">
                        <a class="btn btn-white animated bounce btn-lg btn-rd" onclick="scrollToTarget('#bloc-4')">Registrasi Cepat</a>
                    </div>

                <?php

}
?>

			</div>
		</div>
	</div>
</div>
<!-- bloc-1 END -->

<!-- bloc-2 -->
<div class="bloc bgc-white bg-Background l-bloc" id="bloc-2">
	<div class="container bloc-md">
		<div class="row">
			<div class="col-sm-4">
				<div class="text-center">
					<a href="index.php?page=brosur" class="btn btn-lg btn-dark-midnight-blue btn-rd btn-clean">Download Brosur Digital</a>
				</div>
			</div>
			<div class="col-sm-4">
				<div class="text-center">
					<a href="index.php?page=kirim" class="btn btn-lg btn-dark-midnight-blue btn-rd btn-clean">Kirim Ulang Kartu Pendaftaran</a>
				</div>
			</div>
			<div class="col-sm-4">
				<div class="text-center">
					<a href="index.php?page=hasil" class="btn btn-lg btn-dark-midnight-blue btn-rd btn-clean ">Hasil Test Ujian Masuk</a>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- bloc-2 END -->

<!-- bloc-3 -->
<div class="bloc bgc-anti-flash-white l-bloc" id="bloc-3">
	<div class="container bloc-sm">
		<div class="row">
			<div class="col-sm-12">
				<h3 class=" text-center tc-army-green mg-lg">
					Mengapa Memilih Kuliah di Universitas Budi Luhur?
				</h3>
				<div class="embed-responsive embed-responsive-16by9 none">
					<video controls class="embed-responsive-item lazyload">
						<source src="vid/TVC%20UBL%20%28Versi%20Testimoni%29.mp4" type="video/mp4" />Your browser does not support HTML5 video.
					</video>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- bloc-3 END -->

<!-- bloc-4 -->
<div class="bloc bgc-white l-bloc" id="bloc-4">
	<div class="container bloc-sm">
		<div class="row">
			<div class="col-sm-12">
				<form id="registform" action="pmb00B.php" method="POST" novalidate success-msg="Your message has been sent." fail-msg="Sorry it seems that our mail server is not responding, Sorry for the inconvenience!">
					<h3 class=" text-center mg-lg tc-army-green">
						<span class="ion ion-clipboard"></span>&nbsp;Form Registrasi Online Universitas Budi Luhur
					</h3>
					<div class="form-group">
						<label>
							Nama Lengkap
						</label>
						<input id="name_4494_11523" class="form-control" name="nmcmhs" required />
					</div>
					<div class="form-group">
						<label>
							Email<br>
						</label>
						<input class="form-control" required id="input_1796_4494_11523" type="email" name="email" />
					</div>
					<div class="form-group">
						<label>
							Nomor Handphone<br>
						</label>
						<input class="form-control" required id="input_695_4494_11523" type="number" name="hp" />
					</div>
					<div class="form-group">
						<label>
							Asal Sekolah
						</label>
						<br>

						<select class="form-control btn btn-default dropdown-toggle" required id="sma"  name="sma">
							<option selected disabled> Pilih Asal Sekolah</option>
							<?php
$query = $conn->prepare("SELECT * FROM MSMA order by CNMSMA asc");
$query->execute();
$query = $query->fetchAll();
foreach ($query as $key => $value) {
    echo "<option value='" . $value['CKDSMA'] . "' >" . $value['CNMSMA'] . "</option> ";
}
?>
						</select>
					</div>
					<div class="form-group">
						<label>
							Pilihan Jenjang Studi
						</label>
						<select class="form-control " required id="ckdjen"  name="jenjang">
							<option selected disabled> Pilih Jenjang Studi</option>
							<?php
								$query = $conn->prepare("SELECT * FROM MJENJANG where ckdjen != '60' order by CNMJEN asc");
								$query->execute();
								$query = $query->fetchAll();
								foreach ($query as $key => $value) {
									echo "<option value='" . $value['CKDJEN'] . "' >" . $value['CNMJEN'] . "</option> ";
								}
?>
						</select>
					</div>
					<div class="form-group">
						<label>
							Pilihan Fakultas
						</label>
						<select class="form-control " required id="ckdfak"  name="ckdfak">
							<option selected disabled> Pilih Fakultas</option>

						</select>
					</div>
					<div class="form-group">
						<label>
							Pilihan Program Studi
						</label>
							<select class="form-control" required id="ckdprogst"  name="prodi">
							<option selected disabled> Pilih Program Studi</option>


						</select>
					</div>
					<div class="text-center">
						<button id="submit" class="bloc-button btn btn-lg btn-bleu-de-france btn-block" type="submit">
							Saya Ingin Bergabung
						</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<!-- bloc-4 END -->
<script>

$(window).ready(function(){
		$("#sma").select2();
		$("#ckdfak").change(function(){ // ( CD02-1 )
      var ckdjen = $("#ckdjen").val();
      var ckdfak = $("#ckdfak").val();
      $.ajax({
          url: "pmb00A.php?getdata=prodi",
          data: "ckdjen="+ckdjen+"&ckdfak="+ckdfak,
          cache: false,
          success: function(msg){
              $("#ckdprogst").html(msg);
          }
      });
    });
		$("#ckdjen").change(function(){ // ( CD02-1 )
      var ckdjen = $("#ckdjen").val();

      $.ajax({
          url: "pmb00A.php?getdata=fakultas",
          data: "ckdjen="+ckdjen,
          cache: false,
          success: function(msg){
              $("#ckdfak").html(msg);
          }
      });
    });





});
</script>