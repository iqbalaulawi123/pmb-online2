<div class="bloc bgc-white l-bloc" id="bloc-12">
	<div class="container bloc-sm">
    <div class="row">
			<div class="col-sm-12">
				<div class="alert alert-warning">
					Untuk upload dokumen silakan masukan nomor pendaftaran beserta password sesuai dengan formulir pendaftaran Anda.
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-12">
				<form id="form_3111" action="pmb10A.php" method="POST" novalidate
					success-msg="Your message has been sent."
					fail-msg="Sorry it seems that our mail server is not responding, Sorry for the inconvenience!">
					<h3 class="text-center mg-lg  tc-army-green">
						Login
					</h3>
					<div class="form-group">
						<label>
							Nomor Pendaftaran
						</label>
						<input class="form-control" type="text" name="notest" required id="notest" />
					</div>
					<div class="form-group">
						<label>
							Password
						</label>
						<input class="form-control" required id="input_2942_5167_3111" type="password" name="password" />
					</div>
				
					<button class="bloc-button btn btn-lg btn-block btn-bleu-de-france" type="submit">
						Kirim
					</button>
				</form>
			</div>
		</div>
	</div>
</div>
<script>
  	<?php if(isset($_GET['success'])&&$_GET['success']==true){ ?>
  $(document).ready(function(){
	Swal.fire("Berhasil","Link untuk mengupload file sudah dikirim ke email Anda, Silakan cek kembali email Anda", "success")
  });
  <?php }	elseif(isset($_GET['fail'])&&$_GET['fail']==true)	 { ?>
  	$(document).ready(function(){
   Swal.fire("Nomor Pendaftaran atau password tidak sesuai", "<p style='margin-left=10px;margin-right:10px;'>Silakan periksa kembali nomor pendaftaran dan password Anda</p> ", "error")
  });
  	 <?php } elseif(isset($_GET['bayar'])&&$_GET['bayar']==true)	 { ?>
  	$(document).ready(function(){
   Swal.fire("Anda belum melakukan pembayaran uang formulir pendaftaran", "<p style='margin-left=10px;margin-right:10px;'>Silakan lakukan pembayaran terlebih dahulu sebelum mengunggah dokumen anda atau hubungi kami di<br> WhatsApp: 0811-1939-479<br>Telp: 021-585-3753</p> ", "error")
  });
  	 <?php } ?>
</script>